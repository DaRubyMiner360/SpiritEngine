
-- Spirit Dependencies

function os.name()
	local BinaryFormat = package.cpath
	if string.contains(BinaryFormat, ".dll") then
		return "Windows"
	elseif string.contains(BinaryFormat, ".so") then
		return "Linux"
	elseif string.contains(BinaryFormat, ".dylib") then
		return "MacOS"
	end
	return "Unknown OS"
end


VULKAN_SDK = os.getenv("VULKAN_SDK")

IncludeDir = {}
IncludeDir["assimp"] = "%{wks.location}/Spirit/vendor/assimp/include"
IncludeDir["stb_image"] = "%{wks.location}/Spirit/vendor/stb_image"
IncludeDir["yaml_cpp"] = "%{wks.location}/Spirit/vendor/yaml-cpp/include"
IncludeDir["Box2D"] = "%{wks.location}/Spirit/vendor/Box2D/include"
IncludeDir["GLFW"] = "%{wks.location}/Spirit/vendor/GLFW/include"
IncludeDir["Glad"] = "%{wks.location}/Spirit/vendor/Glad/include"
IncludeDir["ImGui"] = "%{wks.location}/Spirit/vendor/imgui"
IncludeDir["ImGuizmo"] = "%{wks.location}/Spirit/vendor/ImGuizmo"
IncludeDir["ImGuiColorTextEdit"] = "%{wks.location}/Spirit/vendor/ImGuiColorTextEdit"
IncludeDir["NativeFileDialog"] = "%{wks.location}/Spirit/vendor/NativeFileDialog/src/include"
IncludeDir["glm"] = "%{wks.location}/Spirit/vendor/glm"
IncludeDir["entt"] = "%{wks.location}/Spirit/vendor/entt/include"
IncludeDir["mono"] = "%{wks.location}/Spirit/vendor/mono/include"
--IncludeDir["OpenALInclude"] = "%{wks.location}/Spirit/vendor/OpenAL-Soft/include"
--IncludeDir["OpenALSrc"] = "%{wks.location}/Spirit/vendor/OpenAL-Soft"
--IncludeDir["OpenALSrcCommon"] = "%{wks.location}/Spirit/vendor/OpenAL-Soft/common"
IncludeDir["ogg"] = "%{wks.location}/Spirit/vendor/libogg/include"
IncludeDir["Vorbis"] = "%{wks.location}/Spirit/vendor/Vorbis/include"
IncludeDir["minimp3"] = "%{wks.location}/Spirit/vendor/minimp3"
IncludeDir["miniaudio"] = "%{wks.location}/Spirit/vendor/miniaudio"
IncludeDir["DiscordGameSDK"] = "%{wks.location}/Spirit/vendor/Discord-GameSDK/cpp"

LibraryDir = {}
LibraryDir["DiscordGameSDK"] = "%{wks.location}/Spirit/vendor/Discord-GameSDK/lib/x86_64/" .. os.name()
LibraryDir["mono"] = "%{wks.location}/Spirit/vendor/mono/lib/%{cfg.buildcfg}"
LibraryDir["assimp"] = "%{wks.location}/Spirit/vendor/assimp/lib"
LibraryDir["assimp_DLL"] = "%{wks.location}/Spirit/vendor/assimp/bin"

Library = {}
if VULKAN_SDK ~= nil then
	IncludeDir["SPIRV_Cross"] = "%{wks.location}/Spirit/vendor/SPIRV-Cross"
	IncludeDir["VulkanSDK"] = "%{VULKAN_SDK}/Include"

	LibraryDir["VulkanSDK"] = "%{VULKAN_SDK}/Lib"
else
	IncludeDir["shaderc"] = "%{wks.location}/Spirit/vendor/shaderc/libshaderc/include/"
	IncludeDir["SPIRV_Cross"] = "%{wks.location}/Spirit/vendor/SPIRV-Cross"

	Library["ShaderC"] = "shaderc"
	Library["ShaderC_Util"] = "shaderc_util"

	Library["SPIRV"] = "SPIRV"
	Library["SPIRV_Cross"] = "spirv-cross"
	Library["SPIRV_Cross_MachineIndependent"] = "MachineIndependent"
	Library["SPIRV_Cross_OSDependent"] = "OSDependent"
	Library["SPIRV_Cross_GenericCodeGen"] = "GenericCodeGen"
	Library["SPIRV_Cross_OGLCompiler"] = "OGLCompiler"

	Library["SPIRV_Tools_SPIRV_Tools"] = "SPIRV-Tools"
	Library["SPIRV_Tools_Opt"] = "SPIRV-Tools-opt"

	group "spirv_shaderc"
		include "Spirit/vendor/SPIRV-Cross"
		include "Spirit/vendor/shaderc"
	group ""
end

if os.name() == "Windows" then
	print("Detected the Windows platform.")

	LibraryDir["OpenAL"] = "%{wks.location}/Spirit/vendor/OpenAL-Soft/bin/Debug-windows-x86_64/OpenAL-Soft"

	Library["DiscordGameSDK"] = "%{LibraryDir.DiscordGameSDK}/discord_game_sdk.dll.lib"

	Library["mono"] = "%{LibraryDir.mono}/libmono-static-sgen.lib"

	Library["assimp"] = "%{LibraryDir.assimp}/assimp-vc143-mtd.lib"
	
	Library["WinSock"] = "Ws2_32.lib"
	Library["WinMM"] = "Winmm.lib"
	Library["WinVersion"] = "Version.lib"
	Library["BCrypt"] = "Bcrypt.lib"

	if VULKAN_SDK ~= nil then
		Library["Vulkan"] = "%{LibraryDir.VulkanSDK}/vulkan-1.lib"
		Library["VulkanUtils"] = "%{LibraryDir.VulkanSDK}/VkLayer_utils.lib"
	
		Library["ShaderC_Debug"] = "%{LibraryDir.VulkanSDK}/shaderc_sharedd.lib"
		Library["SPIRV_Cross_Debug"] = "%{LibraryDir.VulkanSDK}/spirv-cross-cored.lib"
		Library["SPIRV_Cross_GLSL_Debug"] = "%{LibraryDir.VulkanSDK}/spirv-cross-glsld.lib"
		Library["SPIRV_Tools_Debug"] = "%{LibraryDir.VulkanSDK}/SPIRV-Toolsd.lib"
	
		Library["ShaderC_Release"] = "%{LibraryDir.VulkanSDK}/shaderc_shared.lib"
		Library["SPIRV_Cross_Release"] = "%{LibraryDir.VulkanSDK}/spirv-cross-core.lib"
		Library["SPIRV_Cross_GLSL_Release"] = "%{LibraryDir.VulkanSDK}/spirv-cross-glsl.lib"
	end
elseif os.name() == "Linux" then
	print("Detected the Linux platform.")

	LibraryDir["OpenAL"] = "%{wks.location}/Spirit/vendor/OpenAL-Soft/bin/Debug-linux-x86_64/OpenAL-Soft"
	
	Library["DiscordGameSDK"] = "%{LibraryDir.DiscordGameSDK}/discord_game_sdk.so"

	-- Library["mono"] = "%{LibraryDir.mono}/libmono-static-sgen.a"

	Library["assimp"] = "assimp"

	if VULKAN_SDK ~= nil then
		Library["Vulkan"] = "%{LibraryDir.VulkanSDK}/vulkan-1.a"
		Library["VulkanUtils"] = "%{LibraryDir.VulkanSDK}/VkLayer_utils.a"
	
		Library["ShaderC_Debug"] = "%{LibraryDir.VulkanSDK}/shaderc_sharedd.a"
		Library["SPIRV_Cross_Debug"] = "%{LibraryDir.VulkanSDK}/spirv-cross-cored.a"
		Library["SPIRV_Cross_GLSL_Debug"] = "%{LibraryDir.VulkanSDK}/spirv-cross-glsld.a"
		Library["SPIRV_Tools_Debug"] = "%{LibraryDir.VulkanSDK}/SPIRV-Toolsd.a"
	
		Library["ShaderC_Release"] = "%{LibraryDir.VulkanSDK}/shaderc_shared.a"
		Library["SPIRV_Cross_Release"] = "%{LibraryDir.VulkanSDK}/spirv-cross-core.a"
		Library["SPIRV_Cross_GLSL_Release"] = "%{LibraryDir.VulkanSDK}/spirv-cross-glsl.a"
	end
elseif os.name() == "MacOS" then
	print("Error: Detected the MacOS platform, but MacOS isn't supported yet!")
	os.exit()
else
	print("Error: Couldn't detect the OS!")
	os.exit()
end
