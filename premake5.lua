include "./vendor/premake/premake_customization/solution_items.lua"

workspace "Spirit"
	architecture "x86_64"
	startproject "SpiritEditor"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

	solution_items
	{
		".editorconfig"
	}

	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

include "Dependencies.lua"

group "Dependencies"
	include "vendor/premake"
	include "Spirit/vendor/Box2D"
	include "Spirit/vendor/GLFW"
	include "Spirit/vendor/Glad"
	include "Spirit/vendor/imgui"
	include "Spirit/vendor/yaml-cpp"
	--include "Spirit/vendor/OpenAL-Soft"
	include "Spirit/vendor/libogg"
	include "Spirit/vendor/Vorbis"
	include "Spirit/vendor/assimp"
group ""

group "Core"
	include "Spirit"
	include "Spirit-ScriptCore"
group ""

group "Tools"
	include "SpiritEditor"
	include "ProjectManager"
	include "SpiritRuntime"
group ""

group "Misc"
	include "Sandbox"
group ""
