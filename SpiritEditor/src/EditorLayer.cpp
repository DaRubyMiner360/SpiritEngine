#include "spiritpch.h"
#include "EditorLayer.h"
#include "Spirit/Scene/SceneSerializer.h"
#include "Spirit/Utils/PlatformUtils.h"
#include "Spirit/Utils/StringUtils.h"
#include "Spirit/Utils/FileUtils.h"
#include "Spirit/Utils/INIUtils.h"
#include "Spirit/Utils/ScriptWrapperUtils.h"
#include "Spirit/Math/Math.h"

#include "Spirit/Scene/ScriptableEntity.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <iostream>
#include <fstream>
#include <string>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ImGuizmo.h"
#include <Glad/include/glad/glad.h>

#include "nfd.hpp"

namespace Spirit {

	extern std::filesystem::path g_ProjectPath;
	extern std::filesystem::path g_AssetPath;
	extern std::string g_ProjectName;

	EditorLayer::EditorLayer()
		: Layer("EditorLayer"), m_CameraController(1600.0f / 900.0f)
	{
	}

	template<typename T>
	void DrawComponent(const std::string& name, Entity entity)
	{
		bool disable = !entity || entity.HasComponent<T>();
		if (disable)
		{
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.663f, 0.663f, 0.663f, 1.0f });
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
		}
		if (ImGui::MenuItem(name.c_str()))
		{
			entity.AddComponent<T>();

			ImGui::CloseCurrentPopup();
		}
		if (disable)
		{
			ImGui::PopItemFlag();
			ImGui::PopStyleColor();
		}
	}

	void EditorLayer::OnAttach()
	{
		SPIRIT_PROFILE_FUNCTION();

		NFD_Init();

		m_IconPlay = Texture2D::Create("Resources/Icons/PlayButton.png");
		// m_IconPause = Texture2D::Create("Resources/Icons/PauseButton.png");
		m_IconSimulate = Texture2D::Create("Resources/Icons/SimulateButton.png");
		m_IconStop = Texture2D::Create("Resources/Icons/StopButton.png");

		FramebufferSpecification fbSpec;
		fbSpec.Attachments = { FramebufferTextureFormat::RGBA8, FramebufferTextureFormat::RED_INTEGER, FramebufferTextureFormat::Depth };
		fbSpec.Width = 1600;
		fbSpec.Height = 900;
		m_Framebuffer = Framebuffer::Create(fbSpec);

		m_IDFramebuffer = Framebuffer::Create(fbSpec);

		NewScene();


		auto commandLineArgs = Application::Get().GetSpecification().CommandLineArgs;
		if (commandLineArgs.Count > 2)
		{
			auto sceneFilePath = commandLineArgs[2];
			SceneSerializer serializer(m_ActiveScene);
			serializer.DeserializeScene(sceneFilePath);
		}

		m_EditorCamera = EditorCamera(30.0f, 1.778f, 0.1f, 1000.0f);
		Renderer2D::SetLineWidth(4.0f);

		m_EditorCamera.m_ViewportFocused = m_ViewportFocused;
		m_EditorCamera.m_ViewportHovered = m_ViewportHovered;

		m_SceneHierarchyPanel.SetContext(m_ActiveScene);

		Application::Get().GetWindow().SetTitle("Spiritual - " + m_ActiveScene->Name + " - " + g_ProjectName);

		mINI::INIFile file("configs/Editor.ini");
		mINI::INIStructure ini;

		if (file.read(ini))
		{
			m_ShowPhysicsColliders = StringUtils::ToBoolean(ini["general"]["ShowPhysicsColliders"]);
			m_EditorCamera.m_3DCamera = StringUtils::ToBoolean(ini["general"]["3DCamera"]);

			std::string theme = StringUtils::ToLowerCase(ini["appearance"]["Theme"]);
			if (theme == "dark")
				ImGuiLayer::SetDarkThemeColors();
			else if (theme == "light")
				ImGuiLayer::SetLightThemeColors();
			else if (theme == "corporategrey")
				ImGuiLayer::SetCorporateGreyThemeColors();
			else if (theme == "classicdark")
				ImGuiLayer::SetClassicDarkThemeColors();
			else if (theme == "classiclight")
				ImGuiLayer::SetClassicLightThemeColors();
			else if (theme == "classic")
				ImGuiLayer::SetClassicThemeColors();
			else
				ImGuiLayer::SetDarkThemeColors();

			m_RichPresenceEnabled = StringUtils::ToBoolean(ini["richpresence"]["Enabled"]);
			m_DisplayProjectName = StringUtils::ToBoolean(ini["richpresence"]["DisplayProjectName"]);
			m_DisplaySceneName = StringUtils::ToBoolean(ini["richpresence"]["DisplaySceneName"]);
			m_ResetTimestampOnSceneChange = StringUtils::ToBoolean(ini["richpresence"]["ResetTimestampWhenChangingScenes"]);
		}
		else
		{
			m_ShowPhysicsColliders = true;
			m_EditorCamera.m_3DCamera = true;

			ImGuiLayer::SetDarkThemeColors();

			m_RichPresenceEnabled = true;
			m_DisplayProjectName = true;
			m_DisplaySceneName = true;
			m_ResetTimestampOnSceneChange = false;
		}

		if (!m_EditorCamera.m_3DCamera && (m_EditorCamera.GetYaw() != 0 || m_EditorCamera.GetPitch() != 0))
		{
			m_EditorCamera.m_3DPitch = m_EditorCamera.m_Pitch;
			m_EditorCamera.m_Pitch = 0.0f;
			m_EditorCamera.m_3DYaw = m_EditorCamera.m_Yaw;
			m_EditorCamera.m_Yaw = 0.0f;
		}

		auto result = discord::Core::Create(964977882315231314, DiscordCreateFlags_NoRequireDiscord, &core);
		m_TimestampStart = std::time(0);
		m_LoadedDiscord = core != nullptr;
		if (m_RichPresenceEnabled && m_LoadedDiscord)
		{
			discord::Activity activity{};
			activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			activity.GetAssets().SetLargeImage("logo");
			activity.GetTimestamps().SetStart(m_TimestampStart);
			core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
		}
	}

	void EditorLayer::OnDetach()
	{
		SPIRIT_PROFILE_FUNCTION();

		NFD_Quit();
	}

	void EditorLayer::OnUpdate(Timestep ts)
	{
		SPIRIT_PROFILE_FUNCTION();

		if (m_LoadedDiscord)
			core->RunCallbacks();

		// Resize
		if (FramebufferSpecification spec = m_Framebuffer->GetSpecification();
			m_ViewportSize.x > 0.0f && m_ViewportSize.y > 0.0f && // zero sized framebuffer is invalid
			(spec.Width != m_ViewportSize.x || spec.Height != m_ViewportSize.y))
		{
			m_Framebuffer->Resize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
			m_IDFramebuffer->Resize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
			m_CameraController.OnResize(m_ViewportSize.x, m_ViewportSize.y);
			m_EditorCamera.SetViewportSize(m_ViewportSize.x, m_ViewportSize.y);
			m_ActiveScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
		}

		// Render
		Renderer2D::ResetStats();
		Renderer3D::ResetStats();
		m_Framebuffer->Bind();
		RenderCommand::SetClearColor({ 0.1f, 0.1f, 0.1f, 1 });
		RenderCommand::Clear();
		m_Framebuffer->Bind();

		// Clear our entity ID attachment to -1
		m_Framebuffer->ClearAttachment(1, -1);

		switch (m_SceneState)
		{
		case SceneState::Edit:
		{
			if (m_ViewportFocused)
				m_CameraController.OnUpdate(ts);

			m_EditorCamera.OnUpdate(ts);

			m_ActiveScene->OnUpdateEditor(ts, m_EditorCamera);
			break;
		}
		case SceneState::Simulate:
		{
			m_EditorCamera.OnUpdate(ts);

			m_ActiveScene->OnUpdateSimulation(ts, m_EditorCamera);
			break;
		}
		case SceneState::Play:
		{
			m_ActiveScene->OnUpdateRuntime(ts);
			break;
		}
		}

		auto [mx, my] = ImGui::GetMousePos();
		mx -= m_ViewportBounds[0].x;
		my -= m_ViewportBounds[0].y;
		glm::vec2 viewportSize = m_ViewportBounds[1] - m_ViewportBounds[0];
		my = viewportSize.y - my;
		int mouseX = (int)mx;
		int mouseY = (int)my;
		if (mouseX >= 0 && mouseY >= 0 && mouseX < (int)viewportSize.x && mouseY < (int)viewportSize.y - 30 && m_ViewportHovered && m_ViewportFocused)
		{
			int pixelData = m_Framebuffer->ReadPixel(1, mx, my);
			m_HoveredEntity = pixelData == -1 ? Entity() : Entity((entt::entity)pixelData, m_ActiveScene.get());
		}

		OnOverlayRender();

		m_Framebuffer->Unbind();
	}

	void EditorLayer::OnImGuiRender()
	{
		SPIRIT_PROFILE_FUNCTION();

		// Note: Switch this to true to enable dockspace
		static bool dockspaceOpen = true;
		static bool opt_fullscreen_persistant = true;
		bool opt_fullscreen = opt_fullscreen_persistant;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}

		// When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;

		// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
		// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive, 
		// all active windows docked into it will lose their parent and become undocked.
		// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise 
		// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("DockSpace Demo", &dockspaceOpen, window_flags);
		ImGui::PopStyleVar();

		if (opt_fullscreen)
			ImGui::PopStyleVar(2);

		// DockSpace
		ImGuiIO& io = ImGui::GetIO();
		ImGuiStyle& style = ImGui::GetStyle();
		float minWinSizeX = style.WindowMinSize.x;
		style.WindowMinSize.x = 396.0f;
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
		{
			ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		}

		style.WindowMinSize.x = minWinSizeX;

		UI_Viewport();
		UI_MenuBar();
		UI_Toolbar();
		UI_Settings();
		UI_ChildPanels();
		UI_Stats();

		ImGui::End();
	}

	void EditorLayer::UI_MenuBar()
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				// Disabling fullscreen would allow the window to be moved to the front of other windows,
				// which we can't undo at the moment without finer window depth/z control.
				//ImGui::MenuItem("Fullscreen", NULL, &opt_fullscreen_persistant);

				if (ImGui::MenuItem("Create Project"))
				{
#if defined SPIRIT_PLATFORM_WINDOWS
					std::system("start \"\" ProjectManager.exe -c");
#elif defined SPIRIT_PLATFORM_LINUX
					std::system("xterm \"./ProjectManager -c\"");
#elif defined SPIRIT_PLATFORM_MACOS
					std::system("osascript -e 'tell app \"Terminal\" to do script \"./ProjectManager -c\"'");
#endif
					SafeExit();
				}

				if (ImGui::MenuItem("Open Project"))
				{
#if defined SPIRIT_PLATFORM_WINDOWS
					std::system("start \"\" ProjectManager.exe -o");
#elif defined SPIRIT_PLATFORM_LINUX
					std::system("xterm \"./ProjectManager -o\"");
#elif defined SPIRIT_PLATFORM_MACOS
					std::system("osascript -e 'tell app \"Terminal\" to do script \"./ProjectManager -o\"'");
#endif
					SafeExit();
				}

				if (ImGui::BeginMenu("Open Recent Project"))
				{
					if (ImGui::MenuItem("No Project", (const char*)0, false, g_ProjectName != "No Project"))
					{
#if defined SPIRIT_PLATFORM_WINDOWS
						std::system("start \"\" ProjectManager.exe -o projects/main");
#elif defined SPIRIT_PLATFORM_LINUX
						std::system("xterm \"./ProjectManager -o projects/main\"");
#elif defined SPIRIT_PLATFORM_MACOS
						std::system("osascript -e 'tell app \"Terminal\" to do script \"./ProjectManager -o projects/main\"'");
#endif
						SafeExit();
					}

					mINI::INIFile file("configs/OpenedProjects.ini");
					mINI::INIStructure ini;

					if (file.read(ini))
					{
						for (auto& key : ini["projects"])
						{
							std::filesystem::path filepath = ini["projects"][key.first];

							bool isProject = false;
							std::filesystem::path projectFile = "";
							if (filepath.has_extension())
							{
								isProject = FileTypes::IsProjectType(filepath.extension().string());
								projectFile = filepath;
								filepath = filepath.parent_path();
							}
							else if (std::filesystem::is_directory(filepath))
							{
								for (auto& f : std::filesystem::directory_iterator(filepath))
								{
									for (auto& t : FileTypes::GetProjectTypes())
									{
										auto& type = "." + t;
										if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
										{
											if (isProject)
											{
												SPIRIT_WARN("More than one project file found!");
												continue;
											}
											isProject = std::filesystem::exists(f.path());
											projectFile = f.path();
										}
									}
								}
							}

							if (isProject)
							{
								std::string projectName = "";

								mINI::INIFile projectFile(projectFile.string());
								mINI::INIStructure projectINI;

								if (projectFile.read(projectINI))
									projectName = projectINI["project"]["name"];

								if (ImGui::MenuItem(projectName.c_str(), (const char*)0, false, g_ProjectName != projectName))
								{
#if defined SPIRIT_PLATFORM_WINDOWS
									std::system((std::string("start \"\" ProjectManager.exe -o ") + filepath.string()).c_str());
#elif defined SPIRIT_PLATFORM_LINUX
									std::system((std::string("xterm \"./ProjectManager -o ") + filepath.string() + "\"").c_str());
#elif defined SPIRIT_PLATFORM_MACOS
									std::system((std::string("osascript -e 'tell app \"Terminal\" to do script \"./ProjectManager -o ") + filepath.string() + "\"'").c_str());
#endif
									SafeExit();
								}
							}
						}
					}

					ImGui::EndMenu();
				}

				ImGui::Separator();

				if (ImGui::MenuItem("New Scene", "Ctrl+N"))
					NewScene();

				if (ImGui::MenuItem("Open Scene...", "Ctrl+O"))
					OpenScene();

				if (ImGui::MenuItem("Save Scene", "Ctrl+S", false, m_ActiveScene != nullptr))
					SaveScene();

				if (ImGui::MenuItem("Save Scene As...", "Ctrl+Shift+S", false, m_ActiveScene != nullptr))
					SaveSceneAs();

				if (ImGui::MenuItem("Exit", "Alt+F4"))
					Application::Get().Close();

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Entity"))
			{
				m_SceneHierarchyPanel.EntityCreation();

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Component"))
			{
				if (!m_SceneHierarchyPanel.GetSelectionContext())
				{
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.663f, 0.663f, 0.663f, 1.0f });
					ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				}

				DrawComponent<CameraComponent>("Camera", m_SceneHierarchyPanel.GetSelectionContext());

				{
					bool disable = !m_SceneHierarchyPanel.GetSelectionContext();
					if (disable)
					{
						ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.663f, 0.663f, 0.663f, 1.0f });
						ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
					}
					if (ImGui::MenuItem("Script"))
					{
						if (!m_SceneHierarchyPanel.GetSelectionContext().HasComponent<ScriptWrapperComponent>())
							m_SceneHierarchyPanel.GetSelectionContext().AddComponent<ScriptWrapperComponent>();

						ScriptWrapperUtils::AddComponent(m_SceneHierarchyPanel.GetSelectionContext(), ScriptComponent());

						ImGui::CloseCurrentPopup();
					}
					if (disable)
					{
						ImGui::PopItemFlag();
						ImGui::PopStyleColor();
					}
				}

				DrawComponent<SpriteRendererComponent>("Sprite Renderer", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<CircleRendererComponent>("Circle Renderer", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<MeshRendererComponent>("Mesh Renderer", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<DirectionalLightComponent>("Directional Light", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<PointLightComponent>("Point Light", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<SpotLightComponent>("Spot Light", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<Rigidbody2DComponent>("Rigidbody 2D", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<BoxCollider2DComponent>("Box Collider 2D", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<CircleCollider2DComponent>("Circle Collider 2D", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<AudioListenerComponent>("Audio Listener", m_SceneHierarchyPanel.GetSelectionContext());
				DrawComponent<AudioSourceComponent>("Audio Source", m_SceneHierarchyPanel.GetSelectionContext());

				if (!m_SceneHierarchyPanel.GetSelectionContext())
				{
					ImGui::PopItemFlag();
					ImGui::PopStyleColor();
				}

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}
	}

	void EditorLayer::UI_ChildPanels()
	{
		m_SceneHierarchyPanel.OnImGuiRender();
		m_ContentBrowserPanel.OnImGuiRender();
		//m_ConsolePanel.OnImGuiRender();
	}

	void EditorLayer::UI_Stats()
	{
		ImGui::Begin("Stats");

		auto stats2d = Renderer2D::GetStats();
		auto stats3d = Renderer3D::GetStats();
		ImGui::Text("Renderer Stats:");
		ImGui::Text("Draw Calls: %d (2D: %d, 3D: %d)", stats2d.DrawCalls + stats3d.DrawCalls, stats2d.DrawCalls, stats3d.DrawCalls);
		ImGui::Text("Quads (2D): %d", stats2d.QuadCount);
		ImGui::Text("Meshes (3D): %d", stats3d.MeshCount);
		ImGui::Text("Vertices: %d (2D: %d, 3D: %d)", stats2d.GetTotalVertexCount() + stats3d.GetTotalVertexCount(), stats2d.GetTotalVertexCount(), stats3d.GetTotalVertexCount());
		ImGui::Text("Indices: %d (2D: %d, 3D: %d)", stats2d.GetTotalIndexCount() + stats3d.GetTotalIndexCount(), stats2d.GetTotalIndexCount(), stats3d.GetTotalIndexCount());

		std::string name = "None";
		if ((entt::entity)m_HoveredEntity != entt::null)
			name = m_HoveredEntity.GetComponent<TagComponent>().Name;
		ImGui::Text("Hovered Entity: %s", name.c_str());

		ImGui::End();
	}

	void EditorLayer::UI_Settings()
	{
		ImGui::Begin("Settings");


		mINI::INIFile file("configs/Editor.ini");
		mINI::INIStructure ini;
		file.read(ini);

		ImGui::Checkbox("Show physics colliders", &m_ShowPhysicsColliders);
		ini["general"]["ShowPhysicsColliders"] = std::to_string(m_ShowPhysicsColliders);
		if (ImGui::Checkbox("3D Camera", &m_EditorCamera.m_3DCamera))
		{
			if (m_EditorCamera.m_3DCamera)
			{
				m_EditorCamera.m_3DPitch = m_EditorCamera.m_Pitch;
				m_EditorCamera.m_Pitch = 0.0f;
				m_EditorCamera.m_3DYaw = m_EditorCamera.m_Yaw;
				m_EditorCamera.m_Yaw = 0.0f;
			}
			else
			{
				m_EditorCamera.m_Pitch = m_EditorCamera.m_3DPitch;
				m_EditorCamera.m_3DPitch = 0.0f;
				m_EditorCamera.m_Yaw = m_EditorCamera.m_3DYaw;
				m_EditorCamera.m_3DYaw = 0.0f;
			}
		}
		ini["general"]["3DCamera"] = std::to_string(m_EditorCamera.m_3DCamera);

		if (ImGui::BeginMenu("Theme"))
		{
			if (ImGui::MenuItem("Dark Theme"))
				ImGuiLayer::SetDarkThemeColors();

			if (ImGui::MenuItem("Light Theme"))
				ImGuiLayer::SetLightThemeColors();

			if (ImGui::MenuItem("Corporate Grey Theme"))
				ImGuiLayer::SetCorporateGreyThemeColors();

			if (ImGui::MenuItem("Classic Dark Theme"))
				ImGuiLayer::SetClassicDarkThemeColors();

			if (ImGui::MenuItem("Classic Light Theme"))
				ImGuiLayer::SetClassicLightThemeColors();

			if (ImGui::MenuItem("Classic Theme"))
				ImGuiLayer::SetClassicThemeColors();

			ImGui::EndMenu();
		}

		ini["appearance"]["Theme"] = ImGuiLayer::GetTheme();

		if (ImGui::Checkbox("Rich Presence Enabled", &m_RichPresenceEnabled))
		{
			//if (m_RichPresenceEnabled)
			//{
			//	discord::Activity activity{};
			//	activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			//	activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			//	activity.GetAssets().SetLargeImage("logo");
			//	activity.GetTimestamps().SetStart(m_TimestampStart);
			//	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
			//}
		}
		ini["richpresence"]["Enabled"] = std::to_string(m_RichPresenceEnabled);
		if (ImGui::Checkbox("Display Project Name", &m_DisplayProjectName))
		{
			//if (m_RichPresenceEnabled)
			//{
			//	discord::Activity activity{};
			//	activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			//	activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			//	activity.GetAssets().SetLargeImage("logo");
			//	activity.GetTimestamps().SetStart(m_TimestampStart);
			//	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
			//}
		}
		ini["richpresence"]["DisplayProjectName"] = std::to_string(m_DisplayProjectName);
		if (ImGui::Checkbox("Display Scene Name", &m_DisplaySceneName))
		{
			//if (m_RichPresenceEnabled)
			//{
			//	discord::Activity activity{};
			//	activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			//	activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			//	activity.GetAssets().SetLargeImage("logo");
			//	activity.GetTimestamps().SetStart(m_TimestampStart);
			//	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
			//}
		}
		ini["richpresence"]["DisplaySceneName"] = std::to_string(m_DisplaySceneName);
		if (ImGui::Checkbox("Reset Timestamp When Changing Scenes", &m_ResetTimestampOnSceneChange))
		{
			//if (m_RichPresenceEnabled)
			//{
			//	discord::Activity activity{};
			//	activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			//	activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			//	activity.GetAssets().SetLargeImage("logo");
			//	activity.GetTimestamps().SetStart(m_TimestampStart);
			//	core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
			//	}
		}
		ini["richpresence"]["ResetTimestampWhenChangingScenes"] = std::to_string(m_ResetTimestampOnSceneChange);

		file.write(ini, true);


		ImGui::End();
	}

	void EditorLayer::UI_Viewport()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{ 0, 0 });
		ImGui::Begin("Viewport");

		auto viewportMinRegion = ImGui::GetWindowContentRegionMin();
		auto viewportMaxRegion = ImGui::GetWindowContentRegionMax();
		auto viewportOffset = ImGui::GetWindowPos();
		m_ViewportBounds[0] = { viewportMinRegion.x + viewportOffset.x, viewportMinRegion.y + viewportOffset.y };
		m_ViewportBounds[1] = { viewportMaxRegion.x + viewportOffset.x, viewportMaxRegion.y + viewportOffset.y };

		m_ViewportFocused = ImGui::IsWindowFocused();
		m_ViewportHovered = ImGui::IsWindowHovered();
		m_EditorCamera.m_ViewportFocused = m_ViewportFocused;
		m_EditorCamera.m_ViewportHovered = m_ViewportHovered;

		//Application::Get().GetImGuiLayer()->BlockEvents(!m_ViewportFocused && !m_ViewportHovered);
		Application::Get().GetImGuiLayer()->BlockEvents(false);

		ImVec2 viewportPanelSize = ImGui::GetContentRegionAvail();
		m_ViewportSize = { viewportPanelSize.x, viewportPanelSize.y };

		if (m_SceneState != SceneState::Play || m_EditorScene->GetPrimaryCameraEntity())
		{
			uint64_t textureID = m_Framebuffer->GetColorAttachmentRendererID();
			ImGui::Image(reinterpret_cast<void*>(textureID), ImVec2{ m_ViewportSize.x, m_ViewportSize.y }, ImVec2{ 0, 1 }, ImVec2{ 1, 0 });
		}
		else
		{
			ImGui::SetWindowFontScale(5);
			std::string text = "No Active";
			ImVec2 size = ImGui::CalcTextSize(text.c_str());
			ImGui::SetCursorPosX((m_ViewportSize.x * 0.5f) - (size.x * 0.5f));
			ImGui::SetCursorPosY((m_ViewportSize.y * 0.5f) - ((size.y * 0.5f) * 2.5f));
			ImGui::Text(text.c_str());

			text = "Camera Rendering";
			size = ImGui::CalcTextSize(text.c_str());
			ImGui::SetCursorPosX((m_ViewportSize.x * 0.5f) - (size.x * 0.5f));
			ImGui::SetCursorPosY((m_ViewportSize.y * 0.5f) - ((size.y * 0.5f) / 2.5f));
			ImGui::Text(text.c_str());
			ImGui::SetWindowFontScale(1);
		}

		if (ImGui::BeginDragDropTarget())
		{
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM"))
			{
#ifndef SPIRIT_PLATFORM_LINUX
				const wchar_t* path = (const wchar_t*)payload->Data;
#else
				const char* path = (char*)payload->Data;
#endif

				// TODO: Move this to a method somewhere and call it here, when double-clicking a file in the content browser, or when dropping a file onto an entity in the hierarchy
				std::filesystem::path parentPath = std::filesystem::path(path).parent_path();
				std::filesystem::path filePath = std::filesystem::path(path);
				std::filesystem::path relativePath = std::filesystem::path(g_AssetPath) / path;

				if (FileTypes::IsSceneType(filePath.extension().string()))
					OpenScene(relativePath);
				if (FileTypes::IsPrefabType(filePath.extension().string()))
					OpenPrefab(relativePath);
				else if (FileTypes::IsImageType(filePath.extension().string()))
				{
					std::filesystem::path texturePath = relativePath;
					Ref<Texture2D> texture = Texture2D::Create(texturePath.string());
					if (texture->IsLoaded())
					{
						if (m_HoveredEntity && m_HoveredEntity.HasComponent<SpriteRendererComponent>())
							m_HoveredEntity.GetComponent<SpriteRendererComponent>().Texture = texture;
						else if (!m_HoveredEntity)
						{
							Entity entity = m_ActiveScene->CreateEntity(filePath.filename().replace_extension().string());
							auto& spriteRenderer = entity.AddComponent<SpriteRendererComponent>();
							spriteRenderer.Texture = texture;
							auto& collider = entity.AddComponent<BoxCollider2DComponent>();
							auto& rigidbody = entity.AddComponent<Rigidbody2DComponent>();
							m_SceneHierarchyPanel.SetSelectedEntity(entity);
						}
					}
					else
						SPIRIT_WARN("Could not load texture {0}", texturePath.filename().string());
				}
				else if (FileTypes::IsModelType(filePath.extension().string()))
				{
					std::filesystem::path modelPath = relativePath;
					Ref<Model> model = Model::Create(modelPath.string());

					Entity entity = m_ActiveScene->CreateEntity(filePath.filename().replace_extension().string());
					auto& meshRenderer = entity.AddComponent<MeshRendererComponent>();
					meshRenderer.Mesh = model;
					//auto& collider = sprite.AddComponent<BoxColliderComponent>();
					//auto& rigidbody = sprite.AddComponent<RigidbodyComponent>();
					m_SceneHierarchyPanel.SetSelectedEntity(entity);
				}
				else if (FileTypes::IsAudioType(filePath.extension().string()))
				{
					std::filesystem::path audioPath = relativePath;

					Entity entity = m_ActiveScene->CreateEntity(filePath.filename().replace_extension().string());
					auto& audioSource = entity.AddComponent<AudioSourceComponent>();

					AudioSource src = AudioSource::LoadFromFile(audioPath.string(), audioSource.Spatial);
					src.SetGain(audioSource.Gain);
					src.SetPitch(audioSource.Pitch);
					src.SetSpatial(audioSource.Spatial);
					src.SetLoop(audioSource.Loop);
					audioSource.m_Source = src;

					m_SceneHierarchyPanel.SetSelectedEntity(entity);
				}
			}
			ImGui::EndDragDropTarget();
		}

		UI_Gizmos();

		ImGui::End();
		ImGui::PopStyleVar();
	}

	void EditorLayer::UI_Gizmos()
	{
		// Gizmos
		Entity selectedEntity = m_SceneHierarchyPanel.GetSelectedEntity();
		if (selectedEntity && m_GizmoType != -1 && !Input::IsKeyPressed(Key::LeftAlt))
		{
			ImGuizmo::SetOrthographic(false);
			ImGuizmo::SetDrawlist();
			float windowWidth = (float)ImGui::GetWindowWidth();
			float windowHeight = (float)ImGui::GetWindowHeight();
			ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, windowWidth, windowHeight);

			// Editor camera
			const glm::mat4& cameraProjection = m_EditorCamera.GetProjection();
			glm::mat4 cameraView = m_EditorCamera.GetViewMatrix();

			// Entity transform
			auto& tc = selectedEntity.GetComponent<TransformComponent>();
			glm::mat4 transform = tc.GetTransform();

			// Snapping
			bool snap = Input::IsKeyPressed(Key::LeftControl) || Input::IsKeyPressed(Key::RightControl);
			float snapValue = 0.5f; // SSnap to 0.5m for translation/scale
			// Snap to 45 degrees for rotation
			if (m_GizmoType == ImGuizmo::OPERATION::ROTATE)
				snapValue = 45.0f;

			float snapValues[3] = { snapValue, snapValue, snapValue };

			ImGuizmo::Manipulate(glm::value_ptr(cameraView), glm::value_ptr(cameraProjection),
				(ImGuizmo::OPERATION)m_GizmoType, ImGuizmo::LOCAL, glm::value_ptr(transform),
				nullptr, snap ? snapValues : nullptr);

			if (ImGuizmo::IsUsing())
			{
				glm::vec3 translation, rotation, scale;
				Math::DecomposeTransform(transform, translation, rotation, scale);

				tc.Translation = translation;

				glm::vec3 deltaRotation = rotation - tc.Rotation;
				tc.Rotation += deltaRotation;

				tc.Scale = scale;
			}
		}
	}

	void EditorLayer::UI_Toolbar()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 2));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(0, 0));
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
		auto& colors = ImGui::GetStyle().Colors;
		const auto& buttonHovered = colors[ImGuiCol_ButtonHovered];
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(buttonHovered.x, buttonHovered.y, buttonHovered.z, 0.5f));
		const auto& buttonActive = colors[ImGuiCol_ButtonActive];
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(buttonActive.x, buttonActive.y, buttonActive.z, 0.5f));

		ImGui::Begin("##toolbar", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

		bool toolbarEnabled = (bool)m_ActiveScene;

		ImVec4 tintColor = ImVec4(1, 1, 1, 1);
		if (!toolbarEnabled)
			tintColor.w = 0.5f;

		float size = ImGui::GetWindowHeight() - 4.0f;
		{
			Ref<Texture2D> icon = (m_SceneState == SceneState::Edit || m_SceneState == SceneState::Simulate) ? m_IconPlay : m_IconStop;
			ImGui::SetCursorPosX((ImGui::GetWindowContentRegionMax().x * 0.5f) - (size * 0.5f));
			ImGui::SetCursorPosY((ImGui::GetWindowContentRegionMax().y * 0.5f) - (size * 0.5f));
			if (ImGui::ImageButton((ImTextureID)icon->GetRendererID(), ImVec2(size, size), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0.0f, 0.0f, 0.0f, 0.0f), tintColor) && toolbarEnabled)
			{
				if (m_SceneState == SceneState::Edit || m_SceneState == SceneState::Simulate)
					OnScenePlay();
				else if (m_SceneState == SceneState::Play)
					OnSceneStop();
			}
		}
		ImGui::SameLine();
		{
			Ref<Texture2D> icon = (m_SceneState == SceneState::Edit || m_SceneState == SceneState::Play) ? m_IconSimulate : m_IconStop;
			//ImGui::SetCursorPosX((ImGui::GetWindowContentRegionMax().x * 0.5f) - (size * 0.5f));
			//ImGui::SetCursorPosY((ImGui::GetWindowContentRegionMax().y * 0.5f) - (size * 0.5f));
			if (ImGui::ImageButton((ImTextureID)icon->GetRendererID(), ImVec2(size, size), ImVec2(0, 0), ImVec2(1, 1), 0, ImVec4(0.0f, 0.0f, 0.0f, 0.0f), tintColor) && toolbarEnabled)
			{
				if (m_SceneState == SceneState::Edit || m_SceneState == SceneState::Play)
					OnSceneSimulate();
				else if (m_SceneState == SceneState::Simulate)
					OnSceneStop();
			}
		}
		ImGui::PopStyleVar(2);
		ImGui::PopStyleColor(3);
		ImGui::End();
	}

	void EditorLayer::OnEvent(Event& e)
	{
		m_CameraController.OnEvent(e);
		if (m_SceneState == SceneState::Edit)
			m_EditorCamera.OnEvent(e);

		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<KeyPressedEvent>(SPIRIT_BIND_EVENT_FN(EditorLayer::OnKeyPressed));
		dispatcher.Dispatch<MouseButtonPressedEvent>(SPIRIT_BIND_EVENT_FN(EditorLayer::OnMouseButtonPressed));
	}

	bool EditorLayer::OnKeyPressed(KeyPressedEvent& e)
	{
		// Shortcuts
		bool control = Input::IsKeyPressed(Key::LeftControl) || Input::IsKeyPressed(Key::RightControl);
		bool shift = Input::IsKeyPressed(Key::LeftShift) || Input::IsKeyPressed(Key::RightShift);

		if (e.GetRepeatCount() > 0 && !(control && e.GetKeyCode() == Key::D))
			return false;

		switch (e.GetKeyCode())
		{
		case Key::N:
		{
			if (control)
				NewScene();

			break;
		}
		case Key::O:
		{
			if (control)
				OpenScene();

			break;
		}
		case Key::S:
		{
			if (control)
			{
				if (shift)
					SaveSceneAs();
				else
					SaveScene();
			}

			break;
		}

		case Key::D:
		{
			if (control)
				DuplicateSelectedEntity();

			break;
		}

		case Key::Delete:
		{
			DeleteSelectedEntity();

			break;
		}

		// Gizmos
		case Key::Q:
			if (!ImGuizmo::IsUsing())
				m_GizmoType = -1;
			break;
		case Key::W:
			if (!ImGuizmo::IsUsing())
				m_GizmoType = ImGuizmo::OPERATION::TRANSLATE;
			break;
		case Key::E:
			if (!ImGuizmo::IsUsing())
				m_GizmoType = ImGuizmo::OPERATION::ROTATE;
			break;
		case Key::R:
			if (!ImGuizmo::IsUsing())
				m_GizmoType = ImGuizmo::OPERATION::SCALE;
			break;

		case Key::F:
		{
			//if (SelectionManager::GetSelectionCount(SelectionContext::Scene) == 0)
			//	break;

			//// TODO: Maybe compute average location to focus on? Or maybe cycle through all the selected entities?
			//UUID selectedEntityID = SelectionManager::GetSelections(SelectionContext::Scene).front();
			//Entity selectedEntity = m_CurrentScene->GetEntityWithUUID(selectedEntityID);
			//m_EditorCamera.Focus(selectedEntity.Transform().Translation);
			//break;
		}
		}

		return false;
	}

	bool EditorLayer::OnMouseButtonPressed(MouseButtonPressedEvent& e)
	{
		if (e.GetMouseButton() == Mouse::ButtonLeft && !ImGuizmo::IsUsing() && !ImGuizmo::IsOver() && !Input::IsKeyPressed(Key::LeftAlt) && m_ViewportHovered && m_ViewportFocused)
			m_SceneHierarchyPanel.SetSelectedEntity(m_HoveredEntity);
		return false;
	}

	void EditorLayer::OnOverlayRender()
	{
		if (m_SceneState == SceneState::Play)
		{
			Entity camera = m_ActiveScene->GetPrimaryCameraEntity();
			if (!camera)
				return;

			Renderer2D::BeginScene(camera.GetComponent<CameraComponent>().Camera, camera.GetComponent<TransformComponent>().GetTransform());
			Renderer3D::BeginScene(camera.GetComponent<CameraComponent>().Camera, camera.GetComponent<TransformComponent>().GetTransform());
		}
		else
		{
			Renderer2D::BeginScene(m_EditorCamera);
			Renderer3D::BeginScene(m_EditorCamera);
		}

		if (m_ShowPhysicsColliders)
		{
			// Calculate z index for translation
			float zIndex = 0.001f;
			glm::vec3 cameraForwardDirection = m_EditorCamera.GetForwardDirection();
			glm::vec3 projectionCollider = cameraForwardDirection * glm::vec3(zIndex);

			// Box Colliders
			{
				auto view = m_ActiveScene->GetAllEntitiesWith<TransformComponent, BoxCollider2DComponent>();
				for (auto entity : view)
				{
					auto [tc, bc2d] = view.get<TransformComponent, BoxCollider2DComponent>(entity);

					glm::vec3 scale = tc.Scale * glm::vec3(bc2d.Size * 2.0f, 1.0f);

					glm::mat4 transform = glm::translate(glm::mat4(1.0f), tc.Translation)
						* glm::rotate(glm::mat4(1.0f), tc.Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f))
						* glm::translate(glm::mat4(1.0f), glm::vec3(bc2d.Offset, -projectionCollider.z))
						* glm::rotate(glm::mat4(1.0f), bc2d.Rotation, glm::vec3(0.0f, 0.0f, 1.0f))
						* glm::scale(glm::mat4(1.0f), scale);

					Renderer2D::DrawRect(transform, glm::vec4(0, 1, 0, 1));
				}
			}

			// Circle Colliders
			{
				auto view = m_ActiveScene->GetAllEntitiesWith<TransformComponent, CircleCollider2DComponent>();

				// Calculate z index for translation
				float zIndex = 0.001f;
				glm::vec3 cameraForwardDirection = m_EditorCamera.GetForwardDirection();
				glm::vec3 projectionCollider = cameraForwardDirection * glm::vec3(zIndex);

				for (auto entity : view)
				{
					auto [tc, cc2d] = view.get<TransformComponent, CircleCollider2DComponent>(entity);

					glm::vec3 scale = tc.Scale * glm::vec3(cc2d.Radius * 2.0f);

					glm::mat4 transform = glm::translate(glm::mat4(1.0f), tc.Translation)
						* glm::rotate(glm::mat4(1.0f), tc.Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f))
						* glm::translate(glm::mat4(1.0f), glm::vec3(cc2d.Offset, -projectionCollider.z))
						* glm::scale(glm::mat4(1.0f), glm::vec3(scale.x, scale.x, scale.z));

					Renderer2D::DrawCircle(transform, glm::vec4(0, 1, 0, 1), 0.01f);
				}
			}
		}

		// Draw selected entity outline 
		if (Entity selectedEntity = m_SceneHierarchyPanel.GetSelectedEntity())
		{
			const TransformComponent& transform = selectedEntity.GetComponent<TransformComponent>();
			Renderer2D::DrawRect(transform.GetTransform(), glm::vec4(1.0f, 0.5f, 0.0f, 1.0f));
		}

		Renderer2D::EndScene();
		Renderer3D::EndScene();
	}

	void EditorLayer::OpenPrefab(const std::filesystem::path& path)
	{
		if (!FileTypes::IsPrefabType(path.extension().string()))
		{
			SPIRIT_WARN("Could not load {0} - not a prefab file", path.filename().string());
			return;
		}

		SceneSerializer serializer(m_ActiveScene);
		serializer.DeserializePrefab(path.string());
	}

	void EditorLayer::NewScene()
	{
		if (m_SceneState != SceneState::Edit)
			OnSceneStop();

		m_EditorScene = CreateRef<Scene>();
		m_EditorScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
		m_SceneHierarchyPanel.SetContext(m_EditorScene);

		m_ActiveScene = m_EditorScene;
		m_EditorSceneFilePath = "";

		Application::Get().GetWindow().SetTitle("Spiritual - " + m_ActiveScene->Name + " - " + g_ProjectName);
	}

	void EditorLayer::OpenScene()
	{
		NFD::UniquePath filepath;
		nfdfilteritem_t filter[1] = { FileTypes::GetSceneFilter() };
		nfdresult_t result = NFD::OpenDialog(filepath, filter, 1);

		if (result == NFD_OKAY)
			OpenScene(filepath.get());
	}

	void EditorLayer::OpenScene(const std::filesystem::path& path)
	{
		if (m_SceneState != SceneState::Edit)
			OnSceneStop();

		if (!FileTypes::IsSceneType(path.extension().string()))
		{
			SPIRIT_WARN("Could not load {0} - not a scene file", path.filename().string());
			return;
		}

		Ref<Scene> newScene = CreateRef<Scene>();
		SceneSerializer serializer(newScene);
		if (serializer.DeserializeScene(path.string()))
		{
			newScene = Scene::ReversedCopy(newScene);
			m_EditorScene = newScene;
			m_ActiveScene = m_EditorScene;
			m_ActiveScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
			m_SceneHierarchyPanel.SetContext(m_ActiveScene);

			m_EditorSceneFilePath = path.string();
		}

		Application::Get().GetWindow().SetTitle("Spiritual - " + m_ActiveScene->Name + " - " + g_ProjectName);

		if (m_RichPresenceEnabled && !m_LoadedDiscord)
		{
			auto result = discord::Core::Create(964977882315231314, DiscordCreateFlags_NoRequireDiscord, &core);
			m_LoadedDiscord = core != nullptr;
		}
		if (m_RichPresenceEnabled && m_LoadedDiscord)
		{
			discord::Activity activity{};
			activity.SetState(m_DisplayProjectName ? g_ProjectName.c_str() : "");
			activity.SetDetails(m_DisplaySceneName ? m_ActiveScene->Name.c_str() : "");
			activity.GetAssets().SetLargeImage("logo");
			if (m_ResetTimestampOnSceneChange)
				m_TimestampStart = std::time(0);
			activity.GetTimestamps().SetStart(m_TimestampStart);
			core->ActivityManager().UpdateActivity(activity, [](discord::Result result) {});
		}
	}

	void EditorLayer::SaveScene()
	{
		if (m_EditorSceneFilePath.empty())
			SaveSceneAs();
		else
		{
			if (!FileTypes::IsSceneType(std::filesystem::path(m_EditorSceneFilePath).extension().string()))
			{
				SPIRIT_WARN("Could not save {0} - not a scene file", std::filesystem::path(m_EditorSceneFilePath).filename().string());
				return;
			}

			SceneSerializer serializer(m_ActiveScene);
			serializer.SerializeScene(m_EditorSceneFilePath);

			if (m_ActiveScene->Name != std::filesystem::path(m_EditorSceneFilePath).filename().replace_extension().string())
				m_ActiveScene->Name = std::filesystem::path(m_EditorSceneFilePath).filename().replace_extension().string();

			Application::Get().GetWindow().SetTitle("Spiritual - " + m_ActiveScene->Name + " - " + g_ProjectName);
		}
	}

	void EditorLayer::SaveSceneAs()
	{
		NFD::UniquePath filepath;
		nfdfilteritem_t filter[1] = { FileTypes::GetSceneFilter() };
		nfdresult_t result = NFD::SaveDialog(filepath, filter, 1);

		if (result == NFD_OKAY)
		{
			m_EditorSceneFilePath = filepath.get();

			if (!FileTypes::IsSceneType(std::filesystem::path(m_EditorSceneFilePath).extension().string()))
			{
				SPIRIT_WARN("Could not save {0} - not a scene file", std::filesystem::path(m_EditorSceneFilePath).filename().string());
				return;
			}

			SceneSerializer serializer(m_ActiveScene);
			serializer.SerializeScene(m_EditorSceneFilePath);

			if (m_ActiveScene->Name != std::filesystem::path(m_EditorSceneFilePath).filename().replace_extension().string())
				m_ActiveScene->Name = std::filesystem::path(m_EditorSceneFilePath).filename().replace_extension().string();

			Application::Get().GetWindow().SetTitle("Spiritual - " + m_ActiveScene->Name + " - " + g_ProjectName);
		}
	}

	void EditorLayer::OnScenePlay()
	{
		if (m_SceneState == SceneState::Simulate)
			OnSceneStop();

		if (!m_EditorScene->GetPrimaryCameraEntity())
			SPIRIT_WARN("No active camera rendering!");

		m_SceneState = SceneState::Play;

		// Make a copy of the Editor scene
		m_RuntimeScene = Scene::Copy(m_EditorScene);

		m_ActiveScene = m_RuntimeScene;
		m_ActiveScene->OnRuntimeStart();
		m_SceneHierarchyPanel.SetContext(m_ActiveScene);
	}

	void EditorLayer::OnSceneSimulate()
	{
		if (m_SceneState == SceneState::Play)
			OnSceneStop();

		m_SceneState = SceneState::Simulate;

		m_ActiveScene = Scene::Copy(m_EditorScene);
		m_ActiveScene->OnSimulationStart();

		m_SceneHierarchyPanel.SetContext(m_ActiveScene);
	}

	void EditorLayer::OnSceneStop()
	{
		SPIRIT_CORE_ASSERT(m_SceneState == SceneState::Play || m_SceneState == SceneState::Simulate);

		if (m_SceneState == SceneState::Play)
			m_ActiveScene->OnRuntimeStop();
		else if (m_SceneState == SceneState::Simulate)
			m_ActiveScene->OnSimulationStop();

		m_SceneState = SceneState::Edit;

		m_ActiveScene = m_EditorScene;
		m_RuntimeScene = nullptr;
		m_SceneHierarchyPanel.SetContext(m_ActiveScene);
	}

	void EditorLayer::DuplicateSelectedEntity()
	{
		if (m_SceneState != SceneState::Edit)
			return;

		Entity selectedEntity = m_SceneHierarchyPanel.GetSelectedEntity();
		if (selectedEntity)
			m_SceneHierarchyPanel.SetSelectedEntity(m_EditorScene->DuplicateEntity(selectedEntity));
	}

	void EditorLayer::DeleteSelectedEntity()
	{
		if (m_SceneState != SceneState::Edit)
			return;

		Entity selectedEntity = m_SceneHierarchyPanel.GetSelectedEntity();
		if (selectedEntity)
		{
			m_ActiveScene->DestroyEntity(m_SceneHierarchyPanel.GetSelectionContext());
			m_SceneHierarchyPanel.SetSelectedEntity({});
		}
	}

	void EditorLayer::SafeExit()
	{
		// TODO: Check if there are any unsaved changes

		Application::Get().Close();
	}

}
