#include "SceneHierarchyPanel.h"

#include "Spirit/Scripting/ScriptEngine.h"

#include "Spirit/Scene/Components.h"
#include "Spirit/Core/UUID.h"
#include "Spirit/Scene/SceneSerializer.h"
#include "Spirit/Renderer/Model.h"
#include "Spirit/Utils/FileUtils.h"
#include "Spirit/Utils/ScriptWrapperUtils.h"
#include "../Utils/ImUtils.h"

#include <imgui/misc/cpp/imgui_stdlib.h>
#include <glm/gtc/type_ptr.hpp>

#include <cstring>

/* The Microsoft C++ compiler is non-compliant with the C++ standard and needs
 * the following definition to disable a security warning on std::strncpy().
 */
#ifdef _MSVC_LANG
#define _CRT_SECURE_NO_WARNINGS
#endif

namespace Spirit {

	extern std::filesystem::path g_AssetPath;

	bool ColorEdit4(std::string label, float* col)
	{
		ImGui::Text(label.c_str());
		auto lb = "##" + label;
		return ImGui::ColorEdit4(lb.c_str(), col);
	}

	SceneHierarchyPanel::SceneHierarchyPanel(const Ref<Scene>& context)
	{
		SetContext(context);
	}

	void SceneHierarchyPanel::SetContext(const Ref<Scene>& context)
	{
		m_Context = context;
		m_SelectionContext = {};
	}

	void SceneHierarchyPanel::SetPrefab(const Entity& context)
	{
		m_PrefabSelectionContext = context;
		m_PrefabEntitySelectionContext = context;
	}

	void SceneHierarchyPanel::SetContext(const Entity& context)
	{
		m_PrefabEntitySelectionContext = context;
	}

	Entity SceneHierarchyPanel::GetSelectionContext()
	{
		return m_SelectionContext;
	}

	Entity SceneHierarchyPanel::GetPrefabSelectionContext()
	{
		return m_PrefabSelectionContext;
	}

	void SceneHierarchyPanel::OnImGuiRender()
	{
		ImGui::Begin("Hierarchy");

		ImGuiContext& g = *GImGui;
		ImGuiWindow* window = g.CurrentWindow;
		if (!(g.LastItemData.StatusFlags & ImGuiItemStatusFlags_HoveredRect))
		{
			ImGuiWindow* hovered_window = g.HoveredWindow;
			if (hovered_window != NULL && window->RootWindow == hovered_window->RootWindow)
			{
				const ImRect& rect = window->Viewport->GetMainRect();
				ImGuiID id = window->ViewportId;

				if (ImGui::BeginDragDropTargetCustom(rect, id))
				{
					if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM"))
					{
						const wchar_t* path = (const wchar_t*)payload->Data;

						std::filesystem::path parentPath = std::filesystem::path(path).parent_path();
						std::filesystem::path filePath = std::filesystem::path(path);
						std::filesystem::path relativePath = std::filesystem::path(g_AssetPath) / path;

						if (FileTypes::IsPrefabType(filePath.extension().string()))
						{
							m_PrefabContext = CreateRef<Scene>();
							SceneSerializer serializer(m_PrefabContext);
							if (Entity entity = serializer.DeserializePrefab(relativePath.string()))
							{
								SetPrefab(entity);
								m_HierarchyScope = HierarchyScope::Prefab;
							}
						}
					}
					ImGui::EndDragDropTarget();
				}
			}
		}

		if (m_HierarchyScope == HierarchyScope::Scene)
		{
			m_Context->m_Registry.each([&](auto entityID)
				{
					Entity entity{ entityID , m_Context.get() };
					DrawEntityNode(entity, nullptr);
				});

			if (ImGui::IsMouseDown(Mouse::ButtonLeft) && ImGui::IsWindowHovered())
				m_SelectionContext = {};

			// Right-click on blank space
			if (ImGui::BeginPopupContextWindow(0, 1, false))
			{
				EntityCreation();

				ImGui::EndPopup();
			}
		}
		else
		{
			if ((ImGui::Button("<-") && ImGui::IsWindowHovered()) || (ImGui::IsKeyDown(Key::Escape) && ImGui::IsWindowFocused()))
			{
				m_PrefabSelectionContext = {};
				m_PrefabEntitySelectionContext = {};
				m_HierarchyScope = HierarchyScope::Scene;
			}

			DrawEntityNode(m_PrefabSelectionContext, nullptr);
		}

		ImGui::End();

		ImGui::Begin("Inspector");

		if (m_HierarchyScope == HierarchyScope::Scene)
		{
			if (m_SelectionContext)
				DrawComponents(m_SelectionContext);
		}
		else
		{
			if (m_PrefabEntitySelectionContext)
				DrawComponents(m_PrefabEntitySelectionContext);
		}
		ImGui::End();
	}

	void SceneHierarchyPanel::EntityCreation()
	{
		if (ImGui::MenuItem("Create Empty Entity"))
			m_SelectionContext = m_Context->CreateEntity("Empty Entity");

		if (ImGui::MenuItem("Create Square"))
		{
			Entity square = m_Context->CreateEntity("Square");
			auto& spriteRenderer = square.AddComponent<SpriteRendererComponent>();
			auto& collider = square.AddComponent<BoxCollider2DComponent>();
			auto& rigidbody = square.AddComponent<Rigidbody2DComponent>();
			m_SelectionContext = square;
		}

		if (ImGui::MenuItem("Create Circle"))
		{
			Entity circle = m_Context->CreateEntity("Circle");
			auto& circleRenderer = circle.AddComponent<CircleRendererComponent>();
			auto& collider = circle.AddComponent<CircleCollider2DComponent>();
			auto& rigidbody = circle.AddComponent<Rigidbody2DComponent>();
			m_SelectionContext = circle;
		}

		if (ImGui::MenuItem("Create Mesh"))
		{
			Entity mesh = m_Context->CreateEntity("Mesh");
			auto& meshRenderer = mesh.AddComponent<MeshRendererComponent>();
			// TODO: Add 3D physics
			// auto& collider = mesh.AddComponent<BoxColliderComponent>();
			// auto& rigidbody = mesh.AddComponent<RigidbodyComponent>();

			m_SelectionContext = mesh;
		}

		if (ImGui::MenuItem("Create Directional Light"))
		{
			Entity light = m_Context->CreateEntity("Directional Light");
			light.AddComponent<DirectionalLightComponent>();

			m_SelectionContext = light;
		}

		if (ImGui::MenuItem("Create Point Light"))
		{
			Entity light = m_Context->CreateEntity("Point Light");
			light.AddComponent<PointLightComponent>();

			m_SelectionContext = light;
		}

		if (ImGui::MenuItem("Create Spot Light"))
		{
			Entity light = m_Context->CreateEntity("Spot Light");
			light.AddComponent<SpotLightComponent>();

			m_SelectionContext = light;
		}

		if (ImGui::MenuItem("Create Camera"))
		{
			Entity camera = m_Context->CreateEntity("Camera");
			camera.AddComponent<CameraComponent>();
			camera.AddComponent<AudioListenerComponent>();
			m_SelectionContext = camera;
		}

		if (ImGui::MenuItem("Create Audio Source"))
		{
			Entity source = m_Context->CreateEntity("Audio Source");
			source.AddComponent<AudioSourceComponent>();
			m_SelectionContext = source;
		}

		if (ImGui::MenuItem("Create Audio Listener"))
		{
			Entity listener = m_Context->CreateEntity("Audio Listener");
			listener.AddComponent<AudioListenerComponent>();
			m_SelectionContext = listener;
		}
	}

	void SceneHierarchyPanel::DrawEntityNode(Entity& entity, UUID* requiredParent, ImGuiTreeNodeFlags _flags)
	{
		if (entity.Parent != requiredParent)
			return;

		auto& name = entity.GetName();
		auto& tag = entity.GetTag();

		ImGuiTreeNodeFlags flags = _flags;
		if (flags == ImGuiTreeNodeFlags_None)
		{
			flags = ((m_SelectionContext == entity) ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow;
			flags |= ImGuiTreeNodeFlags_SpanAvailWidth;
		}
		bool opened = ImGui::TreeNodeEx((void*)(uint64_t)(uint32_t)entity, flags, name.c_str());
		if (m_HierarchyScope == HierarchyScope::Scene)
		{
			if (ImGui::IsItemClicked())
				m_SelectionContext = entity;

			if (ImGui::BeginDragDropSource())
			{
				ImGui::SetDragDropPayload("SCENE_HIERARCHY_ITEM", &entity, sizeof(entity) + 1);
				ImGui::EndDragDropSource();
			}
			if (ImGui::BeginDragDropTarget())
			{
				if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_HIERARCHY_ITEM"))
				{
					auto e = ((Entity*)payload->Data);
					auto u = e->GetUUID();
					Entity dropped = entity.GetScene().GetEntityByID(u);

					if (entity != dropped)
					{
						if (dropped.Parent)
						{
							Entity parent = dropped.GetScene().GetEntityByID(*dropped.Parent);
							if (parent)
							{
								for (int i = 0; i < parent.Children.size(); i++)
								{
									if (parent.Children[i] == dropped.GetUUID())
										parent.Children.erase(parent.Children.begin() + i);
								}
							}
							entity.Children.push_back(dropped.GetUUID());
							entity.UpdateRelationships();
						}
					}
				}
				ImGui::EndDragDropTarget();
			}

			bool entityDeleted = false;
			if (ImGui::BeginPopupContextItem())
			{
				if (ImGui::MenuItem("Delete"))
					entityDeleted = true;

				if (ImGui::MenuItem("Duplicate"))
					SetSelectedEntity(m_Context->DuplicateEntity(m_SelectionContext));

				ImGui::Separator();

				EntityCreation();

				ImGui::EndPopup();
			}

			if (opened)
			{
				flags = ImGuiTreeNodeFlags_OpenOnArrow;

				for (auto child : m_Context->GetEntities())
				{
					DrawEntityNode(child, &entity.GetUUID()/*, flags*/);
				}
				ImGui::TreePop();
			}

			if (entityDeleted)
			{
				m_Context->DestroyEntity(entity);
				if (m_SelectionContext == entity)
					m_SelectionContext = {};
			}
		}
		else
		{
			if (ImGui::IsItemClicked())
				m_PrefabEntitySelectionContext = entity;

			if (ImGui::BeginDragDropSource())
			{
				ImGui::SetDragDropPayload("SCENE_HIERARCHY_ITEM", &entity, sizeof(entity) + 1);
				ImGui::EndDragDropSource();
			}
			if (ImGui::BeginDragDropTarget())
			{
				if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_HIERARCHY_ITEM"))
				{
					Entity* dropped = (Entity*)payload->Data;

					if (&entity != dropped)
					{
						entity.Children.push_back(dropped->GetUUID());
						entity.UpdateRelationships();
					}
				}
				ImGui::EndDragDropTarget();
			}

			bool entityDeleted = false;
			if (ImGui::BeginPopupContextItem())
			{
				if (m_PrefabSelectionContext != m_PrefabEntitySelectionContext && ImGui::MenuItem("Delete"))
					entityDeleted = true;

				if (ImGui::MenuItem("Duplicate"))
				{
					Entity newEntity = m_Context->DuplicateEntity(m_PrefabEntitySelectionContext);
					if (m_PrefabSelectionContext == m_PrefabEntitySelectionContext)
					{
						entity.Children.push_back(newEntity.GetUUID());
						entity.UpdateRelationships();
					}
					SetSelectedEntity(newEntity);
				}

				ImGui::Separator();

				EntityCreation();

				ImGui::EndPopup();
			}

			if (opened)
			{
				flags = ImGuiTreeNodeFlags_OpenOnArrow;

				for (auto child : m_Context->GetEntities())
				{
					DrawEntityNode(child, &entity.GetUUID()/*, flags*/);
				}
				ImGui::TreePop();
			}

			if (entityDeleted)
			{
				m_Context->DestroyEntity(entity);
				if (m_PrefabEntitySelectionContext == entity)
					m_PrefabEntitySelectionContext = {};
			}
		}
	}

	template<typename T, typename UIFunction>
	static void DrawComponent(const std::string& name, Entity entity, UIFunction uiFunction)
	{
		const ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_AllowItemOverlap | ImGuiTreeNodeFlags_FramePadding;
		if (entity.HasComponent<T>())
		{
			auto& component = entity.GetComponent<T>();
			ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();

			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{ 4, 4 });
			float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			ImGui::Separator();
			bool open = ImGui::TreeNodeEx((void*)typeid(T).hash_code(), treeNodeFlags, name.c_str());
			ImGui::PopStyleVar(
			);
			ImGui::SameLine(contentRegionAvailable.x - lineHeight * 0.5f);
			if (ImGui::Button("+", ImVec2{ lineHeight, lineHeight }))
			{
				ImGui::OpenPopup("ComponentSettings");
			}

			bool removeComponent = false;
			if (ImGui::BeginPopup("ComponentSettings"))
			{
				if (ImGui::MenuItem("Remove component"))
					removeComponent = true;

				ImGui::EndPopup();
			}

			if (open)
			{
				uiFunction(component);
				ImGui::TreePop();
			}

			if (removeComponent)
				entity.RemoveComponent<T>();
		}
	}

	void SceneHierarchyPanel::DrawComponents(Entity entity)
	{
		if (entity.HasComponent<TagComponent>())
		{
			auto& name = entity.GetComponent<TagComponent>().Name;
			auto& tag = entity.GetComponent<TagComponent>().Tag;

			char nameBuffer[256];
			char tagBuffer[256];
			memset(nameBuffer, 0, sizeof(nameBuffer));
			memset(tagBuffer, 0, sizeof(tagBuffer));
			std::strncpy(nameBuffer, name.c_str(), sizeof(nameBuffer));
			std::strncpy(tagBuffer, tag.c_str(), sizeof(tagBuffer));

			ImGui::PushItemWidth(-1);
			if (ImGui::InputText("##Name", nameBuffer, sizeof(nameBuffer)))
			{
				name = std::string(nameBuffer);
			}
			ImGui::PopItemWidth();

			if (ImGui::InputText("##Tag", tagBuffer, sizeof(tagBuffer)))
			{
				tag = std::string(tagBuffer);
			}

			ImGui::SameLine();

			if (ImGui::Button("Add Component", ImVec2(-1, 0)))
				ImGui::OpenPopup("AddComponent");

			if (ImGui::BeginPopup("AddComponent"))
			{
				DisplayAddComponentEntry<CameraComponent>("Camera");

				// Add Component entry to allow multiple scripts
				{
					if (m_HierarchyScope == HierarchyScope::Scene)
					{
						if (ImGui::MenuItem("Script"))
						{
							ScriptWrapperComponent wrapper = m_SelectionContext.AddOrGetComponent<ScriptWrapperComponent>();
							ScriptWrapperUtils::AddComponent(m_SelectionContext, ScriptComponent());
							ImGui::CloseCurrentPopup();
						}
					}
					else
					{
						if (ImGui::MenuItem("Script"))
						{
							ScriptWrapperComponent wrapper = m_SelectionContext.AddOrGetComponent<ScriptWrapperComponent>();
							ScriptWrapperUtils::AddComponent(m_SelectionContext, ScriptComponent());
							ImGui::CloseCurrentPopup();
						}
					}
				}

				DisplayAddComponentEntry<SpriteRendererComponent>("Sprite Renderer");
				DisplayAddComponentEntry<CircleRendererComponent>("Circle Renderer");
				DisplayAddComponentEntry<MeshRendererComponent>("Mesh Renderer");
				DisplayAddComponentEntry<DirectionalLightComponent>("Directional Light");
				DisplayAddComponentEntry<PointLightComponent>("Point Light");
				DisplayAddComponentEntry<SpotLightComponent>("Spot Light");
				DisplayAddComponentEntry<Rigidbody2DComponent>("Rigidbody 2D");
				DisplayAddComponentEntry<BoxCollider2DComponent>("Box Collider 2D");
				DisplayAddComponentEntry<CircleCollider2DComponent>("Circle Collider 2D");
				DisplayAddComponentEntry<AudioListenerComponent>("Audio Listener");
				DisplayAddComponentEntry<AudioSourceComponent>("Audio Source");

				ImGui::EndPopup();
			}

			DrawComponent<TransformComponent>("Transform", entity, [&](auto& component)
				{
					ImUtils::Vec3Control<TransformComponent>(entity, this, "Translation", component.Translation);
					glm::vec3 rotation = glm::degrees(component.Rotation);
					ImUtils::Vec3Control<TransformComponent>(entity, this, "Rotation", rotation);
					component.Rotation = glm::radians(rotation);
					ImUtils::Vec3Control<TransformComponent>(entity, this, "Scale", component.Scale, 1.0f);
				});

			DrawComponent<CameraComponent>("Camera", entity, [&](auto& component)
				{
					auto& camera = component.Camera;

					ImGui::Checkbox("Primary", &component.Primary);

					const char* projectionTypeStrings[] = { "Perspective", "Orthographic" };
					const char* currentProjectionTypeString = projectionTypeStrings[(int)camera.GetProjectionType()];
					if (ImGui::BeginCombo("Projection", currentProjectionTypeString))
					{
						for (int i = 0; i < 3; i++)
						{
							bool isSelected = currentProjectionTypeString == projectionTypeStrings[i];
							if (ImGui::Selectable(projectionTypeStrings[i], isSelected))
							{
								currentProjectionTypeString = projectionTypeStrings[i];
								camera.SetProjectionType((SceneCamera::ProjectionType)i);
							}

							if (isSelected)
								ImGui::SetItemDefaultFocus();
						}

						ImGui::EndCombo();
					}

					if (camera.GetProjectionType() == SceneCamera::ProjectionType::Perspective)
					{
						float perspectiveVerticalFov = glm::degrees(camera.GetPerspectiveVerticalFOV());
						if (ImGui::DragFloat("Vertical FOV", &perspectiveVerticalFov))
							camera.SetPerspectiveVerticalFOV(glm::radians(perspectiveVerticalFov));

						float perspectiveNear = camera.GetPerspectiveNearClip();
						if (ImGui::DragFloat("Near", &perspectiveNear))
							camera.SetPerspectiveNearClip(perspectiveNear);

						float perspectiveFar = camera.GetPerspectiveFarClip();
						if (ImGui::DragFloat("Far", &perspectiveFar))
							camera.SetPerspectiveFarClip(perspectiveFar);
					}

					if (camera.GetProjectionType() == SceneCamera::ProjectionType::Orthographic)
					{
						float orthoSize = camera.GetOrthographicSize();
						if (ImGui::DragFloat("Size", &orthoSize))
							camera.SetOrthographicSize(orthoSize);

						float orthoNear = camera.GetOrthographicNearClip();
						if (ImGui::DragFloat("Near", &orthoNear))
							camera.SetOrthographicNearClip(orthoNear);

						float orthoFar = camera.GetOrthographicFarClip();
						if (ImGui::DragFloat("Far", &orthoFar))
							camera.SetOrthographicFarClip(orthoFar);

						ImGui::Checkbox("Fixed Aspect Ratio", &component.FixedAspectRatio);
					}
				});

			if (entity.HasComponent<ScriptWrapperComponent>())
			{
				ScriptWrapperComponent& wrapper = entity.GetComponent<ScriptWrapperComponent>();
				int i = 0;
				for (auto& component : wrapper.Scripts)
				{
					bool scriptClassExists = true;

					std::string componentName = component.ClassName;
					if (std::all_of(componentName.begin(), componentName.end(), isspace) || !ScriptEngine::ComponentClassExists(componentName))
					{
						componentName = "Script";
						scriptClassExists = false;
					}

					const ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_AllowItemOverlap | ImGuiTreeNodeFlags_FramePadding;
					ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();

					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{ 4, 4 });
					float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
					ImGui::Separator();
					bool open = ImGui::TreeNodeEx((void*)("Script" + i), treeNodeFlags, componentName.c_str());
					ImGui::PopStyleVar();
					ImGui::SameLine(contentRegionAvailable.x - lineHeight * 0.5f);
					if (ImGui::Button("+", ImVec2{ lineHeight, lineHeight }))
						ImGui::OpenPopup("ComponentSettings");

					bool removeComponent = false;
					if (ImGui::BeginPopup("ComponentSettings"))
					{
						if (ImGui::MenuItem("Remove component"))
							removeComponent = true;

						ImGui::EndPopup();
					}

					if (open)
					{
						static char buffer[64];
						std::strncpy(buffer, component.ClassName.c_str(), sizeof(buffer));

						if (!scriptClassExists)
							ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9f, 0.2f, 0.3f, 1.0f));

						if (ImGui::InputText("Class", buffer, sizeof(buffer)))
							component.ClassName = buffer;

						if (!scriptClassExists)
							ImGui::PopStyleColor();

						ImGui::TreePop();
					}

					if (removeComponent)
					{
						ScriptWrapperUtils::RemoveComponent(entity, component);
						if (wrapper.Scripts.empty())
							entity.RemoveComponent<ScriptWrapperComponent>();
					}

					i++;
				}
			}

			DrawComponent<SpriteRendererComponent>("Sprite Renderer", entity, [&](auto& component)
				{
					ImGui::ColorEdit4("Color", glm::value_ptr(component.Color));
					ImUtils::Vec2Control<SpriteRendererComponent>(entity, this, "UV Offset", component.TextureUVOffset);

					if (ImGui::ImageButton((ImTextureID)"Texture", ImVec2(100.0f, 100.0f)))
					{
						Ref<Texture2D> whiteTexture = Texture2D::Create(1, 1);
						uint32_t whiteTextureData = 0xffffffff;
						whiteTexture->SetData(&whiteTextureData, sizeof(uint32_t));
						component.Texture = whiteTexture;
					}
					if (ImGui::BeginDragDropTarget())
					{
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM"))
						{
							const wchar_t* path = (const wchar_t*)payload->Data;
							std::filesystem::path texturePath = std::filesystem::path(g_AssetPath) / path;
							Ref<Texture2D> texture = Texture2D::Create(texturePath.string());
							if (texture->IsLoaded())
								component.Texture = texture;
							else
								SPIRIT_WARN("Could not load texture {0}", texturePath.filename().string());
						}
						ImGui::EndDragDropTarget();
					}

					ImGui::DragFloat("Tiling Factor", &component.TilingFactor, 0.1f, 0.0f, 100.0f);
				});

			DrawComponent<CircleRendererComponent>("Circle Renderer", entity, [&](auto& component)
				{
					ImGui::ColorEdit4("Color", glm::value_ptr(component.Color));
					ImGui::DragFloat("Thickness", &component.Thickness, 0.025f, 0.0f, 1.0f);
					ImGui::DragFloat("Fade", &component.Fade, 0.00025f, 0.0f, 1.0f);
				});

			DrawComponent<MeshRendererComponent>("Mesh Renderer", entity, [&](auto& component)
				{
					ImGui::ColorEdit4("Color", glm::value_ptr(component.Color));
					ImUtils::Vec2Control<MeshRendererComponent>(entity, this, "UV Offset", component.TextureUVOffset);

					if (ImGui::Button("Mesh", ImVec2(100.0f, 100.0f)))
						component.Mesh = nullptr;
					if (ImGui::BeginDragDropTarget())
					{
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM"))
						{
							std::filesystem::path path = std::filesystem::path(g_AssetPath) / (const wchar_t*)payload->Data;
							if (FileTypes::IsModelType(path.extension().string()))
							{
								Ref<Model> model = Model::Create(path.string());
								component.Mesh = model;
							}
						}
						ImGui::EndDragDropTarget();
					}
				});

			DrawComponent<DirectionalLightComponent>("Directional Light", entity, [&](auto& component)
				{
					ImUtils::Vec3Control<DirectionalLightComponent>(entity, this, "Ambient", component.Ambient);
					ImUtils::Vec3Control<DirectionalLightComponent>(entity, this, "Diffuse", component.Diffuse);
					ImUtils::Vec3Control<DirectionalLightComponent>(entity, this, "Specular", component.Specular);
				});

			DrawComponent<PointLightComponent>("Point Light", entity, [&](auto& component)
				{
					ImGui::DragFloat("Constant", &component.Constant, 0.1f, 0.0f);
					ImGui::DragFloat("Linear", &component.Linear, 0.1f, 0.0f);
					ImGui::DragFloat("Quadratic", &component.Quadratic, 0.1f, 0.0f);

					ImUtils::Vec3Control<PointLightComponent>(entity, this, "Ambient", component.Ambient);
					ImUtils::Vec3Control<PointLightComponent>(entity, this, "Diffuse", component.Diffuse);
					ImUtils::Vec3Control<PointLightComponent>(entity, this, "Specular", component.Specular);
				});

			DrawComponent<SpotLightComponent>("Spot Light", entity, [&](auto& component)
				{
					ImGui::DragFloat("Cut-Off", &component.CutOff, 0.1f, 0.0f);
					ImGui::DragFloat("Outer Cut-Off", &component.OuterCutOff, 0.1f, 0.0f);

					ImGui::DragFloat("Constant", &component.Constant, 0.1f, 0.0f);
					ImGui::DragFloat("Linear", &component.Linear, 0.1f, 0.0f);
					ImGui::DragFloat("Quadratic", &component.Quadratic, 0.1f, 0.0f);

					ImUtils::Vec3Control<SpotLightComponent>(entity, this, "Ambient", component.Ambient);
					ImUtils::Vec3Control<SpotLightComponent>(entity, this, "Diffuse", component.Diffuse);
					ImUtils::Vec3Control<SpotLightComponent>(entity, this, "Specular", component.Specular);
				});

			DrawComponent<Rigidbody2DComponent>("Rigidbody 2D", entity, [&](auto& component)
				{
					const char* bodyTypeStrings[] = { "Static", "Dynamic", "Kinematic" };
					const char* currentBodyTypeString = bodyTypeStrings[(int)component.Type];
					if (ImGui::BeginCombo("Body Type", currentBodyTypeString))
					{
						for (int i = 0; i < 3; i++)
						{
							bool isSelected = currentBodyTypeString == bodyTypeStrings[i];
							if (ImGui::Selectable(bodyTypeStrings[i], isSelected))
							{
								currentBodyTypeString = bodyTypeStrings[i];
								component.Type = (Rigidbody2DComponent::BodyType)i;
							}

							if (isSelected)
								ImGui::SetItemDefaultFocus();
						}

						ImGui::EndCombo();
					}

					ImGui::Checkbox("Fixed Rotation", &component.FixedRotation);
				});

			DrawComponent<BoxCollider2DComponent>("Box Collider 2D", entity, [&](auto& component)
				{
					ImGui::DragFloat2("Offset", glm::value_ptr(component.Offset));
					ImGui::DragFloat2("Size", glm::value_ptr(component.Size));
					ImGui::DragFloat("Density", &component.Density, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Friction", &component.Friction, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Restitution", &component.Restitution, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Restitution Threshold", &component.RestitutionThreshold, 0.01f, 0.0f);
				});

			DrawComponent<CircleCollider2DComponent>("Circle Collider 2D", entity, [&](auto& component)
				{
					ImGui::DragFloat2("Offset", glm::value_ptr(component.Offset));
					ImGui::DragFloat("Radius", &component.Radius);
					ImGui::DragFloat("Density", &component.Density, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Friction", &component.Friction, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Restitution", &component.Restitution, 0.01f, 0.0f, 1.0f);
					ImGui::DragFloat("Restitution Threshold", &component.RestitutionThreshold, 0.01f, 0.0f);
				});

			DrawComponent<AudioListenerComponent>("Audio Listener", entity, [&](auto& component)
				{
					ImGui::Checkbox("Primary", &component.Primary);
				});

			DrawComponent<AudioSourceComponent>("Audio Source", entity, [&](auto& component)
				{
					if (ImGui::Button("Source", ImVec2(100.0f, 100.0f)))
					{
						AudioSource src = AudioSource();
						src.SetGain(component.Gain);
						src.SetPitch(component.Pitch);
						src.SetSpatial(component.Spatial);
						src.SetLoop(component.Loop);
						component.m_Source = src;
					}
					if (ImGui::BeginDragDropTarget())
					{
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("CONTENT_BROWSER_ITEM"))
						{
							std::filesystem::path path = std::filesystem::path(g_AssetPath) / (const wchar_t*)payload->Data;
							if (FileTypes::IsAudioType(path.extension().string()))
							{
								AudioSource src = AudioSource::LoadFromFile(path.string(), component.Spatial);
								src.SetGain(component.Gain);
								src.SetPitch(component.Pitch);
								src.SetSpatial(component.Spatial);
								src.SetLoop(component.Loop);
								component.m_Source = src;
							}
						}
						ImGui::EndDragDropTarget();
					}

					ImGui::Checkbox("Play Immediately", &component.PlayImmediately);
					if (ImGui::DragFloat("Gain", &component.Gain, 0.1f, 0.0f))
						component.m_Source.SetGain(component.Gain);
					if (ImGui::DragFloat("Pitch", &component.Pitch, 0.1f, 0.0f))
						component.m_Source.SetPitch(component.Pitch);
					if (ImGui::Checkbox("Spatial", &component.Spatial))
						component.m_Source.SetSpatial(component.Spatial);
					if (ImGui::Checkbox("Loop", &component.Loop))
						component.m_Source.SetLoop(component.Loop);
				});
		}

	}

	template<typename T>
	void SceneHierarchyPanel::DisplayAddComponentEntry(const std::string& entryName) {
		if (m_HierarchyScope == HierarchyScope::Scene)
		{
			if (!m_SelectionContext.HasComponent<T>())
			{
				if (ImGui::MenuItem(entryName.c_str()))
				{
					m_SelectionContext.AddComponent<T>();
					ImGui::CloseCurrentPopup();
				}
			}
		}
		else
		{
			if (!m_PrefabSelectionContext.HasComponent<T>())
			{
				if (ImGui::MenuItem(entryName.c_str()))
				{
					m_PrefabSelectionContext.AddComponent<T>();
					ImGui::CloseCurrentPopup();
				}
			}
		}
	}

}
