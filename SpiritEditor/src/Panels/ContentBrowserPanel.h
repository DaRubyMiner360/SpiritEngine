#pragma once

#include "Spirit/Renderer/Texture.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

namespace Spirit
{
	class ContentBrowserPanel
	{
	public:
		ContentBrowserPanel();

		void OnImGuiRender();

		void RenderFileTree(std::filesystem::directory_entry directory);
		ImTextureID GetIcon(std::filesystem::directory_entry directoryEntry);
	private:
		std::filesystem::path m_CurrentDirectory;

		Ref<Texture2D> m_DirectoryIcon;
		Ref<Texture2D> m_SceneIcon;
		// Ref<Texture2D> m_PrefabIcon;
		// Ref<Texture2D> m_AudioIcon;
		Ref<Texture2D> m_FileIcon;
		std::unordered_map<std::string, Ref<Texture2D>> m_ImageIcons;
	};
}
