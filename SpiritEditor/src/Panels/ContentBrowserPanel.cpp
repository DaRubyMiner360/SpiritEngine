#include "spiritpch.h"
#include "ContentBrowserPanel.h"

#include "../EditorLayer.h"
#include "Spirit/Core/UUID.h"
#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"
#include "Spirit/Scene/Components.h"
#include "Spirit/Scene/SceneSerializer.h"
#include "Spirit/Utils/FileUtils.h"

#include <stb_image/stb_image.h>

namespace Spirit {

	extern std::filesystem::path g_ProjectPath;
	extern std::filesystem::path g_AssetPath;

	ContentBrowserPanel::ContentBrowserPanel()
		: m_CurrentDirectory(g_AssetPath)
	{
		m_DirectoryIcon = Texture2D::Create("Resources/Icons/ContentBrowser/DirectoryIcon.png");
		m_SceneIcon = Texture2D::Create("Resources/Icons/ContentBrowser/SceneIcon.png");
		// m_PrefabIcon = Texture2D::Create("Resources/Icons/ContentBrowser/PrefabIcon.png");
		// m_AudioIcon = Texture2D::Create("Resources/Icons/ContentBrowser/AudioIcon.png");
		m_FileIcon = Texture2D::Create("Resources/Icons/ContentBrowser/FileIcon.png");
	}

	void ContentBrowserPanel::OnImGuiRender()
	{
		ImGui::Begin("Content Browser");

		ImGuiContext& g = *GImGui;
		ImGuiWindow* window = g.CurrentWindow;
		if (!(g.LastItemData.StatusFlags & ImGuiItemStatusFlags_HoveredRect))
		{
			ImGuiWindow* hovered_window = g.HoveredWindow;
			if (hovered_window != NULL && window->RootWindow == hovered_window->RootWindow)
			{
				const ImRect& rect = window->Viewport->GetMainRect();
				ImGuiID id = window->ViewportId;

				if (ImGui::BeginDragDropTargetCustom(rect, id))
				{
					if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SCENE_HIERARCHY_ITEM"))
					{
						Entity* entity = (Entity*)payload->Data;

						SceneSerializer serializer(CreateRef<Scene>());
						serializer.SerializePrefab((m_CurrentDirectory / (entity->GetComponent<TagComponent>().Name + ".prefab")).string(), *entity);
					}
					ImGui::EndDragDropTarget();
				}
			}
		}

		if (m_CurrentDirectory != std::filesystem::path(g_AssetPath))
		{
			if (ImGui::Button("<-"))
			{
				m_CurrentDirectory = m_CurrentDirectory.parent_path();
			}
		}

		// Make sure cached texture icons exist, if they dont remove them from cache
		for (auto it = m_ImageIcons.cbegin(), next_it = it; it != m_ImageIcons.cend(); it = next_it)
		{
			++next_it;

			if (!std::filesystem::exists((*it).first))
				m_ImageIcons.erase(it);
		}

		static float padding = 16.0f;
		static float thumbnailSize = 128.0f;
		float cellSize = thumbnailSize + padding;

		float panelWidth = ImGui::GetContentRegionAvail().x;
		int columnCount = (int)(panelWidth / cellSize);
		if (columnCount < 1)
			columnCount = 1;

		ImGui::Columns(columnCount, 0, false);

		for (auto& directoryEntry : std::filesystem::directory_iterator(m_CurrentDirectory))
		{
			const auto& path = directoryEntry.path();

			std::filesystem::path relativePath = std::filesystem::relative(path, g_AssetPath);
			std::string filenameString = relativePath.filename().string();

			ImGui::PushID(filenameString.c_str());
			ImTextureID iconID = GetIcon(directoryEntry);
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
			ImGui::ImageButton(iconID, { thumbnailSize, thumbnailSize }, { 0, 1 }, { 1, 0 });

			// TODO: Change this to a dragable BUTTON
			if (ImGui::BeginDragDropSource())
			{
#ifndef SPIRIT_PLATFORM_LINUX
				const wchar_t* itemPath = relativePath.c_str();
				ImGui::SetDragDropPayload("CONTENT_BROWSER_ITEM", itemPath, (wcslen(itemPath) + 1) * sizeof(wchar_t));
#else
				ImGui::SetDragDropPayload("CONTENT_BROWSER_ITEM", (void*)relativePath.c_str(), relativePath.string().length() + 1);
#endif
				ImGui::EndDragDropSource();
			}

			ImGui::PopStyleColor();
			if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
			{
				if (directoryEntry.is_directory())
					m_CurrentDirectory /= path.filename();
			}
			ImGui::TextWrapped(filenameString.c_str());

			ImGui::NextColumn();

			ImGui::PopID();
		}

		ImGui::Columns(1);

		ImGui::SliderFloat("Thumbnail Size", &thumbnailSize, 16, 512);
		ImGui::SliderFloat("Padding", &padding, 0, 32);

		// TODO: status bar
		ImGui::End();
	}

	void ContentBrowserPanel::RenderFileTree(std::filesystem::directory_entry directory)
	{
		float padding = 16.0f;
		float thumbnailSize = 128.0f;
		float cellSize = thumbnailSize + padding;

		for (auto& directoryEntry : std::filesystem::directory_iterator(directory))
		{
			const auto& path = directoryEntry.path();
			auto relativePath = std::filesystem::relative(path, g_AssetPath);
			std::string filenameString = relativePath.filename().string();

			if (directoryEntry.is_directory())
				RenderFileTree(directoryEntry);
			else
			{
				ImGui::PushID(filenameString.c_str());
				ImTextureID iconID = GetIcon(directoryEntry);
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
				if (ImGui::ImageButton(iconID, { thumbnailSize, thumbnailSize }, { 0, 1 }, { 1, 0 }))
				{
					// TODO: Implement
				}

				if (ImGui::BeginDragDropSource())
				{
#ifndef SPIRIT_PLATFORM_LINUX
					const wchar_t* itemPath = relativePath.c_str();
					ImGui::SetDragDropPayload("CONTENT_BROWSER_ITEM", itemPath, (wcslen(itemPath) + 1) * sizeof(wchar_t));
#else
					ImGui::SetDragDropPayload("CONTENT_BROWSER_ITEM", (void*)relativePath.c_str(), relativePath.string().length() + 1);
#endif
					ImGui::EndDragDropSource();
				}

				ImGui::PopStyleColor();
				if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
				{
					if (directoryEntry.is_directory())
						m_CurrentDirectory /= path.filename();
				}
				ImGui::TextWrapped(filenameString.c_str());

				ImGui::NextColumn();

				ImGui::PopID();
			}
		}
	}

	ImTextureID ContentBrowserPanel::GetIcon(std::filesystem::directory_entry directoryEntry)
	{
		ImTextureID icon = (ImTextureID)m_FileIcon->GetRendererID();
		const auto& path = directoryEntry.path();

		if (StringUtils::StartsWith(path.filename().string(), "."))
			return icon;

		if (directoryEntry.is_directory())
			icon = (ImTextureID)m_DirectoryIcon->GetRendererID();
		else if (FileTypes::IsSceneType(path.extension().string()))
			icon = (ImTextureID)m_SceneIcon->GetRendererID();
		// else if (FileTypes::IsPrefabType(path.extension().string()))
		// 	icon = (ImTextureID)m_PrefabIcon->GetRendererID();
		else if (FileTypes::IsImageType(path.extension().string()))
		{
			if (m_ImageIcons.find(path.string()) == m_ImageIcons.end())
				m_ImageIcons[path.string()] = Texture2D::Create(path.string());

			icon = (ImTextureID)m_ImageIcons[path.string()]->GetRendererID();;
		}

		return icon;
	}
}
