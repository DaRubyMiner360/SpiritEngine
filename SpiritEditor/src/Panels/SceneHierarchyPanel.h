#pragma once

#include "Spirit/Core/Base.h"
#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

namespace Spirit {

	class SceneHierarchyPanel
	{
	public:
		SceneHierarchyPanel() = default;
		SceneHierarchyPanel(const Ref<Scene>& scene);

		void SetContext(const Ref<Scene>& scene);
		void SetPrefab(const Entity& entity);
		void SetContext(const Entity& entity);
		Ref<Scene> GetContext() { return m_Context; };

		enum class HierarchyScope
		{
			Scene = 0, Prefab = 1
		};
		HierarchyScope GetScope() { return m_HierarchyScope; }

		Entity GetSelectionContext();
		Entity GetPrefabSelectionContext();

		void OnImGuiRender();

		void SetSelectedEntity(Entity entity) { m_SelectionContext = entity; };
		void SetSelectedPrefab(Entity entity) { m_PrefabSelectionContext = entity; m_PrefabEntitySelectionContext = entity; };
		void SetSelectedPrefabEntity(Entity entity) { m_PrefabEntitySelectionContext = entity; };
		Entity GetSelectedEntity() const { return m_SelectionContext; }
		Entity GetSelectedPrefab() const { return m_PrefabSelectionContext; }
		Entity GetSelectedPrefabEntity() const { return m_PrefabEntitySelectionContext; }

		void EntityCreation();
	private:
		template<typename T>
		void DisplayAddComponentEntry(const std::string& entryName);

		void DrawEntityNode(Entity& entity, UUID* requiredParent, ImGuiTreeNodeFlags _flags = ImGuiTreeNodeFlags_None);
		void DrawComponents(Entity entity);
	private:
		HierarchyScope m_HierarchyScope = HierarchyScope::Scene;

		Ref<Scene> m_Context;
		Ref<Scene> m_PrefabContext;
		Entity m_SelectionContext;
		Entity m_PrefabSelectionContext;
		Entity m_PrefabEntitySelectionContext;

		friend class Scene;
	};

}
