#include <Spirit.h>
#include <Spirit/Core/EntryPoint.h>

#include "EditorLayer.h"

#include "Spirit/Scripting/ScriptEngine.h"
#include "Spirit/Utils/FileUtils.h"
#include "Spirit/Utils/INIUtils.h"

#include <iostream>
#include <fstream>
#include <string>

namespace Spirit {

	extern std::filesystem::path g_ProjectPath = std::filesystem::path("projects") / "main";
	extern std::filesystem::path g_AssetPath = g_ProjectPath / "assets";
	extern std::string g_ProjectName = "No Project";

	class SpiritEditor : public Application
	{
	public:
		SpiritEditor(const ApplicationSpecification& spec)
			: Application(spec, false)
		{
			if (std::filesystem::exists("temp.txt"))
			{
				auto& file = std::ifstream("temp.txt");
				std::string projectPath, projectName;
				std::getline(file, projectPath);
				std::getline(file, projectName);
				g_ProjectPath = projectPath;
				g_AssetPath = g_ProjectPath / "assets";
				g_ProjectName = projectName;
				file.close();
				std::remove("temp.txt");
			}
			else if (spec.CommandLineArgs.Count > 1 && std::filesystem::exists(spec.CommandLineArgs[1]))
			{
				g_ProjectPath = spec.CommandLineArgs[1];

				bool isProject = false;
				std::filesystem::path projectFile = "";
				if (g_ProjectPath.has_extension())
				{
					isProject = FileTypes::IsProjectType(g_ProjectPath.extension().string());
					projectFile = g_ProjectPath;
					g_ProjectPath = g_ProjectPath.parent_path();
				}
				else if (std::filesystem::is_directory(g_ProjectPath))
				{
					for (auto& f : std::filesystem::directory_iterator(g_ProjectPath))
					{
						for (auto& t : FileTypes::GetProjectTypes())
						{
							auto& type = "." + t;
							if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
							{
								if (isProject)
								{
									SPIRIT_WARN("More than one project file found!");
									continue;
								}
								isProject = std::filesystem::exists(f.path());
								projectFile = f.path();
							}
						}
					}
				}

				if (isProject)
				{
					g_AssetPath = g_ProjectPath / "assets";

					mINI::INIFile projectFile(projectFile.string());
					mINI::INIStructure projectINI;

					if (projectFile.read(projectINI))
						g_ProjectName = projectINI["project"]["name"];
					else
						g_ProjectName = "Unnamed Project";
				}
				else
					g_ProjectName = "Unnamed Project";
			}
			else
			{
				g_ProjectPath = "projects/main";
				g_AssetPath = g_ProjectPath / "assets";
				g_ProjectName = "No Project";
			}

			Renderer::Init();
			ScriptEngine::Init();

			PushLayer(new EditorLayer());
		}
	};

	Application* CreateApplication(ApplicationCommandLineArgs args)
	{
		ApplicationSpecification spec;
		spec.Name = "Spiritual";
		spec.CommandLineArgs = args;

		return new SpiritEditor(spec);
	}

}
