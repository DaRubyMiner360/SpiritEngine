#pragma once

#include <Spirit.h>
#include "entt.hpp"
#include "Panels/SceneHierarchyPanel.h"
#include "Panels/ContentBrowserPanel.h"
#include "Spirit/ImGui/ImGuiConsole.h"

#include "Spirit/Renderer/EditorCamera.h"

#include "Discord-GameSDK/cpp/core.h"
#include "Discord-GameSDK/cpp/discord.h"

namespace Spirit {

	class EditorLayer : public Layer
	{
	public:
		EditorLayer();
		virtual ~EditorLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;

		void OnUpdate(Timestep ts) override;
		virtual void OnImGuiRender() override;
		void OnEvent(Event& e) override;

		bool OnKeyPressed(KeyPressedEvent& e);
		bool OnMouseButtonPressed(MouseButtonPressedEvent& e);

		void OnOverlayRender();

		Ref<Scene> GetActiveScene() { return m_ActiveScene; }

		void OpenPrefab(const std::filesystem::path& path);
		void NewScene();
		void OpenScene();
		void OpenScene(const std::filesystem::path& path);
		void SaveScene();
		void SaveSceneAs();

		void OnScenePlay();
		void OnSceneSimulate();
		void OnSceneStop();

		void DuplicateSelectedEntity();
		void DeleteSelectedEntity();

		void SafeExit();

		// UI Panels
		void UI_ChildPanels();
		void UI_MenuBar();
		void UI_Stats();
		void UI_Toolbar();
		void UI_Settings();
		void UI_Viewport();
		void UI_Gizmos();
	private:
		Spirit::OrthographicCameraController m_CameraController;

		// Temp
		Ref<VertexArray> m_SquareVA;
		Ref<Shader> m_FlatColorShader;
		Ref<Framebuffer> m_Framebuffer;
		Ref<Framebuffer> m_IDFramebuffer;

		Ref<Scene> m_ActiveScene;
		std::string m_EditorSceneFilePath = "";
		Ref<Scene> m_EditorScene, m_RuntimeScene;

		Entity m_HoveredEntity;

		EditorCamera m_EditorCamera;

		bool m_ViewportFocused = false, m_ViewportHovered = false;
		glm::vec2 m_ViewportSize = { 0.0f, 0.0f };
		glm::vec2 m_ViewportBounds[2];

		int m_GizmoType = -1;

		bool m_ShowPhysicsColliders = false;

		enum class SceneState
		{
			Edit = 0, Play = 1, Simulate = 2
		};
		SceneState m_SceneState = SceneState::Edit;

		// Panels
		SceneHierarchyPanel m_SceneHierarchyPanel;
		ContentBrowserPanel m_ContentBrowserPanel;
		ImGuiConsole m_ConsolePanel;

		// Editor resources
		Ref<Texture2D> m_IconPlay, m_IconSimulate/*, m_IconPause*/, m_IconStop;

		discord::Core* core{};
		bool m_LoadedDiscord = false;
		bool m_RichPresenceEnabled = true;
		bool m_DisplayProjectName = true;
		bool m_DisplaySceneName = true;
		bool m_ResetTimestampOnSceneChange = false;
		std::time_t m_TimestampStart;
	};

}
