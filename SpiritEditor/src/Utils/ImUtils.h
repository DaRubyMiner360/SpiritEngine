#pragma once

#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"
#include "Spirit/Scene/Components.h"
#include "Spirit/Core/UUID.h"

#include "../Panels/SceneHierarchyPanel.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <glm/gtc/type_ptr.hpp>

#include <cstring>

/* The Microsoft C++ compiler is non-compliant with the C++ standard and needs
 * the following definition to disable a security warning on std::strncpy().
 */
#ifdef _MSVC_LANG
#define _CRT_SECURE_NO_WARNINGS
#endif

namespace Spirit {

	namespace ImUtils {

		template<typename T>
		void UpdateComponent(Entity& entity, T* prefabComponent, T& entityComponent, T before) = delete;

		template<>
		void UpdateComponent<TagComponent>(Entity& entity, TagComponent* prefabComponent, TagComponent& entityComponent, TagComponent before)
		{
			if (!entity.HasComponent<TagComponent>())
				entity.AddComponent<TagComponent>();

			if ((!prefabComponent || prefabComponent->Name != before.Name) && entityComponent.Name == before.Name)
				entityComponent.Name = prefabComponent->Name;
			if ((!prefabComponent || prefabComponent->Tag != before.Tag) && entityComponent.Tag == before.Tag)
				entityComponent.Tag = prefabComponent->Tag;
		}

		template<>
		void UpdateComponent<TransformComponent>(Entity& entity, TransformComponent* prefabComponent, TransformComponent& entityComponent, TransformComponent before)
		{
			if (!entity.HasComponent<TransformComponent>())
				entity.AddComponent<TransformComponent>();

			if ((!prefabComponent || prefabComponent->Translation != before.Translation) && entityComponent.Translation == before.Translation)
				entityComponent.Translation = prefabComponent->Translation;
			if ((!prefabComponent || prefabComponent->Rotation != before.Rotation) && entityComponent.Rotation == before.Rotation)
				entityComponent.Rotation = prefabComponent->Rotation;
			if ((!prefabComponent || prefabComponent->Scale != before.Scale) && entityComponent.Scale == before.Scale)
				entityComponent.Scale = prefabComponent->Scale;
		}

		template<>
		void UpdateComponent<SpriteRendererComponent>(Entity& entity, SpriteRendererComponent* prefabComponent, SpriteRendererComponent& entityComponent, SpriteRendererComponent before)
		{
			if (!entity.HasComponent<SpriteRendererComponent>())
				entity.AddComponent<SpriteRendererComponent>();

			if ((!prefabComponent || prefabComponent->Color != before.Color) && entityComponent.Color == before.Color)
				entityComponent.Color = prefabComponent->Color;
			if ((!prefabComponent || prefabComponent->TextureUVOffset != before.TextureUVOffset) && entityComponent.TextureUVOffset == before.TextureUVOffset)
				entityComponent.TextureUVOffset = prefabComponent->TextureUVOffset;
			if ((!prefabComponent || prefabComponent->Texture != before.Texture) && entityComponent.Texture == before.Texture)
				entityComponent.Texture = prefabComponent->Texture;
			if ((!prefabComponent || prefabComponent->TilingFactor != before.TilingFactor) && entityComponent.TilingFactor == before.TilingFactor)
				entityComponent.TilingFactor = prefabComponent->TilingFactor;
		}

		template<>
		void UpdateComponent<CircleRendererComponent>(Entity& entity, CircleRendererComponent* prefabComponent, CircleRendererComponent& entityComponent, CircleRendererComponent before)
		{
			if (!entity.HasComponent<CircleRendererComponent>())
				entity.AddComponent<CircleRendererComponent>();

			if ((!prefabComponent || prefabComponent->Color != before.Color) && entityComponent.Color == before.Color)
				entityComponent.Color = prefabComponent->Color;
			if ((!prefabComponent || prefabComponent->Thickness != before.Thickness) && entityComponent.Thickness == before.Thickness)
				entityComponent.Thickness = prefabComponent->Thickness;
			if ((!prefabComponent || prefabComponent->Fade != before.Fade) && entityComponent.Fade == before.Fade)
				entityComponent.Fade = prefabComponent->Fade;
		}

		template<>
		void UpdateComponent<MeshRendererComponent>(Entity& entity, MeshRendererComponent* prefabComponent, MeshRendererComponent& entityComponent, MeshRendererComponent before)
		{
			if (!entity.HasComponent<MeshRendererComponent>())
				entity.AddComponent<MeshRendererComponent>();

			if ((!prefabComponent || prefabComponent->Mesh != before.Mesh) && entityComponent.Mesh == before.Mesh)
				entityComponent.Mesh = prefabComponent->Mesh;

			if ((!prefabComponent || prefabComponent->Color != before.Color) && entityComponent.Color == before.Color)
				entityComponent.Color = prefabComponent->Color;
			if ((!prefabComponent || prefabComponent->TextureUVOffset != before.TextureUVOffset) && entityComponent.TextureUVOffset == before.TextureUVOffset)
				entityComponent.TextureUVOffset = prefabComponent->TextureUVOffset;
		}

		template<>
		void UpdateComponent<DirectionalLightComponent>(Entity& entity, DirectionalLightComponent* prefabComponent, DirectionalLightComponent& entityComponent, DirectionalLightComponent before)
		{
			if (!entity.HasComponent<DirectionalLightComponent>())
				entity.AddComponent<DirectionalLightComponent>();

			if ((!prefabComponent || prefabComponent->Ambient != before.Ambient) && entityComponent.Ambient == before.Ambient)
				entityComponent.Ambient = prefabComponent->Ambient;
			if ((!prefabComponent || prefabComponent->Diffuse != before.Diffuse) && entityComponent.Diffuse == before.Diffuse)
				entityComponent.Diffuse = prefabComponent->Diffuse;
			if ((!prefabComponent || prefabComponent->Specular != before.Specular) && entityComponent.Specular == before.Specular)
				entityComponent.Specular = prefabComponent->Specular;
		}

		template<>
		void UpdateComponent<PointLightComponent>(Entity& entity, PointLightComponent* prefabComponent, PointLightComponent& entityComponent, PointLightComponent before)
		{
			if (!entity.HasComponent<PointLightComponent>())
				entity.AddComponent<PointLightComponent>();

			if ((!prefabComponent || prefabComponent->Constant != before.Constant) && entityComponent.Constant == before.Constant)
				entityComponent.Constant = prefabComponent->Constant;
			if ((!prefabComponent || prefabComponent->Linear != before.Linear) && entityComponent.Linear == before.Linear)
				entityComponent.Linear = prefabComponent->Linear;
			if ((!prefabComponent || prefabComponent->Quadratic != before.Quadratic) && entityComponent.Quadratic == before.Quadratic)
				entityComponent.Quadratic = prefabComponent->Quadratic;

			if ((!prefabComponent || prefabComponent->Ambient != before.Ambient) && entityComponent.Ambient == before.Ambient)
				entityComponent.Ambient = prefabComponent->Ambient;
			if ((!prefabComponent || prefabComponent->Diffuse != before.Diffuse) && entityComponent.Diffuse == before.Diffuse)
				entityComponent.Diffuse = prefabComponent->Diffuse;
			if ((!prefabComponent || prefabComponent->Specular != before.Specular) && entityComponent.Specular == before.Specular)
				entityComponent.Specular = prefabComponent->Specular;
		}

		template<>
		void UpdateComponent<SpotLightComponent>(Entity& entity, SpotLightComponent* prefabComponent, SpotLightComponent& entityComponent, SpotLightComponent before)
		{
			if (!entity.HasComponent<SpotLightComponent>())
				entity.AddComponent<SpotLightComponent>();

			if ((!prefabComponent || prefabComponent->CutOff != before.CutOff) && entityComponent.CutOff == before.CutOff)
				entityComponent.CutOff = prefabComponent->CutOff;
			if ((!prefabComponent || prefabComponent->OuterCutOff != before.OuterCutOff) && entityComponent.OuterCutOff == before.OuterCutOff)
				entityComponent.OuterCutOff = prefabComponent->OuterCutOff;

			if ((!prefabComponent || prefabComponent->Constant != before.Constant) && entityComponent.Constant == before.Constant)
				entityComponent.Constant = prefabComponent->Constant;
			if ((!prefabComponent || prefabComponent->Linear != before.Linear) && entityComponent.Linear == before.Linear)
				entityComponent.Linear = prefabComponent->Linear;
			if ((!prefabComponent || prefabComponent->Quadratic != before.Quadratic) && entityComponent.Quadratic == before.Quadratic)
				entityComponent.Quadratic = prefabComponent->Quadratic;

			if ((!prefabComponent || prefabComponent->Ambient != before.Ambient) && entityComponent.Ambient == before.Ambient)
				entityComponent.Ambient = prefabComponent->Ambient;
			if ((!prefabComponent || prefabComponent->Diffuse != before.Diffuse) && entityComponent.Diffuse == before.Diffuse)
				entityComponent.Diffuse = prefabComponent->Diffuse;
			if ((!prefabComponent || prefabComponent->Specular != before.Specular) && entityComponent.Specular == before.Specular)
				entityComponent.Specular = prefabComponent->Specular;
		}

		template<>
		void UpdateComponent<CameraComponent>(Entity& entity, CameraComponent* prefabComponent, CameraComponent& entityComponent, CameraComponent before)
		{
			if (!entity.HasComponent<CameraComponent>())
				entity.AddComponent<CameraComponent>();

			if ((!prefabComponent || prefabComponent->Camera != before.Camera) && entityComponent.Camera == before.Camera)
				entityComponent.Camera = prefabComponent->Camera;
			if ((!prefabComponent || prefabComponent->Primary != before.Primary) && entityComponent.Primary == before.Primary)
				entityComponent.Primary = prefabComponent->Primary;
			if ((!prefabComponent || prefabComponent->FixedAspectRatio != before.FixedAspectRatio) && entityComponent.FixedAspectRatio == before.FixedAspectRatio)
				entityComponent.FixedAspectRatio = prefabComponent->FixedAspectRatio;
		}

		template<>
		void UpdateComponent<Rigidbody2DComponent>(Entity& entity, Rigidbody2DComponent* prefabComponent, Rigidbody2DComponent& entityComponent, Rigidbody2DComponent before)
		{
			if (!entity.HasComponent<Rigidbody2DComponent>())
				entity.AddComponent<Rigidbody2DComponent>();

			if ((!prefabComponent || prefabComponent->Type != before.Type) && entityComponent.Type == before.Type)
				entityComponent.Type = prefabComponent->Type;
			if ((!prefabComponent || prefabComponent->FixedRotation != before.FixedRotation) && entityComponent.FixedRotation == before.FixedRotation)
				entityComponent.FixedRotation = prefabComponent->FixedRotation;

			if ((!prefabComponent || prefabComponent->RuntimeBody != before.RuntimeBody) && entityComponent.RuntimeBody == before.RuntimeBody)
				entityComponent.RuntimeBody = prefabComponent->RuntimeBody;
		}

		template<>
		void UpdateComponent<BoxCollider2DComponent>(Entity& entity, BoxCollider2DComponent* prefabComponent, BoxCollider2DComponent& entityComponent, BoxCollider2DComponent before)
		{
			if (!entity.HasComponent<BoxCollider2DComponent>())
				entity.AddComponent<BoxCollider2DComponent>();

			if ((!prefabComponent || prefabComponent->Offset != before.Offset) && entityComponent.Offset == before.Offset)
				entityComponent.Offset = prefabComponent->Offset;
			if ((!prefabComponent || prefabComponent->Size != before.Size) && entityComponent.Size == before.Size)
				entityComponent.Size = prefabComponent->Size;

			if ((!prefabComponent || prefabComponent->Density != before.Density) && entityComponent.Density == before.Density)
				entityComponent.Density = prefabComponent->Density;
			if ((!prefabComponent || prefabComponent->Friction != before.Friction) && entityComponent.Friction == before.Friction)
				entityComponent.Friction = prefabComponent->Friction;
			if ((!prefabComponent || prefabComponent->Restitution != before.Restitution) && entityComponent.Restitution == before.Restitution)
				entityComponent.Restitution = prefabComponent->Restitution;
			if ((!prefabComponent || prefabComponent->RestitutionThreshold != before.RestitutionThreshold) && entityComponent.RestitutionThreshold == before.RestitutionThreshold)
				entityComponent.RestitutionThreshold = prefabComponent->RestitutionThreshold;

			if ((!prefabComponent || prefabComponent->RuntimeFixture != before.RuntimeFixture) && entityComponent.RuntimeFixture == before.RuntimeFixture)
				entityComponent.RuntimeFixture = prefabComponent->RuntimeFixture;
		}

		template<>
		void UpdateComponent<CircleCollider2DComponent>(Entity& entity, CircleCollider2DComponent* prefabComponent, CircleCollider2DComponent& entityComponent, CircleCollider2DComponent before)
		{
			if (!entity.HasComponent<CircleCollider2DComponent>())
				entity.AddComponent<CircleCollider2DComponent>();

			if ((!prefabComponent || prefabComponent->Offset != before.Offset) && entityComponent.Offset == before.Offset)
				entityComponent.Offset = prefabComponent->Offset;
			if ((!prefabComponent || prefabComponent->Radius != before.Radius) && entityComponent.Radius == before.Radius)
				entityComponent.Radius = prefabComponent->Radius;

			if ((!prefabComponent || prefabComponent->Density != before.Density) && entityComponent.Density == before.Density)
				entityComponent.Density = prefabComponent->Density;
			if ((!prefabComponent || prefabComponent->Friction != before.Friction) && entityComponent.Friction == before.Friction)
				entityComponent.Friction = prefabComponent->Friction;
			if ((!prefabComponent || prefabComponent->Restitution != before.Restitution) && entityComponent.Restitution == before.Restitution)
				entityComponent.Restitution = prefabComponent->Restitution;
			if ((!prefabComponent || prefabComponent->RestitutionThreshold != before.RestitutionThreshold) && entityComponent.RestitutionThreshold == before.RestitutionThreshold)
				entityComponent.RestitutionThreshold = prefabComponent->RestitutionThreshold;

			if ((!prefabComponent || prefabComponent->RuntimeFixture != before.RuntimeFixture) && entityComponent.RuntimeFixture == before.RuntimeFixture)
				entityComponent.RuntimeFixture = prefabComponent->RuntimeFixture;
		}

		template<>
		void UpdateComponent<AudioListenerComponent>(Entity& entity, AudioListenerComponent* prefabComponent, AudioListenerComponent& entityComponent, AudioListenerComponent before)
		{
			if (!entity.HasComponent<AudioListenerComponent>())
				entity.AddComponent<AudioListenerComponent>();

			if ((!prefabComponent || prefabComponent->Primary != before.Primary) && entityComponent.Primary == before.Primary)
				entityComponent.Primary = prefabComponent->Primary;
		}

		template<>
		void UpdateComponent<AudioSourceComponent>(Entity& entity, AudioSourceComponent* prefabComponent, AudioSourceComponent& entityComponent, AudioSourceComponent before)
		{
			if (!entity.HasComponent<AudioSourceComponent>())
				entity.AddComponent<AudioSourceComponent>();

			if ((!prefabComponent || prefabComponent->m_Source != before.m_Source) && entityComponent.m_Source == before.m_Source)
				entityComponent.m_Source = prefabComponent->m_Source;

			if ((!prefabComponent || prefabComponent->PlayImmediately != before.PlayImmediately) && entityComponent.PlayImmediately == before.PlayImmediately)
				entityComponent.PlayImmediately = prefabComponent->PlayImmediately;
			if ((!prefabComponent || prefabComponent->Gain != before.Gain) && entityComponent.Gain == before.Gain)
				entityComponent.Gain = prefabComponent->Gain;
			if ((!prefabComponent || prefabComponent->Pitch != before.Pitch) && entityComponent.Pitch == before.Pitch)
				entityComponent.Pitch = prefabComponent->Pitch;
			if ((!prefabComponent || prefabComponent->Spatial != before.Spatial) && entityComponent.Spatial == before.Spatial)
				entityComponent.Spatial = prefabComponent->Spatial;
			if ((!prefabComponent || prefabComponent->Loop != before.Loop) && entityComponent.Loop == before.Loop)
				entityComponent.Loop = prefabComponent->Loop;
		}

		template<>
		void UpdateComponent<ScriptWrapperComponent>(Entity& entity, ScriptWrapperComponent* prefabComponent, ScriptWrapperComponent& entityComponent, ScriptWrapperComponent before)
		{
			if (!entity.HasComponent<ScriptWrapperComponent>())
				entity.AddComponent<ScriptWrapperComponent>();

			if ((!prefabComponent || prefabComponent->Scripts != before.Scripts) && entityComponent.Scripts == before.Scripts)
				entityComponent.Scripts = prefabComponent->Scripts;

			/*int i = 0;
			for (auto component : entityComponent.Scripts)
			{
				i++;
			}*/
		}

		/*template<>
		void UpdateComponent<ScriptComponent>(Entity& entity, ScriptComponent* prefabComponent, ScriptComponent& entityComponent, ScriptComponent before)
		{
			if (!entity.HasComponent<ScriptComponent>())
				entity.AddComponent<ScriptComponent>();

			if ((!prefabComponent || prefabComponent->ClassName != before.ClassName) && entityComponent.ClassName == before.ClassName)
				entityComponent.ClassName = prefabComponent->ClassName;
		}*/

		template<>
		void UpdateComponent<NativeScriptComponent>(Entity& entity, NativeScriptComponent* prefabComponent, NativeScriptComponent& entityComponent, NativeScriptComponent before)
		{
			if (!entity.HasComponent<NativeScriptComponent>())
				entity.AddComponent<NativeScriptComponent>();

			if ((!prefabComponent || prefabComponent->Instance != before.Instance) && entityComponent.Instance == before.Instance)
				entityComponent.Instance = prefabComponent->Instance;
		}

		void UpdateRelationships(Entity& entity, Entity* prefab, Entity before, SceneHierarchyPanel* sceneHierarchyPanel)
		{
			if (sceneHierarchyPanel && sceneHierarchyPanel->GetScope() == SceneHierarchyPanel::HierarchyScope::Prefab)
			{
				if (prefab)
				{
					if (prefab->Children.size() > before.Children.size())
					{
						if (entity.Children.size() == before.Children.size())
						{
							for (int i = 0; i < prefab->Children.size(); i++)
							{
								if (i > entity.Children.size())
									entity.Children.push_back(prefab->Children[i]);
							}
						}
						else if (entity.Children.size() > before.Children.size())
						{
							for (int i = 0; i < prefab->Children.size(); i++)
							{
								if (i > entity.Children.size())
									entity.Children.push_back(prefab->Children[i]);
							}
						}
						else if (entity.Children.size() < before.Children.size())
						{
						}
					}
					else if (prefab->Children.size() < before.Children.size())
					{
					}
				}

				Entity parent = entity.GetScene().GetEntityByID(*entity.Parent);
				if (entity.Parent)
				{
					int childIndex = -1;
					for (int i = 0; i < parent.Children.size(); i++)
					{
						if (parent.Children[i] == entity.GetUUID())
						{
							childIndex = i;
							break;
						}
					}

					if (childIndex != -1)
					{
						if (childIndex > 0)
							*entity.Previous = *parent.Children[childIndex - 1];
						else
							entity.Previous = nullptr;

						if (parent.Children.size() > childIndex + 1)
							*entity.Next = *parent.Children[childIndex + 1];
						else
							entity.Next = nullptr;
					}
				}

				if (entity.Children.size() > 0)
					*entity.First = *entity.Children[0];
				else
					entity.First = nullptr;

				if (prefab)
				{
					for (int i = 0; i < entity.Children.size(); i++)
					{
						Entity entityChild = entity.GetScene().GetEntityByID(entity.Children[i]);
						Entity prefabChild = prefab->GetScene().GetEntityByID(prefab->Children[i]);
						Entity beforeChild = before.GetScene().GetEntityByID(before.Children[i]);
						if (entityChild && prefabChild && beforeChild)
							UpdateRelationships(entityChild, &prefabChild, beforeChild, sceneHierarchyPanel);
					}
				}
				else
				{
					for (int i = 0; i < entity.Children.size(); i++)
					{
						Entity entityChild = entity.GetScene().GetEntityByID(entity.Children[i]);
						Entity beforeChild = before.GetScene().GetEntityByID(before.Children[i]);
						if (entityChild && beforeChild)
							UpdateRelationships(entityChild, nullptr, beforeChild, sceneHierarchyPanel);
					}
				}
			}
		}

		template<typename T>
		void UpdateEntity(Entity& entity, Entity* prefab, Entity before, SceneHierarchyPanel* sceneHierarchyPanel)
		{
			if (sceneHierarchyPanel && sceneHierarchyPanel->GetScope() == SceneHierarchyPanel::HierarchyScope::Prefab)
			{
				if (prefab)
				{
					auto& prefabComponent = prefab->GetComponent<T>();
					auto& entityComponent = entity.GetComponent<T>();
					auto& beforeComponent = before.GetComponent<T>();
					UpdateComponent<T>(*prefab, &prefabComponent, entityComponent, beforeComponent);
					entity.GetScene().OnComponentModified<T>(*prefab, entityComponent);
				}

				UpdateRelationships(entity, prefab, before, sceneHierarchyPanel);

				if (prefab)
				{
					for (int i = 0; i < entity.Children.size(); i++)
					{
						Entity entityChild = entity.GetScene().GetEntityByID(entity.Children[i]);
						Entity prefabChild = prefab->GetScene().GetEntityByID(prefab->Children[i]);
						Entity beforeChild = before.GetScene().GetEntityByID(before.Children[i]);
						if (entityChild && prefabChild && beforeChild)
							UpdateEntity<T>(entityChild, &prefabChild, beforeChild, sceneHierarchyPanel);
					}
				}
				else
				{
					for (int i = 0; i < entity.Children.size(); i++)
					{
						Entity entityChild = entity.GetScene().GetEntityByID(entity.Children[i]);
						Entity beforeChild = before.GetScene().GetEntityByID(before.Children[i]);
						if (entityChild && beforeChild)
							UpdateEntity<T>(entityChild, nullptr, beforeChild, sceneHierarchyPanel);
					}
				}
			}
		}

		template<typename T>
		void UpdateEntities(Entity* entity, SceneHierarchyPanel* sceneHierarchyPanel, Entity before)
		{
			if (entity)
			{
				if (sceneHierarchyPanel && sceneHierarchyPanel->GetScope() == SceneHierarchyPanel::HierarchyScope::Prefab)
				{
					std::vector<Entity> entities = sceneHierarchyPanel->GetContext()->GetEntitiesByAssetID(entity->GetAssetID());
					for (Entity e : entities)
					{
						UpdateEntity<T>(e, entity, before, sceneHierarchyPanel);
					}
				}
			}
		}



		bool Vec3Control(const std::string& label, glm::vec3& values, float resetValue = 0.0f, float columnWidth = 100)
		{
			bool changed = false;


			ImGuiIO& io = ImGui::GetIO();
			auto boldFont = io.Fonts->Fonts[0];

			ImGui::PushID(label.c_str());

			ImGui::Columns(2);
			ImGui::SetColumnWidth(0, columnWidth);
			ImGui::Text(label.c_str());
			ImGui::NextColumn();

			ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

			float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			ImVec2 buttonSize = { lineHeight + 3.0f, lineHeight };

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("X", buttonSize))
			{
				values.x = resetValue;
				changed = true;
			}
			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			if (ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f"))
				changed = true;
			ImGui::PopItemWidth();
			ImGui::SameLine();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.3f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.3f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("Y", buttonSize))
			{
				values.y = resetValue;
				changed = true;
			}
			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			if (ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f"))
				changed = true;
			ImGui::PopItemWidth();
			ImGui::SameLine();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.35f, 0.9f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("Z", buttonSize))
			{
				values.z = resetValue;
				changed = true;
			}
			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			if (ImGui::DragFloat("##Z", &values.z, 0.1f, 0.0f, 0.0f, "%.2f"))
				changed = true;
			ImGui::PopItemWidth();

			ImGui::PopStyleVar();

			ImGui::Columns(1);

			ImGui::PopID();


			return changed;
		}

		template<typename T>
		bool Vec3Control(Entity& entity, SceneHierarchyPanel* sceneHierarchyPanel, const std::string& label, glm::vec3& values, float resetValue = 0.0f, float columnWidth = 100)
		{
			Entity before = entity;
			bool changed = Vec3Control(label, values, resetValue, columnWidth);

			if (entity && changed)
				UpdateEntities<T>(&entity, sceneHierarchyPanel, before);

			return changed;
		}

		bool Vec2Control(const std::string& label, glm::vec2& values, float resetValue = 0.0f, float columnWidth = 150)
		{
			bool changed = false;


			ImGuiIO& io = ImGui::GetIO();
			auto boldFont = io.Fonts->Fonts[0];

			ImGui::PushID(label.c_str());

			ImGui::Columns(2);
			ImGui::SetColumnWidth(0, columnWidth);
			ImGui::Text(label.c_str());
			ImGui::NextColumn();

			ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

			float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			ImVec2 buttonSize = { lineHeight + 3.0f, lineHeight };

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("X", buttonSize))
			{
				values.x = resetValue;
				changed = true;
			}
			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			if (ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f"))
				changed = true;
			ImGui::PopItemWidth();
			ImGui::SameLine();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.3f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.3f, 1.0f });
			ImGui::PushFont(boldFont);
			if (ImGui::Button("Y", buttonSize))
			{
				values.y = resetValue;
				changed = true;
			}
			ImGui::PopFont();
			ImGui::PopStyleColor(3);

			ImGui::SameLine();
			if (ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f"))
				changed = true;
			ImGui::PopItemWidth();

			ImGui::PopStyleVar();

			ImGui::Columns(1);

			ImGui::PopID();


			return changed;
		}

		template<typename T>
		bool Vec2Control(Entity& entity, SceneHierarchyPanel* sceneHierarchyPanel, const std::string& label, glm::vec2& values, float resetValue = 0.0f, float columnWidth = 150)
		{
			Entity before = entity;
			bool changed = Vec2Control(label, values, resetValue, columnWidth);

			if (entity && changed)
				UpdateEntities<T>(&entity, sceneHierarchyPanel, before);

			return changed;
		}
	}

}
