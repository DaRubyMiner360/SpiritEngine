﻿using System;
using Spirit;

namespace Sandbox
{
	public class Camera : MonoComponent
	{
		public float baseSpeed = 0.05f;
		[HideInInspector]
		public float speed;

		public override void OnInit()
		{
			speed = baseSpeed;
		}

		public override void OnUpdate(float ts)
		{
			Vector3 velocity = Vector3.Zero;

			if (Input.IsKeyDown(KeyCode.Up))
				velocity.Y = 1.0f;
			else if (Input.IsKeyDown(KeyCode.Down))
				velocity.Y = -1.0f;

			if (Input.IsKeyDown(KeyCode.Left))
				velocity.X = -1.0f;
			else if (Input.IsKeyDown(KeyCode.Right))
				velocity.X = 1.0f;

			if (Input.IsKeyDown(KeyCode.X))
				Entity.Destroy();

			velocity *= speed;

			Vector3 translation = Entity.Translation;
			translation += velocity * ts;
			Entity.Translation = translation;
		}
	}
}
