﻿using System;
using Spirit;

namespace Sandbox
{
	public class Player : MonoComponent
	{
		public float speed = 0.05f;
		private TransformComponent transform;
		private Rigidbody2DComponent rigidbody;
		private Camera cam;

		public override void OnCreate()
		{
			Console.WriteLine("Player.OnCreate()");
			Console.WriteLine($"{Entity.Name} (Player)'s Entity ID: {Entity.ID}");
			Console.WriteLine($"{Entity.Name} (Player)'s Asset ID: {Entity.AssetID}");

			transform = GetComponent<TransformComponent>();
			rigidbody = GetComponent<Rigidbody2DComponent>();
			cam = GetComponent<Camera>();
		}

		public override void OnUpdate(float ts)
		{
			//Console.WriteLine($"Player.OnUpdate({ts})");
			Vector3 velocity = Vector3.Zero;

			if (Input.IsKeyDown(KeyCode.W))
				velocity.Y = 1.0f;
			else if (Input.IsKeyDown(KeyCode.S))
				velocity.Y = -1.0f;

			if (Input.IsKeyDown(KeyCode.A))
				velocity.X = -1.0f;
			else if (Input.IsKeyDown(KeyCode.D))
				velocity.X = 1.0f;

			if (Input.IsKeyDown(KeyCode.Z))
				Entity.Destroy();

			if (Input.IsKeyDown(KeyCode.LeftShift) || Input.IsKeyDown(KeyCode.RightShift))
				cam.speed = cam.baseSpeed * 2;
			else
				cam.speed = cam.baseSpeed;

			velocity *= speed;

			rigidbody.ApplyLinearImpulse(velocity.XY, true);
		}
	}
}
