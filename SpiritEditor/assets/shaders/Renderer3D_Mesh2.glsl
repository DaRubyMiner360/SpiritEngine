//--------------------------
// - Spirit Engine -
// Renderer3D Mesh Shader
// --------------------------

#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec4 a_Color;
layout(location = 4) in vec3 a_Tangent;
layout(location = 5) in vec3 a_Bitangent;
layout(location = 6) in int a_EntityID;

layout(std140, binding = 0) uniform Camera
{
	mat4 u_ViewProjection;
};

struct VertexOutput
{
	vec3 Normal;
	vec2 TexCoord;
	vec4 Color;
	vec3 Tangent;
	vec3 Bitangent;
};

layout (location = 0) out VertexOutput Output;
layout (location = 5) out flat int v_EntityID;

void main()
{
	Output.Normal = a_Normal;
	Output.TexCoord = a_TexCoord;
	Output.Color = a_Color;
	Output.Tangent = a_Tangent;
	Output.Bitangent = a_Bitangent;
	v_EntityID = a_EntityID;

	gl_Position = u_ViewProjection * vec4(a_Position, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;
layout(location = 1) out int o_EntityID;

struct VertexOutput
{
	vec3 Normal;
	vec2 TexCoord;
	vec4 Color;
	vec3 Tangent;
	vec3 Bitangent;
};

layout (location = 0) in VertexOutput Input;
layout (location = 5) in flat int v_EntityID;

layout (binding = 0) uniform sampler2D u_TextureDiffuse1;
layout(std140, binding = 1) uniform Config
{
	int u_HasTextures;
};

void main()
{
	vec4 texColor = Input.Color;

	if (u_HasTextures == 1)
	{
		texColor *= texture(u_TextureDiffuse1, Input.TexCoord);
	}

	o_Color = texColor;
	o_EntityID = v_EntityID;
}
