//--------------------------
// - Spirit Engine -
// Renderer3D Mesh Shader
// --------------------------

#type vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec2 a_TexCoord;
layout(location = 3) in vec4 a_Color;
layout(location = 4) in vec3 a_Tangent;
layout(location = 5) in vec3 a_Bitangent;
layout(location = 6) in int a_EntityID;

layout(std140, binding = 0) uniform Camera
{
	mat4 u_ViewProjection;
};

struct VertexOutput
{
	vec3 Normal;
	vec2 TexCoord;
	vec4 Color;
	vec3 Tangent;
	vec3 Bitangent;
};

layout (location = 0) out VertexOutput Output;
layout (location = 5) out flat vec3 v_Position;
layout (location = 6) out flat int v_EntityID;

void main()
{
	Output.Normal = a_Normal;
	Output.TexCoord = a_TexCoord;
	Output.Color = a_Color;
	Output.Tangent = a_Tangent;
	Output.Bitangent = a_Bitangent;
	v_Position = a_Position;
	v_EntityID = a_EntityID;

	gl_Position = u_ViewProjection * vec4(a_Position, 1.0);
}

#type fragment
#version 450 core

layout(location = 0) out vec4 o_Color;
layout(location = 1) out int o_EntityID;

struct DirectionalLight
{
    vec3 Direction;
	
    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;
};

struct PointLight
{
    vec3 Position;
    
    float Constant;
    float Linear;
    float Quadratic;
	
    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;
};

struct SpotLight
{
    vec3 Position;
    vec3 Direction;
    float CutOff;
    float OuterCutOff;
  
    float Constant;
    float Linear;
    float Quadratic;
  
    vec3 Ambient;
    vec3 Diffuse;
    vec3 Specular;
};

struct VertexOutput
{
	vec3 Normal;
	vec2 TexCoord;
	vec4 Color;
	vec3 Tangent;
	vec3 Bitangent;
};

layout (location = 0) in VertexOutput Input;
layout (location = 5) in flat vec3 v_Position;
layout (location = 6) in flat int v_EntityID;

layout (binding = 0) uniform sampler2D u_TextureDiffuse1;
layout(std140, binding = 1) uniform Data
{
	int u_HasTextures;
	int u_DirectionalLightCount;
	int u_PointLightCount;
	int u_SpotLightCount;

	vec3 u_ViewPosition;
	DirectionalLight u_DirectionalLights[1];
	PointLight u_PointLights[4];
	SpotLight u_SpotLights[1];

	float u_MaterialShininess;
};
layout (binding = 2) uniform sampler2D u_MaterialDiffuse;
layout (binding = 3) uniform sampler2D u_MaterialSpecular;

vec3 CalculateDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDirection)
{
    vec3 lightDirection = normalize(-light.Direction);
    // Diffuse shading
    float diff = max(dot(normal, lightDirection), 0.0);
    // Specular shading
    vec3 reflectDirection = reflect(-lightDirection, normal);
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), u_MaterialShininess);
    // Combine results
    vec3 ambient = light.Ambient * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 diffuse = light.Diffuse * diff * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 specular = light.Specular * spec * vec3(texture(u_MaterialSpecular, Input.TexCoord));
    return (ambient + diffuse + specular);
}

vec3 CalculatePointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDirection)
{
    vec3 lightDirection = normalize(light.Position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDirection), 0.0);
    // Specular shading
    vec3 reflectDirection = reflect(-lightDirection, normal);
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), u_MaterialShininess);
    // Attenuation
    float distance = length(light.Position - fragPos);
    float attenuation = 1.0 / (light.Constant + light.Linear * distance + light.Quadratic * (distance * distance));
    // Combine results
    vec3 ambient = light.Ambient * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 diffuse = light.Diffuse * diff * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 specular = light.Specular * spec * vec3(texture(u_MaterialSpecular, Input.TexCoord));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

vec3 CalculateSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDirection)
{
    vec3 lightDirection = normalize(light.Position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDirection), 0.0);
    // Specular shading
    vec3 reflectDirection = reflect(-lightDirection, normal);
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), u_MaterialShininess);
    // Attenuation
    float distance = length(light.Position - fragPos);
    float attenuation = 1.0 / (light.Constant + light.Linear * distance + light.Quadratic * (distance * distance));
    // Spotlight intensity
    float theta = dot(lightDirection, normalize(-light.Direction)); 
    float epsilon = light.CutOff - light.OuterCutOff;
    float intensity = clamp((theta - light.OuterCutOff) / epsilon, 0.0, 1.0);
    // Combine results
    vec3 ambient = light.Ambient * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 diffuse = light.Diffuse * diff * vec3(texture(u_MaterialDiffuse, Input.TexCoord));
    vec3 specular = light.Specular * spec * vec3(texture(u_MaterialSpecular, Input.TexCoord));
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular);
}

void main()
{
	// Properties
    vec3 norm = normalize(Input.Normal);
    vec3 viewDirection = normalize(u_ViewPosition - v_Position);
    
    // == =====================================================
    // Our lighting is set up in 3 phases: directional, point lights and an optional flashlight
    // For each phase, a calculate function is defined that calculates the corresponding color
    // per lamp. In the main() function we take all the calculated colors and sum them up for
    // this fragment's final color.
    // == =====================================================
    vec3 result = vec3(1.0, 1.0, 1.0);
	// Phase 1: Directional lights
    for (int i = 0; i < u_DirectionalLightCount; i++)
        result += CalculateDirectionalLight(u_DirectionalLights[i], norm, viewDirection);
    // Phase 2: Point lights
    for (int i = 0; i < u_PointLightCount; i++)
        result += CalculatePointLight(u_PointLights[i], norm, v_Position, viewDirection);
    // Phase 3: Spot lights
	for (int i = 0; i < u_SpotLightCount; i++)
        result += CalculateSpotLight(u_SpotLights[i], norm, v_Position, viewDirection);
    
    vec4 texColor = Input.Color;
	if (u_HasTextures == 1)
		texColor *= texture(u_TextureDiffuse1, Input.TexCoord);

	texColor *= vec4(result, 1.0);

	o_Color = texColor;
	o_EntityID = v_EntityID;
}
