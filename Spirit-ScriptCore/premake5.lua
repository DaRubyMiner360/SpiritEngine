project "Spirit-ScriptCore"
	kind "SharedLib"
	language "C#"
	dotnetframework "4.7.2"
	namespace "Spirit"

	targetdir ("../SpiritEditor/Resources/Scripts")
	objdir ("../SpiritEditor/Resources/Scripts/Intermediates")

	files 
	{
		"Source/**.cs",
		"Properties/**.cs"
	}

	postbuildcommands
	{
		--"{COPYDIR} \"../SpiritEditor/Resources/Scripts\" \"../SpiritRuntime/Resources/Scripts\"",
		--"{COPYDIR} \"../SpiritEditor/Resources/Scripts\" \"../bin/" .. outputdir .. "/Resources/Scripts\""
	}
	
	filter "configurations:Debug"
		optimize "Off"
		symbols "Default"

	filter "configurations:Release"
		optimize "On"
		symbols "Default"

	filter "configurations:Dist"
		optimize "Full"
		symbols "Off"
