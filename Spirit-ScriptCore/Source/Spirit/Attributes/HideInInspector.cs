﻿using System;

namespace Spirit
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
	public class HideInInspector : Attribute { }
}
