﻿using System;

namespace Spirit
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
	public class SerializeField : Attribute { }
}
