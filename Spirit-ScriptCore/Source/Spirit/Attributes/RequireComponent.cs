﻿using System;

namespace Spirit
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class RequireComponent : Attribute
	{
		public Component RequiredComponent { get; internal set; }

		public RequireComponent(Component requiredComponent)
		{
			this.RequiredComponent = requiredComponent;
		}
	}
}
