﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spirit
{
	public class Input
	{
		public static bool IsKeyPressed(KeyCode keyCode)
		{
			return InternalCalls.Input_IsKeyDown(keyCode);
		}
		public static bool IsKeyDown(KeyCode keyCode)
		{
			return InternalCalls.Input_IsKeyPressed(keyCode);
		}
		public static bool IsKeyUp(KeyCode keyCode)
		{
			return InternalCalls.Input_IsKeyUp(keyCode);
		}
		public static bool IsMouseButtonPressed(MouseCode mouseCode)
		{
			return InternalCalls.Input_IsMouseButtonDown(mouseCode);
		}
		public static bool IsMouseButtonDown(MouseCode mouseCode)
		{
			return InternalCalls.Input_IsMouseButtonPressed(mouseCode);
		}
		public static bool IsMouseButtonUp(MouseCode mouseCode)
		{
			return InternalCalls.Input_IsMouseButtonUp(mouseCode);
		}
		public static float GetMouseX()
		{
			return InternalCalls.Input_GetMouseX();
		}
		public static float GetMouseY()
		{
			return InternalCalls.Input_GetMouseY();
		}
	}
}
