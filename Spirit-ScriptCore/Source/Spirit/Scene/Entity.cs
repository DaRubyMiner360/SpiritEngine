﻿using System;

namespace Spirit
{
	public class Entity
	{
		public readonly ulong ID, AssetID;

		protected Entity() { ID = 0; AssetID = 0; }

		internal Entity(ulong id)
		{
			ID = id;
			AssetID = 0;
		}

		internal Entity(ulong id, ulong assetID)
		{
			ID = id;
			AssetID = assetID;
		}

		public string Name
		{
			get
			{
				InternalCalls.Entity_GetName(ID, out string name);
				return name;
			}
			set
			{
				InternalCalls.Entity_SetName(ID, ref value);
			}
		}

		public string Tag
		{
			get
			{
				InternalCalls.Entity_GetTag(ID, out string tag);
				return tag;
			}
			set
			{
				InternalCalls.Entity_SetTag(ID, ref value);
			}
		}

		public Vector3 Translation
		{
			get
			{
				InternalCalls.TransformComponent_GetTranslation(ID, out Vector3 translation);
				return translation;
			}
			set
			{
				InternalCalls.TransformComponent_SetTranslation(ID, ref value);
			}
		}

		public Vector3 Rotation
		{
			get
			{
				InternalCalls.TransformComponent_GetRotation(ID, out Vector3 rotation);
				return rotation;
			}
			set
			{
				InternalCalls.TransformComponent_SetRotation(ID, ref value);
			}
		}

		public Vector3 Scale
		{
			get
			{
				InternalCalls.TransformComponent_GetTranslation(ID, out Vector3 scale);
				return scale;
			}
			set
			{
				InternalCalls.TransformComponent_SetTranslation(ID, ref value);
			}
		}

		public void Destroy()
		{
			InternalCalls.Entity_Destroy(ID);
		}

		public T AddComponent<T>() where T : Component, new()
		{
			return AddComponent<T>(true);
		}

		internal T AddComponent<T>(bool safely) where T : Component, new()
		{
			if (safely && HasComponent<T>())
				return null;

			var component = InternalCalls.Entity_AddComponent(ID, typeof(T));
			if (IsInternalComponent<T>())
				return new T() { Entity = this };

			if (component == null)
				return null;
			return (T)component;
		}

		public T AddOrGetComponent<T>() where T : Component, new()
		{
			if (HasComponent<T>())
				return GetComponent<T>(false);

			return AddComponent<T>(false);
		}

		public T GetComponent<T>() where T : Component, new()
		{
			return GetComponent<T>(true);
		}

		internal T GetComponent<T>(bool safely) where T : Component, new()
		{
			if (safely && !HasComponent<T>())
				return null;

			if (IsInternalComponent<T>())
				return new T() { Entity = this };

			var monoComponent = InternalCalls.Entity_GetMonoComponent(ID, typeof(T));
			if (monoComponent == null)
				return null;
			return (T)monoComponent;
		}

		/*public T[] GetComponents<T>() where T : Component, new()
		{
			if (!HasComponent<T>())
				return null;

			if (IsInternalComponent<T>())
				return new T[] { new T() { Entity = this } };

			var monoComponent = InternalCalls.Entity_GetMonoComponents(ID, typeof(T));
			if (monoComponent == null)
				return null;
			return (T[])monoComponent;
		}*/

		internal static bool IsInternalComponent<T>() where T : Component, new()
		{
			return !typeof(T).IsSubclassOf(typeof(MonoComponent));
		}

		public bool HasComponent<T>() where T : Component, new()
		{
			return InternalCalls.Entity_HasComponent(ID, typeof(T));
		}

		public void RemoveComponent<T>() where T : Component, new()
		{
			RemoveComponent<T>(true);
		}

		public void RemoveComponent<T>(bool safely) where T : Component, new()
		{
			if (safely && !HasComponent<T>())
				return;

			InternalCalls.Entity_RemoveComponent(ID, typeof(T));
		}
	}
}
