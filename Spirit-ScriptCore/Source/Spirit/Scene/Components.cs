﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spirit
{
	public abstract class Component
	{
		public Entity Entity { get; internal set; }

		public T AddComponent<T>() where T : Component, new()
		{
			return Entity.AddComponent<T>();
		}

		public T AddOrGetComponent<T>() where T : Component, new()
		{
			return Entity.AddOrGetComponent<T>();
		}

		public T GetComponent<T>() where T : Component, new()
		{
			return Entity.GetComponent<T>();
		}

		/*public T[] GetComponents<T>() where T : Component, new()
		{
			return Entity.GetComponents<T>();
		}*/

		public bool HasComponent<T>() where T : Component, new()
		{
			return Entity.HasComponent<T>();
		}

		public void RemoveComponent<T>() where T : Component, new()
		{
			Entity.RemoveComponent<T>();
		}
	}

	public class TransformComponent : Component
	{
		public Vector3 Translation
		{
			get
			{
				InternalCalls.TransformComponent_GetTranslation(Entity.ID, out Vector3 translation);
				return translation;
			}
			set
			{
				InternalCalls.TransformComponent_SetTranslation(Entity.ID, ref value);
			}
		}

		public Vector3 Rotation
		{
			get
			{
				InternalCalls.TransformComponent_GetRotation(Entity.ID, out Vector3 rotation);
				return rotation;
			}
			set
			{
				InternalCalls.TransformComponent_SetRotation(Entity.ID, ref value);
			}
		}

		public Vector3 Scale
		{
			get
			{
				InternalCalls.TransformComponent_GetScale(Entity.ID, out Vector3 scale);
				return scale;
			}
			set
			{
				InternalCalls.TransformComponent_SetScale(Entity.ID, ref value);
			}
		}
	}

	public class SpriteRendererComponent : Component
	{
	}

	public class CircleRendererComponent : Component
	{
	}

	public class MeshRendererComponent : Component
	{
	}

	public class DirectionalLightComponent : Component
	{
	}

	public class PointLightComponent : Component
	{
	}

	public class SpotLightComponent : Component
	{
	}

	public class CameraComponent : Component
	{
	}

	public class Rigidbody2DComponent : Component
	{
		public void ApplyLinearImpulse(Vector2 impulse, Vector2 worldPosition, bool wake)
		{
			InternalCalls.Rigidbody2DComponent_ApplyLinearImpulse(Entity.ID, ref impulse, ref worldPosition, wake);
		}

		public void ApplyLinearImpulse(Vector2 impulse, bool wake)
		{
			InternalCalls.Rigidbody2DComponent_ApplyLinearImpulseToCenter(Entity.ID, ref impulse, wake);
		}
	}

	public class BoxCollider2DComponent : Component
	{
	}

	public class CircleCollider2DComponent : Component
	{
	}

	public class AudioListenerComponent : Component
	{
	}

	public class AudioSourceComponent : Component
	{
	}

	internal class ScriptWrapperComponent : Component { }

	internal class NativeScriptComponent : Component { }

	public class MonoComponent : Component
	{
		protected MonoComponent() { Entity = new Entity(0, 0); }

		internal MonoComponent(ulong id)
		{
			Entity = new Entity(id);
		}

		internal MonoComponent(ulong id, ulong assetID)
		{
			Entity = new Entity(id, assetID);
		}

		public virtual void OnInit() { }
		public virtual void OnCreate() { }
		public virtual void OnUpdate(float ts) { }
		public virtual void OnPhysicsUpdate(float ts) { }
		public virtual void OnModify() { }
		public virtual void OnRemove() { }
		public virtual void OnDestroy() { }
	}
}
