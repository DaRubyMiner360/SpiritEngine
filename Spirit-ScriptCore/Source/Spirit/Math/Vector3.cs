﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spirit
{
	public struct Vector3
	{
		public float X, Y, Z;

		public static Vector3 Zero = new Vector3(0.0f);

		public Vector3(float scalar)
		{
			X = scalar;
			Y = scalar;
			Z = scalar;
		}

		public Vector3(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public Vector3(Vector3 vector)
		{
			X = vector.X;
			Y = vector.Y;
			Z = vector.Z;
		}

		public Vector3(Vector4 vector)
		{
			X = vector.X;
			Y = vector.Y;
			Z = vector.Z;
		}

		public Vector3(Vector2 vector)
		{
			X = vector.X;
			Y = vector.Y;
			Z = 0.0f;
		}

		public Vector2 XY
		{
			get => new Vector2(X, Y);
			set
			{
				X = value.X;
				Y = value.Y;
			}
		}

		public static Vector3 operator +(Vector3 a, Vector3 b)
		{
			return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
		}

		public static Vector3 operator -(Vector3 a, Vector3 b)
		{
			return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
		}

		public static Vector3 operator *(Vector3 a, Vector3 b)
		{
			return new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
		}

		public static Vector3 operator /(Vector3 a, Vector3 b)
		{
			return new Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
		}

		public static Vector3 operator +(Vector3 a, Vector4 b)
		{
			return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
		}

		public static Vector3 operator -(Vector3 a, Vector4 b)
		{
			return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
		}

		public static Vector3 operator *(Vector3 a, Vector4 b)
		{
			return new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
		}

		public static Vector3 operator /(Vector3 a, Vector4 b)
		{
			return new Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
		}

		public static Vector3 operator +(Vector3 a, Vector2 b)
		{
			return new Vector3(a.X + b.X, a.Y + b.Y, a.Z);
		}

		public static Vector3 operator -(Vector3 a, Vector2 b)
		{
			return new Vector3(a.X - b.X, a.Y - b.Y, a.Z);
		}

		public static Vector3 operator *(Vector3 a, Vector2 b)
		{
			return new Vector3(a.X * b.X, a.Y * b.Y, a.Z);
		}

		public static Vector3 operator /(Vector3 a, Vector2 b)
		{
			return new Vector3(a.X / b.X, a.Y / b.Y, a.Z);
		}

		public static Vector3 operator *(Vector3 vector, float scalar)
		{
			return new Vector3(vector.X * scalar, vector.Y * scalar, vector.Z * scalar);
		}
	}
}
