project "ProjectManager"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "off"

	targetdir ("%{wks.location}/bin/" .. outputdir)
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"src/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"%{wks.location}/Spirit/vendor/spdlog/include",
		"%{wks.location}/Spirit/src",
		"%{wks.location}/Spirit/vendor",
		"%{IncludeDir.glm}",
		"%{IncludeDir.entt}",
		"%{IncludeDir.NativeFileDialog}"
	}

	links
	{
		"Spirit"
	}

	postbuildcommands
	{
		"{COPYDIR} \"%{LibraryDir.VulkanSDK_DebugDLL}\" \"%{cfg.targetdir}\"",
			
		"{COPYDIR} \"%{wks.location}/%{prj.name}/projects\" \"%{cfg.targetdir}/projects\"",
		"{COPYDIR} \"%{wks.location}/%{prj.name}/configs\" \"%{cfg.targetdir}/configs\""
	}

	filter "system:windows"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_WINDOWS"
		}

		files
		{
			"resource.h",
			"app.aps",
			"app.rc"
		}

	filter "system:linux"
		links
		{
			"GLFW",
			"Glad",
			"ImGui",
			"Xrandr",
			"Xi",
			"GLU",
			"GL",
			"X11",
			"dl",
			"pthread",
			"stdc++fs"	--GCC versions 5.3 through 8.x need stdc++fs for std::filesystem
		}
		defines
		{
			"SPIRIT_PLATFORM_LINUX"
		}

	filter "system:macosx"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_MACOS"
		}

	filter "configurations:Debug"
		debugdir "%{cfg.targetdir}"
		defines "SPIRIT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "SPIRIT_RELEASE"
		runtime "Release"
		optimize "on"
		kind "WindowedApp"
		entrypoint "mainCRTStartup"

	filter "configurations:Dist"
		defines "SPIRIT_DIST"
		runtime "Release"
		optimize "on"
