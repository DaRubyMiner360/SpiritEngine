#pragma once

#include <Spirit.h>

#include <iostream>
#include <fstream>
#include <string>

namespace Spirit {

	struct Project
	{
		std::string Name;
		std::string AssetsDirectory;

		std::filesystem::path Path;

		bool operator==(const Project& other) const { return Name == other.Name && AssetsDirectory == other.AssetsDirectory && Path.string() == other.Path.string(); }
		bool operator!=(const Project& other) const { return !(*this == other); }
	};

	struct CreationProgress
	{
		std::string Name;
		std::string Path;
	};

	class ProjectSelectionLayer : public Layer
	{
	public:
		ProjectSelectionLayer();
		virtual ~ProjectSelectionLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;

		void OnUpdate(Timestep ts) override;
		virtual void OnImGuiRender() override;
		void OnEvent(Event& e) override;

		bool OnKeyPressed(KeyPressedEvent& e);
		bool OnMouseButtonPressed(MouseButtonPressedEvent& e);

		bool CreateProject();
		bool CreateProject(const std::filesystem::path& templatePath, const std::filesystem::path& path);
		void OpenProject();
		void OpenProject(const std::filesystem::path& path, bool forceExit = false);

		std::filesystem::path m_DefaultProjectPath;
		std::vector<Project> m_PreviouslyOpenedProjects;
		std::vector<Project> m_ProjectTemplates;
	private:
		enum class ProjectSelectionOption
		{
			Open = 0, Create = 1
		};
		ProjectSelectionOption m_SelectedOption = ProjectSelectionOption::Open;

		int m_ProjectIndex = -1;
		int m_TemplateIndex = -1;

		CreationProgress m_CreationProgress = { "", m_DefaultProjectPath.string() };
	};

}
