#include <Spirit.h>
#include <Spirit/Core/EntryPoint.h>

#include "ProjectSelectionLayer.h"

#include <iostream>
#include <fstream>
#include <string>

namespace Spirit {

	class ProjectManager : public Application
	{
	public:
		ProjectManager(const ApplicationSpecification& spec)
			: Application(spec, false, false)
		{
			PushLayer(new ProjectSelectionLayer());
		}

		~ProjectManager() {}
	};

	Application* CreateApplication(ApplicationCommandLineArgs args)
	{
		ApplicationSpecification spec;
		spec.Name = "Spirit Project Manager";
		spec.CommandLineArgs = args;

		return new ProjectManager(spec);
	}

}
