#include "spiritpch.h"
#include "ProjectSelectionLayer.h"
#include "Spirit/Utils/StringUtils.h"
#include "Spirit/Utils/FileUtils.h"
#include "Spirit/Utils/INIUtils.h"
#include "Spirit/Math/Math.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include <imgui/misc/cpp/imgui_stdlib.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <Glad/include/glad/glad.h>

#include "nfd.hpp"

namespace Spirit {

	extern std::filesystem::path g_AssetPath = "assets";

	ProjectSelectionLayer::ProjectSelectionLayer()
		: Layer("ProjectSelectionLayer")
	{
	}

	void ProjectSelectionLayer::OnAttach()
	{
		SPIRIT_PROFILE_FUNCTION();

		NFD_Init();

		for (auto const& file : std::filesystem::directory_iterator{ std::filesystem::path("projects") })
		{
			if (!file.is_directory())
				continue;

			std::filesystem::path filepath = file;

			bool isProject = false;
			std::filesystem::path projectFile = "";
			if (filepath.has_extension())
			{
				isProject = FileTypes::IsProjectType(filepath.extension().string());
				projectFile = filepath;
				filepath = filepath.parent_path();
			}
			else if (std::filesystem::is_directory(filepath))
			{
				for (auto& f : std::filesystem::directory_iterator(filepath))
				{
					for (auto& t : FileTypes::GetProjectTypes())
					{
						auto& type = "." + t;
						if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
						{
							if (isProject)
							{
								SPIRIT_WARN("More than one project file found!");
								continue;
							}
							isProject = std::filesystem::exists(f.path());
							projectFile = f.path();
						}
					}
				}
			}

			if (isProject)
			{
				std::string projectName = "";

				mINI::INIFile projectFile(projectFile.string());
				mINI::INIStructure projectINI;

				if (projectFile.read(projectINI))
					projectName = projectINI["project"]["name"];

				Project project = { projectName, (filepath / "assets").string(), filepath };
				if (filepath != "projects/main")
					m_PreviouslyOpenedProjects.push_back(project);
			}
		}

		mINI::INIFile file("configs/OpenedProjects.ini");
		mINI::INIStructure ini;

		if (file.read(ini))
		{
			for (auto& key : ini["projects"])
			{
				std::filesystem::path filepath = ini["projects"][key.first];

				bool isProject = false;
				std::filesystem::path projectFile = "";
				if (filepath.has_extension())
				{
					isProject = FileTypes::IsProjectType(filepath.extension().string());
					projectFile = filepath;
					filepath = filepath.parent_path();
				}
				else if (std::filesystem::is_directory(filepath))
				{
					for (auto& f : std::filesystem::directory_iterator(filepath))
					{
						for (auto& t : FileTypes::GetProjectTypes())
						{
							auto& type = "." + t;
							if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
							{
								if (isProject)
								{
									SPIRIT_WARN("More than one project file found!");
									continue;
								}
								isProject = std::filesystem::exists(f.path());
								projectFile = f.path();
							}
						}
					}
				}

				if (isProject)
				{
					std::string projectName = "";

					mINI::INIFile projectFile(projectFile.string());
					mINI::INIStructure projectINI;

					if (projectFile.read(projectINI))
						projectName = projectINI["project"]["name"];

					Project project = { projectName, (filepath / "assets").string(), filepath };
					if (std::find(m_PreviouslyOpenedProjects.begin(), m_PreviouslyOpenedProjects.end(), project) != m_PreviouslyOpenedProjects.end())
						m_PreviouslyOpenedProjects.push_back(project);
				}
			}
		}

		for (auto const& file : std::filesystem::directory_iterator{ std::filesystem::path("templates") })
		{
			if (!file.is_directory())
				continue;

			std::filesystem::path filepath = file;

			bool isProject = false;
			std::filesystem::path templateFile = "";
			if (filepath.has_extension())
			{
				isProject = FileTypes::IsProjectType(filepath.extension().string());
				templateFile = filepath;
				filepath = filepath.parent_path();
			}
			else if (std::filesystem::is_directory(filepath))
			{
				for (auto& f : std::filesystem::directory_iterator(filepath))
				{
					for (auto& t : FileTypes::GetProjectTypes())
					{
						auto& type = "." + t;
						if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
						{
							if (isProject)
							{
								SPIRIT_WARN("More than one project file found!");
								continue;
							}
							isProject = std::filesystem::exists(f.path());
							templateFile = f.path();
						}
					}
				}
			}

			if (isProject)
			{
				std::string projectName = "";

				mINI::INIFile projectFile(templateFile.string());
				mINI::INIStructure projectINI;

				if (projectFile.read(projectINI))
					projectName = projectINI["project"]["name"];

				Project project = { projectName, (filepath / "assets").string(), filepath };
				m_ProjectTemplates.push_back(project);
			}
		}

		mINI::INIFile templatesFile("configs/ProjectTemplates.ini");
		mINI::INIStructure templatesINI;

		if (templatesFile.read(templatesINI))
		{
			for (auto& key : templatesINI["projects"])
			{
				std::filesystem::path filepath = templatesINI["projects"][key.first];

				bool isProject = false;
				std::filesystem::path templateFile = "";
				if (filepath.has_extension())
				{
					isProject = FileTypes::IsProjectType(filepath.extension().string());
					templateFile = filepath;
					filepath = filepath.parent_path();
				}
				else if (std::filesystem::is_directory(filepath))
				{
					for (auto& f : std::filesystem::directory_iterator(filepath))
					{
						for (auto& t : FileTypes::GetProjectTypes())
						{
							auto& type = "." + t;
							if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
							{
								if (isProject)
								{
									SPIRIT_WARN("More than one project file found!");
									continue;
								}
								isProject = std::filesystem::exists(f.path());
								templateFile = f.path();
							}
						}
					}
				}

				if (isProject)
				{
					std::string projectName = "";

					mINI::INIFile projectFile(templateFile.string());
					mINI::INIStructure projectINI;

					if (projectFile.read(projectINI))
						projectName = projectINI["project"]["name"];

					Project project = { projectName, (filepath / "assets").string(), filepath };
					if (std::find(m_PreviouslyOpenedProjects.begin(), m_PreviouslyOpenedProjects.end(), project) != m_PreviouslyOpenedProjects.end())
						m_ProjectTemplates.push_back(project);
				}
			}
		}

		mINI::INIFile editorFile("configs/Editor.ini");
		mINI::INIStructure editorINI;

		if (editorFile.read(editorINI))
		{
			std::string theme = StringUtils::ToLowerCase(editorINI["appearance"]["Theme"]);

			if (theme == "dark")
				ImGuiLayer::SetDarkThemeColors();
			else if (theme == "light")
				ImGuiLayer::SetLightThemeColors();
			else if (theme == "corporategrey")
				ImGuiLayer::SetCorporateGreyThemeColors();
			else if (theme == "classicdark")
				ImGuiLayer::SetClassicDarkThemeColors();
			else if (theme == "classiclight")
				ImGuiLayer::SetClassicLightThemeColors();
			else if (theme == "classic")
				ImGuiLayer::SetClassicThemeColors();
			else
				ImGuiLayer::SetDarkThemeColors();
		}
		else
			ImGuiLayer::SetDarkThemeColors();

		auto commandLineArgs = Application::Get().GetSpecification().CommandLineArgs;
		if (commandLineArgs.Count > 1)
		{
			if (std::string(commandLineArgs[1]) == std::string("-c") || std::string(commandLineArgs[1]) == std::string("--create") || std::string(commandLineArgs[1]) == std::string("create") || std::string(commandLineArgs[1]) == std::string("-n") || std::string(commandLineArgs[1]) == std::string("--new") || std::string(commandLineArgs[1]) == std::string("new"))
			{
				m_SelectedOption = ProjectSelectionOption::Create;
			}
			else if (std::string(commandLineArgs[1]) == std::string("-o") || std::string(commandLineArgs[1]) == std::string("--open") || std::string(commandLineArgs[1]) == std::string("open") || std::string(commandLineArgs[1]) == std::string("-i") || std::string(commandLineArgs[1]) == std::string("--import") || std::string(commandLineArgs[1]) == std::string("import"))
			{
				if (commandLineArgs.Count > 2)
				{
					if (std::filesystem::exists(commandLineArgs[2]))
					{
						OpenProject(commandLineArgs[2], true);
						return;
					}
				}
				else
					m_SelectedOption = ProjectSelectionOption::Open;
			}
			else if (std::filesystem::exists(std::string(commandLineArgs[1])))
			{
				OpenProject(commandLineArgs[1], true);
				return;
			}
		}
	}

	void ProjectSelectionLayer::OnDetach()
	{
		SPIRIT_PROFILE_FUNCTION();

		NFD_Quit();
	}

	void ProjectSelectionLayer::OnUpdate(Timestep ts)
	{
		SPIRIT_PROFILE_FUNCTION();
	}

	void ProjectSelectionLayer::OnImGuiRender()
	{
		SPIRIT_PROFILE_FUNCTION();

		// Note: Switch this to true to enable dockspace
		static bool dockspaceOpen = true;
		static bool opt_fullscreen_persistant = true;
		bool opt_fullscreen = opt_fullscreen_persistant;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}

		// When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;

		// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
		// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive, 
		// all active windows docked into it will lose their parent and become undocked.
		// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise 
		// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
		//ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		//ImGui::Begin("DockSpace Demo", &dockspaceOpen, window_flags);
		//ImGui::PopStyleVar();

		if (opt_fullscreen)
			ImGui::PopStyleVar(2);

		// DockSpace
		ImGuiIO& io = ImGui::GetIO();
		ImGuiStyle& style = ImGui::GetStyle();
		float minWinSizeX = style.WindowMinSize.x;
		style.WindowMinSize.x = 396.0f;
		//if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
		//{
		//	ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
		//	ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		//}

		style.WindowMinSize.x = minWinSizeX;


		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{ 0, 0 });

		ImGui::Begin("ProjectSelector", false, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);

		ImVec2 sizeMultiplier = { (ImGui::GetWindowSize().x / 1280) * 0.75f, (ImGui::GetWindowSize().y / 720) * 0.75f };
		float sizeMultiplierAverage = (sizeMultiplier.x + sizeMultiplier.y) / 2;

		ImGui::SetCursorPos({ 461.0f * sizeMultiplier.x, 64.0f * sizeMultiplier.y });
		ImFont* font = ImGui::GetFont();
		font->Scale *= 2 * sizeMultiplierAverage;
		ImGui::PushFont(font);
		ImGui::PushStyleVar(ImGuiStyleVar_SelectableTextAlign, ImVec2(0.5f, 0.5f));
		if (ImGui::Selectable("Open Project", m_SelectedOption == ProjectSelectionOption::Open, 0, ImVec2(168.0f * sizeMultiplier.y, 0.0f * sizeMultiplier.y)))
			m_SelectedOption = ProjectSelectionOption::Open;
		ImGui::SetCursorPos({ 983.0f * sizeMultiplier.x, 64.0f * sizeMultiplier.y });
		if (ImGui::Selectable("Create Project", m_SelectedOption == ProjectSelectionOption::Create, 0, ImVec2(196.0f * sizeMultiplier.y, 0.0f * sizeMultiplier.y)))
			m_SelectedOption = ProjectSelectionOption::Create;
		ImGui::PopStyleVar();
		ImGui::PopFont();
		font->Scale /= 2 * sizeMultiplierAverage;

		auto& colors = ImGui::GetStyle().Colors;
		ImGui::SetCursorPos({ 220.0f * sizeMultiplier.x, 161.0f * sizeMultiplier.y });
		if (m_SelectedOption == ProjectSelectionOption::Open)
		{
			int projectIndex = m_ProjectIndex;

			bool hasDefault = false;
			for (auto project : m_PreviouslyOpenedProjects)
			{
				if (project.Path == "projects/main")
					hasDefault = true;
			}
			ImGui::BeginChild("projectlist", { 579.0f * sizeMultiplier.x, 487.0f * sizeMultiplier.y }, true);
			ImGui::SetCursorPos({ 0.0f * sizeMultiplier.x, 0.0f * sizeMultiplier.y });
			if (projectIndex == -1)
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.25f, 0.255f, 0.26f, 1.0f });
			if (ImGui::Button("No Project", { 579.0f * sizeMultiplier.x, 78.0f * sizeMultiplier.y }))
			{
				g_AssetPath = "assets";
				m_ProjectIndex = -1;
			}
			if (projectIndex == -1)
				ImGui::PopStyleColor();

			for (int i = 0; i < m_PreviouslyOpenedProjects.size(); i++)
			{
				auto& project = m_PreviouslyOpenedProjects[i];
				if (project.Path == "projects/main")
					continue;

				if (projectIndex == i)
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.25f, 0.255f, 0.26f, 1.0f });
				ImGui::SetCursorPos({ 0.0f * sizeMultiplier.x, 77.5f * ((i + 1) * sizeMultiplier.y) });
				if (ImGui::Button(project.Name.c_str(), { 579.0f * sizeMultiplier.x, 78.0f * sizeMultiplier.y }))
				{
					g_AssetPath = project.AssetsDirectory;
					m_ProjectIndex = i;
				}
				if (projectIndex == i)
					ImGui::PopStyleColor();
			}
			ImGui::EndChild();
		}
		else
		{
			int templateIndex = m_TemplateIndex;

			ImGui::BeginChild("templatelist", { 579.0f * sizeMultiplier.x, 487.0f * sizeMultiplier.y }, true);
			for (int i = 0; i < m_ProjectTemplates.size(); i++)
			{
				if (templateIndex == i)
					ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.25f, 0.255f, 0.26f, 1.0f });
				auto& project = m_ProjectTemplates[i];
				ImGui::SetCursorPos({ 0.0f * sizeMultiplier.x, 77.5f * (i * sizeMultiplier.y) });
				if (ImGui::Button(project.Name.c_str(), { 579.0f * sizeMultiplier.x, 78.0f * sizeMultiplier.y }))
				{
					g_AssetPath = project.AssetsDirectory;
					m_TemplateIndex = i;
				}
				if (templateIndex == i)
					ImGui::PopStyleColor();
			}
			ImGui::EndChild();
		}
		if (ImGui::IsMouseDown(Mouse::ButtonLeft) && ImGui::IsWindowHovered() && ImGui::IsItemHovered())
			g_AssetPath = "assets";

		if (m_SelectedOption == ProjectSelectionOption::Open)
		{
			ImGui::SetCursorPos({ 525.0f * sizeMultiplier.x, 726.0f * sizeMultiplier.y });
			/*if (g_AssetPath.parent_path().empty())
			{
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.663f, 0.663f, 0.663f, 1.0f });
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			}*/
			if (ImGui::Button("Open", { 210.0f * sizeMultiplier.x, 47.0f * sizeMultiplier.y }))
			{
				if (g_AssetPath == "assets")
					OpenProject(g_AssetPath);
				else
					OpenProject(g_AssetPath.parent_path());
			}
			/*if (g_AssetPath.parent_path().empty())
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleColor();
			}*/

			ImGui::SetCursorPos({ 844.0f * sizeMultiplier.x, 726.0f * sizeMultiplier.y });
		}
		else
		{
			auto& name = m_CreationProgress.Name;
			auto& path = m_CreationProgress.Path;

			char nameBuffer[256];
			memset(nameBuffer, 0, sizeof(nameBuffer));
			std::strncpy(nameBuffer, name.c_str(), sizeof(nameBuffer));

			ImGui::SetCursorPos({ 300.0f * sizeMultiplier.x, 671.0f * sizeMultiplier.y });
			ImGui::Text("Name");
			ImGui::SameLine();
			ImGui::PushItemWidth(ImGui::CalcItemWidth() - 18);
			if (ImGui::InputText("##Name", nameBuffer, sizeof(nameBuffer)))
				name = std::string(nameBuffer);
			ImGui::PopItemWidth();

			ImGui::SetCursorPos({ 300.0f * sizeMultiplier.x, 731.0f * sizeMultiplier.y });
			ImGui::Text("Path");
			ImGui::SameLine();
			ImGui::InputText("##Path", &path);
			ImGui::SameLine();
			if (ImGui::Button("Browse", { 100.0f * sizeMultiplier.x, 47.0f * sizeMultiplier.y }))
			{
				NFD::UniquePath filepath;
				nfdresult_t result = NFD::PickFolder(filepath);
				if (result == NFD_OKAY)
					path = filepath.get();
			}

			ImGui::SetCursorPos({ 525.0f * sizeMultiplier.x, 801.0f * sizeMultiplier.y });
			bool disabled = g_AssetPath.parent_path().empty() || std::filesystem::exists(std::filesystem::path(path) / name);
			if (disabled)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.663f, 0.663f, 0.663f, 1.0f });
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			}
			if (ImGui::Button("Create", { 210.0f * sizeMultiplier.x, 47.0f * sizeMultiplier.y }))
			{
				if (CreateProject(g_AssetPath.parent_path(), std::filesystem::path(path) / name))
					OpenProject(std::filesystem::path(path) / name / ".sproj");
			}
			if (disabled)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleColor();
			}

			ImGui::SetCursorPos({ 844.0f * sizeMultiplier.x, 801.0f * sizeMultiplier.y });
		}

		if (ImGui::Button("Exit", { 210.0f * sizeMultiplier.x, 47.0f * sizeMultiplier.y }))
			Application::Get().Close();

		ImGui::SetCursorPos({ 857.0f * sizeMultiplier.x, 161.0f * sizeMultiplier.y });
		ImGui::BeginChild("Thumbnail", { 476.0f * sizeMultiplier.x, 281.0f * sizeMultiplier.y }, true);
		ImGui::EndChild();

		ImGui::End();

		ImGui::PopStyleVar();


		// ImGui::End();
	}

	void ProjectSelectionLayer::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<KeyPressedEvent>(SPIRIT_BIND_EVENT_FN(ProjectSelectionLayer::OnKeyPressed));
		dispatcher.Dispatch<MouseButtonPressedEvent>(SPIRIT_BIND_EVENT_FN(ProjectSelectionLayer::OnMouseButtonPressed));
	}

	bool ProjectSelectionLayer::OnKeyPressed(KeyPressedEvent& e)
	{
		// Shortcuts
		bool control = Input::IsKeyPressed(Key::LeftControl) || Input::IsKeyPressed(Key::RightControl);
		bool shift = Input::IsKeyPressed(Key::LeftShift) || Input::IsKeyPressed(Key::RightShift);

		if (e.GetRepeatCount() > 0 && !(control && e.GetKeyCode() == Key::D))
			return false;

		switch (e.GetKeyCode())
		{
		case Key::N:
		{
			if (control)
				CreateProject();

			break;
		}
		case Key::O:
		{
			if (control)
				OpenProject();

			break;
		}
		}

		return false;
	}

	bool ProjectSelectionLayer::OnMouseButtonPressed(MouseButtonPressedEvent& e)
	{
		// if (e.GetMouseButton() == Mouse::ButtonLeft && !ImGuizmo::IsUsing() && !ImGuizmo::IsOver() && !Input::IsKeyPressed(Key::LeftAlt) && m_ViewportHovered && m_ViewportFocused)
		// 	m_SceneHierarchyPanel.SetSelectedEntity(m_HoveredEntity);
		return false;
	}

	bool ProjectSelectionLayer::CreateProject()
	{
		NFD::UniquePath templateFilepath;
		nfdresult_t templateResult = NFD::PickFolder(templateFilepath);
		NFD::UniquePath filepath;
		nfdresult_t result = NFD::PickFolder(filepath);
		if (templateResult == NFD_OKAY && result == NFD_OKAY)
			return CreateProject(templateFilepath.get(), filepath.get());
		// std::string filepath = FileDialogs::OpenFile(FileTypes::GetProjectFilter().c_str());
		// if (!filepath.empty())
		// 	return CreateProject(filepath);

		return false;
	}

	bool ProjectSelectionLayer::CreateProject(const std::filesystem::path& templatePath, const std::filesystem::path& path)
	{
		bool isProject = false;
		std::filesystem::path file = "";
		std::string filename = "";
		if (std::filesystem::is_directory(templatePath))
		{
			for (auto& f : std::filesystem::directory_iterator(templatePath))
			{
				for (auto& t : FileTypes::GetProjectTypes())
				{
					auto& type = "." + t;
					if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
					{
						if (isProject)
						{
							SPIRIT_WARN("More than one project file found!");
							return false;
						}
						isProject = std::filesystem::exists(f.path());
						file = f.path();
						filename = file.filename().string();
					}
				}
				if (!isProject)
					filename = templatePath.filename().string();
			}
		}
		else
		{
			isProject = "assets" || FileTypes::IsProjectType(templatePath.has_extension() ? templatePath.extension().string() : path.filename().string());
			file = path;
			filename = file.filename().string();
		}

		if (!isProject)
		{
			SPIRIT_WARN("Could not load {0} - not a project", templatePath / filename);
			return false;
		}

		std::filesystem::copy(templatePath, path, std::filesystem::copy_options::recursive);

		mINI::INIFile projectFile((path / ".sproj").string());
		mINI::INIStructure projectINI;

		if (projectFile.read(projectINI))
		{
			projectINI["project"]["name"] = path.filename().string();
			projectFile.write(projectINI);
		}

		return true;
	}

	void ProjectSelectionLayer::OpenProject()
	{
		NFD::UniquePath filepath;
		nfdresult_t result = NFD::PickFolder(filepath);
		if (result == NFD_OKAY)
			OpenProject(filepath.get());
		// std::string filepath = FileDialogs::OpenFile(FileTypes::GetProjectFilter().c_str());
		// if (!filepath.empty())
		// 	OpenProject(filepath);
	}

	void ProjectSelectionLayer::OpenProject(const std::filesystem::path& path, bool forceExit)
	{
		bool isProject = path == std::filesystem::path("assets");
		std::filesystem::path file = "";
		std::string filename = "";
		if (std::filesystem::is_directory(path))
		{
			for (auto& f : std::filesystem::directory_iterator(path))
			{
				for (auto& t : FileTypes::GetProjectTypes())
				{
					auto& type = "." + t;
					if ((f.path().has_extension() ? f.path().extension().string() : f.path().filename().string()) == type)
					{
						if (isProject)
						{
							SPIRIT_WARN("More than one project file found!");
							return;
						}
						isProject = std::filesystem::exists(f.path());
						file = f.path();
						filename = file.filename().string();
					}
				}
				if (!isProject)
					filename = path.filename().string();
			}
		}
		else
		{
			isProject = FileTypes::IsProjectType(path.has_extension() ? path.extension().string() : path.filename().string());
			file = path;
			filename = file.filename().string();
		}

		if (!isProject)
		{
			SPIRIT_WARN("Could not load {0} - not a project", filename);
			return;
		}

		std::string projectName = "";

		mINI::INIFile projectFile(file.string());
		mINI::INIStructure projectINI;

		if (projectFile.read(projectINI))
			projectName = projectINI["project"]["name"];

		mINI::INIFile openedProjectsFile("configs/OpenedProjects.ini");
		mINI::INIStructure openedProjectsINI;
		openedProjectsFile.read(openedProjectsINI);
		std::string name = g_AssetPath.parent_path().filename().string();
		std::remove(name.begin(), name.end(), ' ');
		std::replace(name.begin(), name.end(), '\\', '/');
		openedProjectsINI["projects"][name] = g_AssetPath.parent_path().string();
		openedProjectsFile.write(openedProjectsINI, true);

		std::ofstream tempfile("temp.txt");
		if (tempfile.is_open())
		{
			tempfile << file.parent_path().string();
			tempfile << "\n";
			tempfile << projectName;
			tempfile.close();
		}

#if defined SPIRIT_PLATFORM_WINDOWS
		std::system("start SpiritEditor.exe");
#elif defined SPIRIT_PLATFORM_LINUX
		std::system("xterm ./SpiritEditor");
#elif defined SPIRIT_PLATFORM_MACOS
		std::system("osascript -e 'tell app \"Terminal\" to do script \"./SpiritEditor\"'");
#endif

		mINI::INIFile configFile("config/ProjectManager.ini");
		mINI::INIStructure configINI;

		if (forceExit)
			Application::Get().Close();
		if (configFile.read(configINI))
		{
			if (StringUtils::ToBoolean(configINI["general"]["AutoClose"]))
				Application::Get().Close();
		}
	}

}
