import sys
import os
import platform
from pathlib import Path

import Utils

class PremakeConfiguration:
    premakeVersion = "5.0.0-beta1"
    
    if platform.system() == "Windows":
        premakeUrls = f"https://github.com/premake/premake-core/releases/download/v{premakeVersion}/premake-{premakeVersion}-windows.zip"
    elif platform.system() == "Linux":
        premakeUrls = f"https://github.com/premake/premake-core/releases/download/v{premakeVersion}/premake-{premakeVersion}-linux.tar.gz"
    premakeLicenseUrl = "https://raw.githubusercontent.com/premake/premake-core/master/LICENSE.txt"
    premakeDirectory = "./vendor/premake/bin"

    @classmethod
    def Validate(cls):
        if (not cls.CheckIfPremakeInstalled()):
            print("Premake is not installed.")
            return False

        print(f"Correct Premake located at {os.path.abspath(cls.premakeDirectory)}")
        return True

    @classmethod
    def CheckIfPremakeInstalled(cls):
        if platform.system() == "Windows":
            premake = Path(f"{cls.premakeDirectory}/premake5.exe")
        elif platform.system() == "Linux":
            premake = Path(f"{cls.premakeDirectory}/premake5-linux")
        
        if not premake.exists():
            return cls.InstallPremake()

        return True

    @classmethod
    def InstallPremake(cls):
        permissionGranted = False
        while not permissionGranted:
            reply = str(input("Premake not found. Would you like to download Premake {0:s}? [Y/N]: ".format(cls.premakeVersion))).lower().strip()[:1]
            if reply == 'n':
                return False
            permissionGranted = (reply == 'y')

        if platform.system() == "Windows":
            premakePath = f"{cls.premakeDirectory}/premake-{cls.premakeVersion}-windows.zip"
        elif platform.system() == "Linux":
            premakePath = f"{cls.premakeDirectory}/premake-{cls.premakeVersion}-linux.tar.gz"

        print("Downloading {0:s} to {1:s}".format(cls.premakeUrls, premakePath))
        Utils.DownloadFile(cls.premakeUrls, premakePath)
        print("Extracting", premakePath)
        Utils.UnzipFile(premakePath, deleteZipFile=True)
        if platform.system() == "Linux":
            os.system(f"chmod +x {cls.premakeDirectory}/premake5")
            os.system(f"sudo cp {cls.premakeDirectory}/premake5 /usr/bin/")
            os.rename(cls.premakeDirectory + "/premake5", cls.premakeDirectory + "/premake5-linux")
        print(f"Premake {cls.premakeVersion} has been downloaded to '{cls.premakeDirectory}'")

        premakeLicensePath = f"{cls.premakeDirectory}/LICENSE.txt"
        print("Downloading {0:s} to {1:s}".format(cls.premakeLicenseUrl, premakeLicensePath))
        Utils.DownloadFile(cls.premakeLicenseUrl, premakeLicensePath)
        print(f"Premake License file has been downloaded to '{cls.premakeDirectory}'")

        return True
