import os
import subprocess
import platform

from SetupPython import PythonConfiguration as PythonRequirements

# Make sure everything we need for the setup is installed
PythonRequirements.Validate()

from SetupPremake import PremakeConfiguration as PremakeRequirements
if platform.system() == "Windows":
	from SetupVulkan import VulkanConfiguration as VulkanRequirements
os.chdir('./../') # Change from devtools/scripts directory to root

premakeInstalled = PremakeRequirements.Validate()
if platform.system() == "Windows":
	VulkanRequirements.Validate()

#if "MONO_PROJECT" not in os.environ:
#    print("You don't have the Mono environment variable set! If it isn't installed, do so now and return. When it's installed, provide it's path here.")
#    os.environ["MONO_PROJECT"] = input("What is the path to Mono? (On Windows it is 'C:\\Program Files\\Mono' by default)")

print("\nUpdating submodules...")
subprocess.call(["git", "submodule", "update", "--init", "--recursive"])

if (premakeInstalled):
    print("\nRunning premake...")
    if platform.system() == "Windows":
        path = os.path.abspath("./scripts/Win-GenProjects.bat")
    elif platform.system() == "Linux":
        path = os.path.abspath("./scripts/Lnx-GenProjects.sh")
    
    try:
        subprocess.call([path, "nopause"])
    except PermissionError:
        if platform.system() == "Linux":
            subprocess.call(["chmod", "-wx", path])
            subprocess.call([path, "nopause"])
    print("\nSetup completed!")
else:
    print("Spirit requires Premake to generate project files.")
