<!-- https://img.shields.io/gitlab/license/darubyminer360/SpiritEngine.svg -->
# Spirit [![License](https://img.shields.io/badge/License-Apache--2.0-green)](/LICENSE) ![Maintained](https://img.shields.io/badge/Maintained%3F-yes-green.svg)

![Spirit](/Resources/Branding/Spirit_Logo_Text_Light_Square.png?raw=true "Spirit")

Spirit is primarily an early-stage interactive application and rendering engine for Windows. Currently not much is implemented.

## Spirit support

Spirit is in active development. Here is a short list of what is supported and what isn't. This can change at any time.

### Supported platforms
Currently Spirit supports:

- Computer OS:
  - ![Windows Supported](https://img.shields.io/badge/Windows-win--64-green.svg)
  - ![Linux Partially Supported](https://img.shields.io/badge/Linux-Partially%20Supported-yellow.svg)
  - ![MacOS Not Supported](https://img.shields.io/badge/MacOS-Not%20Supported-red.svg)
- Mobile OS:
  - ![Android Not Supported](https://img.shields.io/badge/Android-Not%20Supported-red.svg)
  - ![IOS Not Supported](https://img.shields.io/badge/IOS-Not%20Supported-red.svg)

Windows is currently supported with plans for Linux, MacOS, Android, and IOS support in the future.

### Hardware requirements
As of now, Spirit only supports OpenGL Rendering and requires a minimum version of 4.5.

## Installing and setup

Start by cloning the repository with `git clone --recursive https://gitlab.com/DaRubyMiner360/SpiritEngine`.

If the repository was cloned non-recursively previously, use `git submodule update --init` to clone the necessary submodules.
Spirit uses _Premake 5_ as a build generation tool. Visit the [Premake website](https://premake.github.io/download.html) to download and install it.

Next: Follow the steps relevant to your operating system.

### Windows

1. Run the [Setup.bat](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/scripts/Setup.bat) file found in `scripts` folder. This will download the required prerequisites for the project if they are not present yet.
2. One prerequisite is the Vulkan SDK. If it is not installed, the script will execute the `VulkanSDK.exe` file, and will prompt the user to install the SDK.
3. After installation, run the [Setup.bat](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/scripts/Setup.bat) file again. If the Vulkan SDK is installed properly, it will then download the Vulkan SDK Debug libraries. (This may take a longer amount of time)
4. After downloading and unzipping the files, the [Win-GenProjects.bat](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/scripts/Win-GenProjects.bat) script file will get executed automatically, which will then generate a Visual Studio solution file for user's usage.

Premake 5.0.0-betaXX is then provided as [premake5.exe](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/vendor/bin/premake/premake5.exe) in the repository. Execute and follow the install instructions.

Premake generates project files for Visual Studio, Visual Studio 2017, 2019, or 2022 is recommended. To generate the `.sln` and `.vcxproj` files for Visual Studio 2017, run `premake vs2017` at the command line. Or you may run [Win-GenProjects.bat](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/Win-GenProjects.bat) as a convenience batch file for this task.

### Linux

1. Install all the Linux dependencies.
2. Run the [Setup.sh](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/scripts/Setup.sh) file found in `scripts` folder. This will download the required prerequisites for the project if they are not present yet.

Premake 5.0.0-betaXX is then provided as [premake5-linux](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/vendor/bin/premake/premake5-linux) in the repository. It's also been copied into `/usr/bin/`, so you can globally use it as `premake5`. Execute and follow the install instructions.

Spirit has extra development dependencies needed for Linux. The following packages are needed to compile the project:

- `libxcursor`
- `libxrandr`
- `libxinerama`
- `libxi`
- `zenity`

Vulkan is also required, but installing it is inconsistent between distributions.

You may run [Lnx-GenProjects.bat](https://gitlab.com/DaRubyMiner360/SpiritEngine/-/tree/main/Lnx-GenProjects.bat) as a convenience bash file for generating projects, or you can run `premake5 gmake2`.

Then, to compile Spirit, run `make`.

#### Arch

On Arch derivative distributions, install the additional dependencies by running:

`sudo pacman -S libxcursor libxrandr libxinerama libxi zenity`

Then, to install Vulkan, follow the steps bellow for your GPU:

##### AMD

On systems with an AMD GPU, install the additional dependencies by running:

`sudo pacman -S vulkan-radeon lib32-vulkan-radeon`

##### Nvidia

On systems with an Nvidia GPU, install the additional dependencies by running:

`sudo pacman -S nvidia lib32-nvidia-utils libvulkan-dev`

#### Debian

On Debian derivative distributions, install the additional dependencies by running:

`sudo apt install libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev libglu1-mesa-dev zenity libvulkan1 mesa-vulkan-drivers vulkan-utils libvulkan-dev`

---

## The Plan

The plan for Spirit is two-fold: to create a powerful 2D and 3D engine.

### Main features to come:

- Fast 2D rendering (UI, particles, sprites, etc.)
- High-fidelity Physically-Based 3D rendering (this will be expanded later, 2D to come first)
- Support for Mac, Linux, Android and iOS
  - Native rendering API support (DirectX, Vulkan, Metal)
- Fully featured viewer and editor applications
- Fully scripted interaction and behavior
- Integrated 3rd party 2D and 3D physics engine
- Procedural terrain and world generation
- Artificial Intelligence
- Audio system

## Short term goals :

By mid-2022, I want to make a game with Spirit using the proper tools that would be required to make a game. This means a full 2D workflow is needed:

- Design the game scene by using Spirit Editor,
- Test the game inside the editor, including the ability to save/load the created game,
- Load and play the game inside Sandbox.

I want everyone to be able to play the game on all desktop platforms (Windows, Mac and Linux). When this is implemented, another attempt at the "Creating a game in one hour using Spirit" will be made to see how far the engine has become.

[![Twitter](https://img.shields.io/badge/%40darubyminer360--blue.svg?style=social&logo=Twitter)](https://twitter.com/darubyminer360)
[![Youtube](https://img.shields.io/badge/DaRubyMiner360--red.svg?style=social&logo=youtube)](https://www.youtube.com/channel/UCJdZupuPgueQTLY2cg4L7yw)
