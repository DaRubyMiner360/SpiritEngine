#pragma once

#include "Spirit.h"

class Sandbox2D : public Spirit::Layer
{
public:
	Sandbox2D();
	virtual ~Sandbox2D() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	void OnUpdate(Spirit::Timestep ts) override;
	virtual void OnImGuiRender() override;
	void OnEvent(Spirit::Event& e) override;
private:
	Spirit::OrthographicCameraController m_CameraController;

	// Temp
	Spirit::Ref<Spirit::VertexArray> m_SquareVA;
	Spirit::Ref<Spirit::Shader> m_FlatColorShader;

	Spirit::Ref<Spirit::Texture2D> m_CheckerboardTexture;

	glm::vec4 m_SquareColor = { 0.2f, 0.3f, 0.8f, 1.0f };
};
