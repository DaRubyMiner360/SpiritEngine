#pragma once

#include "Spirit.h"

class ExampleLayer : public Spirit::Layer
{
public:
	ExampleLayer();
	virtual ~ExampleLayer() = default;

	virtual void OnAttach() override;
	virtual void OnDetach() override;

	void OnUpdate(Spirit::Timestep ts) override;
	virtual void OnImGuiRender() override;
	void OnEvent(Spirit::Event& e) override;
private:
	Spirit::ShaderLibrary m_ShaderLibrary;
	Spirit::Ref<Spirit::Shader> m_Shader;
	Spirit::Ref<Spirit::VertexArray> m_VertexArray;

	Spirit::Ref<Spirit::Shader> m_FlatColorShader;
	Spirit::Ref<Spirit::VertexArray> m_SquareVA;

	Spirit::Ref<Spirit::Texture2D> m_Texture, m_ChernoLogoTexture;

	Spirit::OrthographicCameraController m_CameraController;
	glm::vec3 m_SquareColor = { 0.2f, 0.3f, 0.8f };
};
