#include <Spirit.h>
#include <Spirit/Core/EntryPoint.h>

#include "Sandbox2D.h"
#include "ExampleLayer.h"

class Sandbox : public Spirit::Application
{
public:
	Sandbox(const Spirit::ApplicationSpecification& specification)
		: Spirit::Application(specification)
	{
		// PushLayer(new ExampleLayer());
		PushLayer(new Sandbox2D());
	}

	~Sandbox()
	{
	}
};

Spirit::Application* Spirit::CreateApplication(Spirit::ApplicationCommandLineArgs args)
{
	ApplicationSpecification spec;
	spec.Name = "Sandbox";
	spec.WorkingDirectory = "../SpiritEditor";
	spec.CommandLineArgs = args;

	return new Sandbox(spec);
}
