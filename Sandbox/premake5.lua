project "Sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("%{wks.location}/bin/" .. outputdir)
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"src/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"%{wks.location}/Spirit/vendor/spdlog/include",
		"%{wks.location}/Spirit/src",
		"%{wks.location}/Spirit/vendor",
		"%{IncludeDir.glm}",
		"%{IncludeDir.entt}"
	}

	links
	{
		"Spirit"
	}

	postbuildcommands
	{
		"{COPYDIR} \"%{LibraryDir.assimp_DLL}\" \"%{cfg.targetdir}\"",
			
		"{COPYDIR} \"%{wks.location}/%{prj.name}/assets\" \"%{cfg.targetdir}/assets\"",
		"{COPY} \"%{wks.location}/%{prj.name}/imgui.ini\" \"%{cfg.targetdir}/imgui.ini\""
	}

	filter "system:windows"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_WINDOWS"
		}

	filter { "system:windows", "action:gmake*" }
		links
		{
			"GLFW",
			"Glad",
			"ImGui",
			"yaml-cpp",
			"OpenGL32",
			"gdi32",
			"spirv-cross",
			"shaderc",
			"shaderc_util",
			"SPIRV-Tools-opt",
			"SPIRV-Tools",
			"MachineIndependent",
			"OSDependent",
			"GenericCodeGen",
			"OGLCompiler",
			"SPIRV"
		}

	filter "system:linux"
		links
		{
			"dl",
			"pthread",
			"GLFW",
			"GL",
			"Glad",
			"ImGui",
			"yaml-cpp",
			"X11",
			"spirv-cross",
			"shaderc",
			"shaderc_util",
			"SPIRV-Tools-opt",
			"SPIRV-Tools",
			"MachineIndependent",
			"OSDependent",
			"GenericCodeGen",
			"OGLCompiler",
			"SPIRV"


			--"Xrandr",
			--"Xi",
			--"GLU",
			--"stdc++fs",	--GCC versions 5.3 through 8.x need stdc++fs for std::filesystem

			--"vulkan",
			--"shaderc_shared",
			--"spirv-cross-c",
			--"spirv-cross-core",
			--"spirv-cross-cpp",
			--"spirv-cross-glsl",
			--"spirv-cross-reflect",
			--"spirv-cross-util",
			--"spirv-cross-c-shared",
			--"spirv-cross-hlsl",
			--"spirv-cross-msl"
		}

		defines
		{
			"SPIRIT_PLATFORM_LINUX"
		}

	filter "system:macosx"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_MACOS"
		}
					
	filter "configurations:Debug"
		debugdir "%{cfg.targetdir}"
		defines "SPIRIT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "SPIRIT_RELEASE"
		runtime "Release"
		optimize "on"
		kind "WindowedApp"
		entrypoint "mainCRTStartup"

	filter "configurations:Dist"
		defines "SPIRIT_DIST"
		runtime "Release"
		optimize "on"
