#include "spiritpch.h"
#include "Entity.h"

namespace Spirit {

	Entity::Entity(entt::entity handle, Scene* scene)
		: m_EntityHandle(handle), m_Scene(scene)
	{
		Children = {};
		First = nullptr;
		Previous = nullptr;
		Next = nullptr;
		Parent = nullptr;
	}

}
