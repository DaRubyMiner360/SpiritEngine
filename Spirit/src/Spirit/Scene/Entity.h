#pragma once

#include "Scene.h"

#include "Components.h"

#include "entt.hpp"

namespace Spirit {

	class Entity
	{
	public:
		Entity() = default;
		Entity(entt::entity handle, Scene* scene);
		Entity(const Entity& other) = default;

		Scene& GetScene()
		{
			return *m_Scene;
		}

		template<typename T, typename... Args>
		T& AddComponent(Args&&... args)
		{
			SPIRIT_CORE_ASSERT(!HasComponent<T>(), "Entity already has component!");
			T& component = m_Scene->m_Registry.emplace<T>(m_EntityHandle, std::forward<Args>(args)...);
			m_Scene->OnComponentAdded<T>(*this, component);
			return component;
		}

		template<typename T, typename... Args>
		T& AddOrReplaceComponent(Args&&... args)
		{
			T& component = m_Scene->m_Registry.emplace_or_replace<T>(m_EntityHandle, std::forward<Args>(args)...);
			m_Scene->OnComponentAdded<T>(*this, component);
			return component;
		}

		template<typename T, typename... Args>
		T& AddOrGetComponent(Args&&... args)
		{
			if (HasComponent<T>())
				return GetComponent<T>();
			else
				return AddComponent<T>(args...);
		}

		template<typename T>
		T& GetComponent()
		{
			SPIRIT_CORE_ASSERT(HasComponent<T>(), "Entity does not have component!");
			return m_Scene->m_Registry.get<T>(m_EntityHandle);
		}

		template<typename T>
		bool HasComponent()
		{
			return m_Scene->m_Registry.all_of<T>(m_EntityHandle);
		}

		template<typename T>
		void RemoveComponent()
		{
			SPIRIT_CORE_ASSERT(HasComponent<T>(), "Entity does not have component!");
			m_Scene->OnComponentRemoved<T>(*this, GetComponent<T>());
			m_Scene->m_Registry.remove<T>(m_EntityHandle);
		}

		void Destroy()
		{
			m_Scene->DestroyEntity(*this);
		}

		void UpdateRelationships()
		{
			if (Children.size() > 0)
			{
				for (UUID uuid : Children)
				{
					*m_Scene->GetEntityByID(uuid).Parent = *GetUUID();
				}
			}

			if (Parent && m_Scene->GetEntityByID(*Parent))
			{
				int childIndex = -1;
				for (int i = 0; i < m_Scene->GetEntityByID(*Parent).Children.size(); i++)
				{
					if (m_Scene->GetEntityByID(*Parent).Children[i] == GetUUID())
					{
						childIndex = i;
						break;
					}
				}

				if (childIndex != -1)
				{
					if (childIndex > 0)
						*Previous = *m_Scene->GetEntityByID(*Parent).Children[childIndex - 1];
					else
						Previous = nullptr;

					if (m_Scene->GetEntityByID(*Parent).Children.size() > childIndex + 1)
						*Next = *m_Scene->GetEntityByID(*Parent).Children[childIndex + 1];
					else
						Next = nullptr;
				}
			}

			if (Children.size() > 0)
				*First = *Children[0];
			else
				First = nullptr;

			for (UUID uuid : Children)
			{
				m_Scene->GetEntityByID(uuid).UpdateRelationships();
			}
		}

		operator bool() const { return m_EntityHandle != entt::null; }
		operator entt::entity() const { return m_EntityHandle; }
		operator uint32_t() const { return (uint32_t)m_EntityHandle; }

		UUID GetUUID() { return GetComponent<IDComponent>().ID; }
		UUID GetAssetID() { return GetComponent<IDComponent>().AssetID; }
		const std::string& GetName() { return GetComponent<TagComponent>().Name; }
		const std::string& GetTag() { return GetComponent<TagComponent>().Tag; }

		std::vector<UUID> Children = {};
		// TODO: Remove First and replace with method to get the first child
		UUID* First = nullptr;
		UUID* Previous = nullptr;
		UUID* Next = nullptr;
		UUID* Parent = nullptr;

		bool operator==(const Entity& other) const
		{
			return (!this && !other) || (this && other && m_EntityHandle == other.m_EntityHandle && m_Scene == other.m_Scene);
		}

		bool operator!=(const Entity& other) const
		{
			return !(*this == other);
		}
	private:
		entt::entity m_EntityHandle{ entt::null };
		Scene* m_Scene = nullptr;
	};

}
