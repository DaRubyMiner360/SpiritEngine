#pragma once

#include "SceneCamera.h"
#include "Spirit/Core/UUID.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/Model.h"

#include "Spirit/Utils/Audio.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <initializer_list>
#include <unordered_map>
#include <functional>
#include <vector>
#include <memory>

namespace Spirit {

	// TODO: Create a base Component struct that will contain a method with the logic for when the scene gets updated?
	// TODO: Have something that allows the component to choose if it should be run when simulating the scene?

	// Required components (not displayed)
	struct IDComponent
	{
		UUID ID;
		UUID AssetID;

		IDComponent() = default;
		IDComponent(const IDComponent&) = default;
	};

	struct TagComponent
	{
		std::string Name;
		std::string Tag;

		TagComponent() = default;
		TagComponent(const TagComponent&) = default;
		TagComponent(const std::string& name, const std::string& tag = std::string())
			: Name(name), Tag(tag) {}
	};

	// Required components (displayed)
	struct TransformComponent
	{
		glm::vec3 Translation = { 0.0f, 0.0f, 0.0f };
		glm::vec3 Rotation = { 0.0f, 0.0f, 0.0f };
		glm::vec3 Scale = { 1.0f, 1.0f, 1.0f };

		TransformComponent() = default;
		TransformComponent(const TransformComponent&) = default;
		TransformComponent(const glm::vec3& translation)
			: Translation(translation) {}

		glm::mat4 GetTransform() const
		{
			glm::mat4 rotation = glm::toMat4(glm::quat(Rotation));

			return glm::translate(glm::mat4(1.0f), Translation)
				* rotation
				* glm::scale(glm::mat4(1.0f), Scale);
		}
	};

	// Non-required components
	struct SpriteRendererComponent
	{
		glm::vec4 Color{ 1.0f, 1.0f, 1.0f, 1.0f };
		glm::vec2 TextureUVOffset = glm::vec2(0.0f);
		Ref<Texture2D> Texture;
		float TilingFactor = 1.0f;

		SpriteRendererComponent() = default;
		SpriteRendererComponent(const SpriteRendererComponent&) = default;
		SpriteRendererComponent(const glm::vec4& color)
			: Color(color) {}
	};

	struct CircleRendererComponent
	{
		glm::vec4 Color{ 1.0f, 1.0f, 1.0f, 1.0f };
		float Thickness = 1.0f;
		float Fade = 0.005f;

		CircleRendererComponent() = default;
		CircleRendererComponent(const CircleRendererComponent&) = default;
	};

	struct MeshRendererComponent
	{
		Ref<Model> Mesh;
		// TODO: Add a mapping selection (None, Normal, Parallax) selection

		glm::vec4 Color{ 1.0f, 1.0f, 1.0f, 1.0f };
		glm::vec2 TextureUVOffset = glm::vec2(0.0f);

		MeshRendererComponent() = default;
		MeshRendererComponent(const MeshRendererComponent&) = default;
	};

	struct DirectionalLightComponent
	{
		glm::vec3 Ambient;
		glm::vec3 Diffuse;
		glm::vec3 Specular;

		DirectionalLightComponent() = default;
		DirectionalLightComponent(const DirectionalLightComponent&) = default;
	};

	struct PointLightComponent
	{
		float Constant;
		float Linear;
		float Quadratic;

		glm::vec3 Ambient;
		glm::vec3 Diffuse;
		glm::vec3 Specular;

		PointLightComponent() = default;
		PointLightComponent(const PointLightComponent&) = default;
	};

	struct SpotLightComponent
	{
		float CutOff;
		float OuterCutOff;

		float Constant;
		float Linear;
		float Quadratic;

		glm::vec3 Ambient;
		glm::vec3 Diffuse;
		glm::vec3 Specular;

		SpotLightComponent() = default;
		SpotLightComponent(const SpotLightComponent&) = default;
	};

	struct CameraComponent
	{
		SceneCamera Camera;
		bool Primary = true; // TODO: think about moving to Scene
		bool FixedAspectRatio = false;

		CameraComponent() = default;
		CameraComponent(const CameraComponent&) = default;
	};

	// Physics

	struct Rigidbody2DComponent
	{
		enum class BodyType { Static = 0, Dynamic, Kinematic };
		BodyType Type = BodyType::Static;
		bool FixedRotation = false;

		// Storage for runtime
		void* RuntimeBody = nullptr;

		Rigidbody2DComponent() = default;
		Rigidbody2DComponent(const Rigidbody2DComponent&) = default;
	};

	struct BoxCollider2DComponent
	{
		glm::vec2 Offset = { 0.0f, 0.0f };
		glm::vec2 Size = { 0.5f, 0.5f };
		float Rotation = 0.0f;

		// TODO: move into physics material in the future maybe
		float Density = 1.0f;
		float Friction = 0.5f;
		float Restitution = 0.0f;
		float RestitutionThreshold = 0.5f;

		// Storage for runtime
		void* RuntimeFixture = nullptr;

		BoxCollider2DComponent() = default;
		BoxCollider2DComponent(const BoxCollider2DComponent&) = default;
	};

	struct CircleCollider2DComponent
	{
		glm::vec2 Offset = { 0.0f, 0.0f };
		float Radius = 0.5f;

		// TODO: move into physics material in the future maybe
		float Density = 1.0f;
		float Friction = 0.5f;
		float Restitution = 0.0f;
		float RestitutionThreshold = 0.5f;

		// Storage for runtime
		void* RuntimeFixture = nullptr;

		CircleCollider2DComponent() = default;
		CircleCollider2DComponent(const CircleCollider2DComponent&) = default;
	};

	struct AudioListenerComponent
	{
		bool Primary = true; // TODO: think about moving to Scene

		AudioListenerComponent() = default;
		AudioListenerComponent(const AudioListenerComponent&) = default;
	};

	struct AudioSourceComponent
	{
		AudioSource m_Source;

		bool PlayImmediately = true;
		float Gain = 1.0f;
		float Pitch = 1.0f;
		bool Spatial = false;
		bool Loop = false;

		//bool IsLoaded() const { return m_Source.IsLoaded(); }

		std::pair<uint32_t, uint32_t> GetLengthMinutesAndSeconds() const { return m_Source.GetLengthMinutesAndSeconds(); }

		void Play()
		{
			Audio::Play(m_Source);
		}

		AudioSourceComponent() = default;
		AudioSourceComponent(const AudioSourceComponent&) = default;
		AudioSourceComponent(const std::string& file, bool spatial = false, bool playImmediately = true)
		{
			m_Source = AudioSource::LoadFromFile(file, spatial);
			Gain = m_Source.GetGain();
			Pitch = m_Source.GetPitch();
			Spatial = m_Source.GetSpatial();
			Loop = m_Source.GetLoop();

			PlayImmediately = playImmediately;
		}
	};

	struct ScriptComponent
	{
		std::string ClassName;

		bool operator==(const ScriptComponent& other) const { return ClassName == other.ClassName; }

		ScriptComponent() = default;
		ScriptComponent(const ScriptComponent&) = default;
	};

	struct ScriptWrapperComponent
	{
		std::vector<ScriptComponent> Scripts;

		ScriptWrapperComponent() = default;
		ScriptWrapperComponent(const ScriptWrapperComponent&) = default;
	};

	// Forward declaration
	class ScriptableEntity;

	struct NativeScriptComponent
	{
		ScriptableEntity* Instance = nullptr;

		ScriptableEntity* (*InstantiateScript)();
		void (*DestroyScript)(NativeScriptComponent*);

		template<typename T>
		void Bind()
		{
			InstantiateScript = []() { return static_cast<ScriptableEntity*>(new T()); };
			DestroyScript = [](NativeScriptComponent* nsc) { delete nsc->Instance; nsc->Instance = nullptr; };
		}
	};

	template<typename... T>
	struct ComponentGroup {};
	using AllComponents = ComponentGroup
		<
		TransformComponent,
		SpriteRendererComponent,
		CircleRendererComponent,
		MeshRendererComponent,
		DirectionalLightComponent,
		PointLightComponent,
		SpotLightComponent,
		CameraComponent,
		Rigidbody2DComponent,
		BoxCollider2DComponent,
		CircleCollider2DComponent,
		AudioListenerComponent,
		AudioSourceComponent,
		ScriptWrapperComponent,
		NativeScriptComponent
		>;

}
