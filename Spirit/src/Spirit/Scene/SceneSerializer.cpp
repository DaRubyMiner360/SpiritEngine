#include "spiritpch.h"
#include "SceneSerializer.h"

#include "Entity.h"
#include "Components.h"
#include "Spirit/Utils/ScriptWrapperUtils.h"

#include <fstream>

#include <yaml-cpp/yaml.h>

namespace YAML {

	template<>
	struct convert<glm::vec2>
	{
		static Node encode(const glm::vec2& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.SetStyle(EmitterStyle::Flow);
			return node;
		}

		static bool decode(const Node& node, glm::vec2& rhs)
		{
			if (!node.IsSequence() || node.size() != 2)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			return true;
		}
	};

	template<>
	struct convert<glm::vec3>
	{
		static Node encode(const glm::vec3& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			node.SetStyle(EmitterStyle::Flow);
			return node;
		}

		static bool decode(const Node& node, glm::vec3& rhs)
		{
			if (!node.IsSequence() || node.size() != 3)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			return true;
		}
	};

	template<>
	struct convert<glm::vec4>
	{
		static Node encode(const glm::vec4& rhs)
		{
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			node.push_back(rhs.w);
			node.SetStyle(EmitterStyle::Flow);
			return node;
		}

		static bool decode(const Node& node, glm::vec4& rhs)
		{
			if (!node.IsSequence() || node.size() != 4)
				return false;

			rhs.x = node[0].as<float>();
			rhs.y = node[1].as<float>();
			rhs.z = node[2].as<float>();
			rhs.w = node[3].as<float>();
			return true;
		}
	};

}
namespace Spirit {

	void SerializeAllEntityComponents(YAML::Emitter& out, Entity entity);

	void DeserializeAllEntryComponents(YAML::detail::iterator_value& entity, Entity& deserializedEntity);
	void DeserializeAllEntryComponents(YAML::Node& entity, Entity& deserializedEntity);

	YAML::Emitter& operator<<(YAML::Emitter& out, const glm::vec2& v)
	{
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << YAML::EndSeq;
		return out;
	}

	YAML::Emitter& operator<<(YAML::Emitter& out, const glm::vec3& v)
	{
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << v.z << YAML::EndSeq;
		return out;
	}

	YAML::Emitter& operator<<(YAML::Emitter& out, const glm::vec4& v)
	{
		out << YAML::Flow;
		out << YAML::BeginSeq << v.x << v.y << v.z << v.w << YAML::EndSeq;
		return out;
	}

	static std::string RigidBody2DBodyTypeToString(Rigidbody2DComponent::BodyType bodyType)
	{
		switch (bodyType)
		{
		case Rigidbody2DComponent::BodyType::Static:    return "Static";
		case Rigidbody2DComponent::BodyType::Dynamic:   return "Dynamic";
		case Rigidbody2DComponent::BodyType::Kinematic: return "Kinematic";
		}

		SPIRIT_CORE_ASSERT(false, "Unknown body type");
		return {};
	}

	static Rigidbody2DComponent::BodyType RigidBody2DBodyTypeFromString(const std::string& bodyTypeString)
	{
		if (bodyTypeString == "Static")    return Rigidbody2DComponent::BodyType::Static;
		if (bodyTypeString == "Dynamic")   return Rigidbody2DComponent::BodyType::Dynamic;
		if (bodyTypeString == "Kinematic") return Rigidbody2DComponent::BodyType::Kinematic;

		SPIRIT_CORE_ASSERT(false, "Unknown body type");
		return Rigidbody2DComponent::BodyType::Static;
	}

	SceneSerializer::SceneSerializer(const Ref<Scene>& scene)
		: m_Scene(scene)
	{
	}

	void SerializeEntity(YAML::Emitter& out, Entity entity)
	{
		out << YAML::BeginMap; // Entity
		out << YAML::Key << "Entity" << YAML::Value << entity.GetUUID();
		out << YAML::Key << "AssetID" << YAML::Value << entity.GetAssetID();

		auto& children = entity.Children;
		out << YAML::Key << "Children" << YAML::Value << YAML::BeginSeq;
		for (UUID child : children)
		{
			out << child;
		}
		out << YAML::EndSeq; // Children

		// Serialize components (except IDComponent)
		SerializeAllEntityComponents(out, entity);

		out << YAML::EndMap; // Entity
	}

	void SceneSerializer::SerializeScene(const std::string& filepath)
	{
		YAML::Emitter out;
		out << YAML::BeginMap;
		out << YAML::Key << "Scene" << YAML::Value << m_Scene->Name;
		out << YAML::Key << "Entities" << YAML::Value << YAML::BeginSeq;
		m_Scene->m_Registry.each([&](auto entityID)
			{
				Entity entity = { entityID, m_Scene.get() };
				if (!entity)
					return;

				SerializeEntity(out, entity);
			});
		out << YAML::EndSeq;
		out << YAML::EndMap;

		std::ofstream fout(filepath);
		fout << out.c_str();
	}

	void SceneSerializer::SerializePrefab(const std::string& filepath, Entity entity)
	{
		YAML::Emitter out;
		SerializeEntity(out, entity);

		std::ofstream fout(filepath);
		fout << out.c_str();
	}

	void SceneSerializer::SerializeRuntime(const std::string& filepath)
	{
		// Not implemented
		SPIRIT_CORE_ASSERT(false);
	}

	bool SceneSerializer::DeserializeScene(const std::string& filepath)
	{
		YAML::Node data;
		try
		{
			data = YAML::LoadFile(filepath);
		}
		catch (const YAML::ParserException& ex)
		{
			SPIRIT_CORE_ERROR("Failed to deserialize scene '{0}'\n     {1}", filepath, ex.what());
			return false;
		}

		if (!data["Scene"])
			return false;

		m_Scene->Name = data["Scene"].as<std::string>();
		SPIRIT_CORE_TRACE("Deserializing scene '{0}'", m_Scene->Name);

		auto entities = data["Entities"];
		if (entities)
		{
			for (auto entity : entities)
			{
				uint64_t uuid = entity["Entity"].as<uint64_t>();

				std::string name, tag;
				auto tagComponent = entity["TagComponent"];
				if (tagComponent)
				{
					name = tagComponent["Name"].as<std::string>();
					tag = tagComponent["Tag"].as<std::string>();
				}

				SPIRIT_CORE_TRACE("Deserialized entity with ID = {0}, name = {1}", uuid, name);

				Entity deserializedEntity = m_Scene->CreateEntityWithUUID(uuid, name, tag);
				deserializedEntity.GetComponent<IDComponent>().AssetID = entity["AssetID"].as<uint64_t>();

				for (auto child : entity["Children"].as<std::vector<uint64_t>>())
				{
					deserializedEntity.Children.push_back(child);
				}

				// Deserialize components (except IDComponent and TagComponent)
				DeserializeAllEntryComponents(entity, deserializedEntity);
			}
		}
		m_Scene->UpdateRelationships();

		return true;
	}

	Entity SceneSerializer::DeserializePrefab(const std::string& filepath)
	{
		YAML::Node data;
		try
		{
			data = YAML::LoadFile(filepath);
		}
		catch (const YAML::ParserException& ex)
		{
			SPIRIT_CORE_ERROR("Failed to deserialize prefab '{0}'\n     {1}", filepath, ex.what());
			return {};
		}

		uint64_t uuid = UUID();

		std::string name, tag;
		auto tagComponent = data["TagComponent"];
		if (tagComponent)
		{
			name = tagComponent["Name"].as<std::string>();
			tag = tagComponent["Tag"].as<std::string>();
		}

		SPIRIT_CORE_TRACE("Deserialized entity with ID = {0}, name = {1}", uuid, name);

		Entity deserializedEntity = m_Scene->CreateEntityWithUUID(uuid, name, tag);
		deserializedEntity.GetComponent<IDComponent>().AssetID = data["AssetID"].as<uint64_t>();

		// Deserialize components (except IDComponent and TagComponent)
		DeserializeAllEntryComponents(data, deserializedEntity);

		return deserializedEntity;
	}

	bool SceneSerializer::DeserializeRuntime(const std::string& filepath)
	{
		// Not implemented
		SPIRIT_CORE_ASSERT(false);
		return false;
	}

	template<typename T>
	void SerializeEntityComponent(YAML::Emitter&, Entity) = delete;

	template<typename T>
	void DeserializeEntryComponent(YAML::detail::iterator_value& entity, Entity&) = delete;

	template<typename T>
	void DeserializeEntryComponent(YAML::Node& entity, Entity&) = delete;

	template<>
	void SerializeEntityComponent<TagComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<TagComponent>())
		{
			out << YAML::Key << "TagComponent";
			out << YAML::BeginMap; // TagComponent

			auto& tag = entity.GetComponent<TagComponent>().Tag;
			auto& name = entity.GetComponent<TagComponent>().Name;
			out << YAML::Key << "Name" << YAML::Value << name;
			out << YAML::Key << "Tag" << YAML::Value << tag;

			out << YAML::EndMap; // TagComponent
		}
	}

	template<>
	void SerializeEntityComponent<TransformComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<TransformComponent>())
		{
			out << YAML::Key << "TransformComponent";
			out << YAML::BeginMap; // TransformComponent

			auto& tc = entity.GetComponent<TransformComponent>();
			out << YAML::Key << "Translation" << YAML::Value << tc.Translation;
			out << YAML::Key << "Rotation" << YAML::Value << tc.Rotation;
			out << YAML::Key << "Scale" << YAML::Value << tc.Scale;

			out << YAML::EndMap; // TransformComponent
		}
	}

	template<>
	void DeserializeEntryComponent<TransformComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto transformComponent = entity["TransformComponent"];
		if (transformComponent)
		{
			// Entities always have transforms
			auto& tc = deserializedEntity.GetComponent<TransformComponent>();
			tc.Translation = transformComponent["Translation"].as<glm::vec3>();
			tc.Rotation = transformComponent["Rotation"].as<glm::vec3>();
			tc.Scale = transformComponent["Scale"].as<glm::vec3>();
		}
	}

	template<>
	void SerializeEntityComponent<CameraComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<CameraComponent>())
		{
			out << YAML::Key << "CameraComponent";
			out << YAML::BeginMap; // CameraComponent

			auto& cameraComponent = entity.GetComponent<CameraComponent>();
			auto& camera = cameraComponent.Camera;

			out << YAML::Key << "Camera" << YAML::Value;
			out << YAML::BeginMap; // Camera
			out << YAML::Key << "ProjectionType" << YAML::Value << (int)camera.GetProjectionType();
			out << YAML::Key << "PerspectiveFOV" << YAML::Value << camera.GetPerspectiveVerticalFOV();
			out << YAML::Key << "PerspectiveNear" << YAML::Value << camera.GetPerspectiveNearClip();
			out << YAML::Key << "PerspectiveFar" << YAML::Value << camera.GetPerspectiveFarClip();
			out << YAML::Key << "OrthographicSize" << YAML::Value << camera.GetOrthographicSize();
			out << YAML::Key << "OrthographicNear" << YAML::Value << camera.GetOrthographicNearClip();
			out << YAML::Key << "OrthographicFar" << YAML::Value << camera.GetOrthographicFarClip();
			out << YAML::EndMap; // Camera

			out << YAML::Key << "Primary" << YAML::Value << cameraComponent.Primary;
			out << YAML::Key << "FixedAspectRatio" << YAML::Value << cameraComponent.FixedAspectRatio;

			out << YAML::EndMap; // CameraComponent
		}
	}

	template<>
	void DeserializeEntryComponent<CameraComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto cameraComponent = entity["CameraComponent"];
		if (cameraComponent)
		{
			auto& cc = deserializedEntity.AddComponent<CameraComponent>();

			const auto cameraProps = cameraComponent["Camera"];
			cc.Camera.SetProjectionType((SceneCamera::ProjectionType)cameraProps["ProjectionType"].as<int>());

			cc.Camera.SetPerspectiveVerticalFOV(cameraProps["PerspectiveFOV"].as<float>());
			cc.Camera.SetPerspectiveNearClip(cameraProps["PerspectiveNear"].as<float>());
			cc.Camera.SetPerspectiveFarClip(cameraProps["PerspectiveFar"].as<float>());

			cc.Camera.SetOrthographicSize(cameraProps["OrthographicSize"].as<float>());
			cc.Camera.SetOrthographicNearClip(cameraProps["OrthographicNear"].as<float>());
			cc.Camera.SetOrthographicFarClip(cameraProps["OrthographicFar"].as<float>());

			cc.Primary = cameraComponent["Primary"].as<bool>();
			cc.FixedAspectRatio = cameraComponent["FixedAspectRatio"].as<bool>();
		}
	}

	template<>
	void SerializeEntityComponent<SpriteRendererComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<SpriteRendererComponent>())
		{
			out << YAML::Key << "SpriteRendererComponent";
			out << YAML::BeginMap; // SpriteRendererComponent

			auto& spriteRendererComponent = entity.GetComponent<SpriteRendererComponent>();
			if (spriteRendererComponent.Texture && std::filesystem::exists(spriteRendererComponent.Texture->GetPath()))
				out << YAML::Key << "TexturePath" << YAML::Value << spriteRendererComponent.Texture->GetPath();
			out << YAML::Key << "Color" << YAML::Value << spriteRendererComponent.Color;
			out << YAML::Key << "TextureUVOffset" << YAML::Value << spriteRendererComponent.TextureUVOffset;
			out << YAML::Key << "TilingFactor" << YAML::Value << spriteRendererComponent.TilingFactor;

			out << YAML::EndMap; // SpriteRendererComponent
		}
	}

	template<>
	void DeserializeEntryComponent<SpriteRendererComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto spriteRendererComponent = entity["SpriteRendererComponent"];
		if (spriteRendererComponent)
		{
			auto& src = deserializedEntity.AddComponent<SpriteRendererComponent>();
			if (spriteRendererComponent["TexturePath"])
				src.Texture = Texture2D::Create(spriteRendererComponent["TexturePath"].as<std::string>());
			src.Color = spriteRendererComponent["Color"].as<glm::vec4>();
			src.TextureUVOffset = spriteRendererComponent["TextureUVOffset"].as<glm::vec2>();

			if (spriteRendererComponent["TilingFactor"])
				src.TilingFactor = spriteRendererComponent["TilingFactor"].as<float>();
		}
	}

	template<>
	void SerializeEntityComponent<CircleRendererComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<CircleRendererComponent>())
		{
			out << YAML::Key << "CircleRendererComponent";
			out << YAML::BeginMap; // CircleRendererComponent

			auto& circleRendererComponent = entity.GetComponent<CircleRendererComponent>();
			out << YAML::Key << "Color" << YAML::Value << circleRendererComponent.Color;
			out << YAML::Key << "Thickness" << YAML::Value << circleRendererComponent.Thickness;
			out << YAML::Key << "Fade" << YAML::Value << circleRendererComponent.Fade;

			out << YAML::EndMap; // CircleRendererComponent
		}
	}

	template<>
	void DeserializeEntryComponent<CircleRendererComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto circleRendererComponent = entity["CircleRendererComponent"];
		if (circleRendererComponent)
		{
			auto& crc = deserializedEntity.AddComponent<CircleRendererComponent>();
			crc.Color = circleRendererComponent["Color"].as<glm::vec4>();
			crc.Thickness = circleRendererComponent["Thickness"].as<float>();
			crc.Fade = circleRendererComponent["Fade"].as<float>();
		}
	}

	template<>
	void SerializeEntityComponent<MeshRendererComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<MeshRendererComponent>())
		{
			out << YAML::Key << "MeshRendererComponent";
			out << YAML::BeginMap; // MeshRendererComponent

			auto& meshRendererComponent = entity.GetComponent<MeshRendererComponent>();
			out << YAML::Key << "Mesh" << YAML::Value << meshRendererComponent.Mesh->GetPath();
			out << YAML::Key << "Color" << YAML::Value << meshRendererComponent.Color;
			out << YAML::Key << "TextureUVOffset" << YAML::Value << meshRendererComponent.TextureUVOffset;

			out << YAML::EndMap; // MeshRendererComponent
		}
	}

	template<>
	void DeserializeEntryComponent<MeshRendererComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto meshRendererComponent = entity["MeshRendererComponent"];
		if (meshRendererComponent)
		{
			auto& src = deserializedEntity.AddComponent<MeshRendererComponent>();
			src.Mesh = Model::Create(meshRendererComponent["Mesh"].as<std::string>());
			src.Color = meshRendererComponent["Color"].as<glm::vec4>();
			src.TextureUVOffset = meshRendererComponent["TextureUVOffset"].as<glm::vec2>();
		}
	}

	template<>
	void SerializeEntityComponent<DirectionalLightComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<DirectionalLightComponent>())
		{
			out << YAML::Key << "DirectionalLightComponent";
			out << YAML::BeginMap; // DirectionalLightComponent

			auto& directionalLightComponent = entity.GetComponent<DirectionalLightComponent>();
			out << YAML::Key << "Ambient" << YAML::Value << directionalLightComponent.Ambient;
			out << YAML::Key << "Diffuse" << YAML::Value << directionalLightComponent.Diffuse;
			out << YAML::Key << "Specular" << YAML::Value << directionalLightComponent.Specular;

			out << YAML::EndMap; // DirectionalLightComponent
		}
	}

	template<>
	void DeserializeEntryComponent<DirectionalLightComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto directionalLightComponent = entity["DirectionalLightComponent"];
		if (directionalLightComponent)
		{
			auto& src = deserializedEntity.AddComponent<DirectionalLightComponent>();
			src.Ambient = directionalLightComponent["Ambient"].as<glm::vec3>();
			src.Diffuse = directionalLightComponent["Diffuse"].as<glm::vec3>();
			src.Specular = directionalLightComponent["Specular"].as<glm::vec3>();
		}
	}

	template<>
	void SerializeEntityComponent<PointLightComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<PointLightComponent>())
		{
			out << YAML::Key << "PointLightComponent";
			out << YAML::BeginMap; // PointLightComponent

			auto& pointLightComponent = entity.GetComponent<PointLightComponent>();
			out << YAML::Key << "Constant" << YAML::Value << pointLightComponent.Constant;
			out << YAML::Key << "Linear" << YAML::Value << pointLightComponent.Linear;
			out << YAML::Key << "Quadratic" << YAML::Value << pointLightComponent.Quadratic;

			out << YAML::Key << "Ambient" << YAML::Value << pointLightComponent.Ambient;
			out << YAML::Key << "Diffuse" << YAML::Value << pointLightComponent.Diffuse;
			out << YAML::Key << "Specular" << YAML::Value << pointLightComponent.Specular;

			out << YAML::EndMap; // PointLightComponent
		}
	}

	template<>
	void DeserializeEntryComponent<PointLightComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto pointLightComponent = entity["PointLightComponent"];
		if (pointLightComponent)
		{
			auto& src = deserializedEntity.AddComponent<PointLightComponent>();
			src.Constant = pointLightComponent["Constant"].as<float>();
			src.Linear = pointLightComponent["Linear"].as<float>();
			src.Quadratic = pointLightComponent["Quadratic"].as<float>();

			src.Ambient = pointLightComponent["Ambient"].as<glm::vec3>();
			src.Diffuse = pointLightComponent["Diffuse"].as<glm::vec3>();
			src.Specular = pointLightComponent["Specular"].as<glm::vec3>();
		}
	}

	template<>
	void SerializeEntityComponent<SpotLightComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<SpotLightComponent>())
		{
			out << YAML::Key << "SpotLightComponent";
			out << YAML::BeginMap; // SpotLightComponent

			auto& spotLightComponent = entity.GetComponent<SpotLightComponent>();
			out << YAML::Key << "CutOff" << YAML::Value << spotLightComponent.CutOff;
			out << YAML::Key << "OuterCutOff" << YAML::Value << spotLightComponent.OuterCutOff;

			out << YAML::Key << "Constant" << YAML::Value << spotLightComponent.Constant;
			out << YAML::Key << "Linear" << YAML::Value << spotLightComponent.Linear;
			out << YAML::Key << "Quadratic" << YAML::Value << spotLightComponent.Quadratic;

			out << YAML::Key << "Ambient" << YAML::Value << spotLightComponent.Ambient;
			out << YAML::Key << "Diffuse" << YAML::Value << spotLightComponent.Diffuse;
			out << YAML::Key << "Specular" << YAML::Value << spotLightComponent.Specular;

			out << YAML::EndMap; // SpotLightComponent
		}
	}

	template<>
	void DeserializeEntryComponent<SpotLightComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto spotLightComponent = entity["SpotLightComponent"];
		if (spotLightComponent)
		{
			auto& src = deserializedEntity.AddComponent<SpotLightComponent>();
			src.CutOff = spotLightComponent["CutOff"].as<float>();
			src.OuterCutOff = spotLightComponent["OuterCutOff"].as<float>();

			src.Constant = spotLightComponent["Constant"].as<float>();
			src.Linear = spotLightComponent["Linear"].as<float>();
			src.Quadratic = spotLightComponent["Quadratic"].as<float>();

			src.Ambient = spotLightComponent["Ambient"].as<glm::vec3>();
			src.Diffuse = spotLightComponent["Diffuse"].as<glm::vec3>();
			src.Specular = spotLightComponent["Specular"].as<glm::vec3>();
		}
	}

	template<>
	void SerializeEntityComponent<Rigidbody2DComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<Rigidbody2DComponent>())
		{
			out << YAML::Key << "Rigidbody2DComponent";
			out << YAML::BeginMap; // Rigidbody2DComponent

			auto& rb2dComponent = entity.GetComponent<Rigidbody2DComponent>();
			out << YAML::Key << "BodyType" << YAML::Value << RigidBody2DBodyTypeToString(rb2dComponent.Type);
			out << YAML::Key << "FixedRotation" << YAML::Value << rb2dComponent.FixedRotation;

			out << YAML::EndMap; // Rigidbody2DComponent
		}
	}

	template<>
	void DeserializeEntryComponent<Rigidbody2DComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto rigidbody2DComponent = entity["Rigidbody2DComponent"];
		if (rigidbody2DComponent)
		{
			auto& rb2d = deserializedEntity.AddComponent<Rigidbody2DComponent>();
			rb2d.Type = RigidBody2DBodyTypeFromString(rigidbody2DComponent["BodyType"].as<std::string>());
			rb2d.FixedRotation = rigidbody2DComponent["FixedRotation"].as<bool>();
		}
	}

	template<>
	void SerializeEntityComponent<BoxCollider2DComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<BoxCollider2DComponent>())
		{
			out << YAML::Key << "BoxCollider2DComponent";
			out << YAML::BeginMap; // BoxCollider2DComponent

			auto& bc2dComponent = entity.GetComponent<BoxCollider2DComponent>();
			out << YAML::Key << "Offset" << YAML::Value << bc2dComponent.Offset;
			out << YAML::Key << "Size" << YAML::Value << bc2dComponent.Size;
			out << YAML::Key << "Density" << YAML::Value << bc2dComponent.Density;
			out << YAML::Key << "Friction" << YAML::Value << bc2dComponent.Friction;
			out << YAML::Key << "Restitution" << YAML::Value << bc2dComponent.Restitution;
			out << YAML::Key << "RestitutionThreshold" << YAML::Value << bc2dComponent.RestitutionThreshold;

			out << YAML::EndMap; // BoxCollider2DComponent
		}
	}

	template<>
	void DeserializeEntryComponent<BoxCollider2DComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto boxCollider2DComponent = entity["BoxCollider2DComponent"];
		if (boxCollider2DComponent)
		{
			auto& bc2d = deserializedEntity.AddComponent<BoxCollider2DComponent>();
			bc2d.Offset = boxCollider2DComponent["Offset"].as<glm::vec2>();
			bc2d.Size = boxCollider2DComponent["Size"].as<glm::vec2>();
			bc2d.Density = boxCollider2DComponent["Density"].as<float>();
			bc2d.Friction = boxCollider2DComponent["Friction"].as<float>();
			bc2d.Restitution = boxCollider2DComponent["Restitution"].as<float>();
			bc2d.RestitutionThreshold = boxCollider2DComponent["RestitutionThreshold"].as<float>();
		}
	}

	template<>
	void SerializeEntityComponent<CircleCollider2DComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<CircleCollider2DComponent>())
		{
			out << YAML::Key << "CircleCollider2DComponent";
			out << YAML::BeginMap; // CircleCollider2DComponent

			auto& cc2dComponent = entity.GetComponent<CircleCollider2DComponent>();
			out << YAML::Key << "Offset" << YAML::Value << cc2dComponent.Offset;
			out << YAML::Key << "Radius" << YAML::Value << cc2dComponent.Radius;
			out << YAML::Key << "Density" << YAML::Value << cc2dComponent.Density;
			out << YAML::Key << "Friction" << YAML::Value << cc2dComponent.Friction;
			out << YAML::Key << "Restitution" << YAML::Value << cc2dComponent.Restitution;
			out << YAML::Key << "RestitutionThreshold" << YAML::Value << cc2dComponent.RestitutionThreshold;

			out << YAML::EndMap; // CircleCollider2DComponent
		}
	}

	template<>
	void DeserializeEntryComponent<CircleCollider2DComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto circleCollider2DComponent = entity["CircleCollider2DComponent"];
		if (circleCollider2DComponent)
		{
			auto& cc2d = deserializedEntity.AddComponent<CircleCollider2DComponent>();
			cc2d.Offset = circleCollider2DComponent["Offset"].as<glm::vec2>();
			cc2d.Radius = circleCollider2DComponent["Radius"].as<float>();
			cc2d.Density = circleCollider2DComponent["Density"].as<float>();
			cc2d.Friction = circleCollider2DComponent["Friction"].as<float>();
			cc2d.Restitution = circleCollider2DComponent["Restitution"].as<float>();
			cc2d.RestitutionThreshold = circleCollider2DComponent["RestitutionThreshold"].as<float>();
		}
	}

	template<>
	void SerializeEntityComponent<AudioListenerComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<AudioListenerComponent>())
		{
			out << YAML::Key << "AudioListenerComponent";
			out << YAML::BeginMap; // AudioListenerComponent

			auto& audioListenerComponent = entity.GetComponent<AudioListenerComponent>();
			out << YAML::Key << "Primary" << YAML::Value << audioListenerComponent.Primary;

			out << YAML::EndMap; // AudioListenerComponent
		}
	}

	template<>
	void DeserializeEntryComponent<AudioListenerComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto audioListenerComponent = entity["AudioListenerComponent"];
		if (audioListenerComponent)
		{
			auto& listener = deserializedEntity.AddComponent<AudioListenerComponent>();

			listener.Primary = audioListenerComponent["Primary"].as<bool>();
		}
	}

	template<>
	void SerializeEntityComponent<AudioSourceComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<AudioSourceComponent>())
		{
			out << YAML::Key << "AudioSourceComponent";
			out << YAML::BeginMap; // AudioSourceComponent

			auto& audioSourceComponent = entity.GetComponent<AudioSourceComponent>();

			out << YAML::Key << "PlayImmediately" << YAML::Value << audioSourceComponent.PlayImmediately;
			out << YAML::Key << "Gain" << YAML::Value << audioSourceComponent.Gain;
			out << YAML::Key << "Pitch" << YAML::Value << audioSourceComponent.Pitch;
			out << YAML::Key << "Spatial" << YAML::Value << audioSourceComponent.Spatial;
			out << YAML::Key << "Loop" << YAML::Value << audioSourceComponent.Loop;

			out << YAML::EndMap; // AudioSourceComponent
		}
	}

	template<>
	void DeserializeEntryComponent<AudioSourceComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto audioSourceComponent = entity["AudioSourceComponent"];
		if (audioSourceComponent)
		{
			auto& source = deserializedEntity.AddComponent<AudioSourceComponent>();

			source.PlayImmediately = audioSourceComponent["PlayImmediately"].as<bool>();
			source.Gain = audioSourceComponent["Gain"].as<float>();
			source.Pitch = audioSourceComponent["Pitch"].as<float>();
			source.Spatial = audioSourceComponent["Spatial"].as<bool>();
			source.Loop = audioSourceComponent["Loop"].as<bool>();
		}
	}

	void SerializeScriptComponent(YAML::Emitter& out, Entity entity, ScriptComponent& scriptComponent)
	{
		out << YAML::BeginMap; // Script
		out << YAML::Key << "Script" << YAML::Value << scriptComponent.ClassName;

		// TODO: Serialize public fields here.

		out << YAML::EndMap; // Script
	}

	void DeserializeScriptComponent(YAML::detail::iterator_value& serializedScript, Entity& deserializedEntity)
	{
		if (serializedScript)
		{
			auto& script = ScriptComponent();
			script.ClassName = serializedScript["Script"].as<std::string>();
			ScriptWrapperUtils::AddComponent(deserializedEntity, script);

			// TODO: Deserialize public fields here.
		}
	}

	template<>
	void SerializeEntityComponent<ScriptWrapperComponent>(YAML::Emitter& out, Entity entity)
	{
		if (entity.HasComponent<ScriptWrapperComponent>())
		{
			out << YAML::Key << "ScriptWrapperComponent";
			out << YAML::BeginMap; // ScriptWrapperComponent

			auto& scriptWrapperComponent = entity.GetComponent<ScriptWrapperComponent>();

			out << YAML::Key << "Scripts" << YAML::Value << YAML::BeginSeq;
			for (auto& script : scriptWrapperComponent.Scripts)
				SerializeScriptComponent(out, entity, script);
			out << YAML::EndSeq;

			out << YAML::EndMap; // ScriptWrapperComponent
		}
	}

	template<>
	void DeserializeEntryComponent<ScriptWrapperComponent>(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		auto scriptWrapperComponent = entity["ScriptWrapperComponent"];
		if (scriptWrapperComponent)
		{
			auto& scriptWrapper = deserializedEntity.AddComponent<ScriptWrapperComponent>();

			auto scripts = scriptWrapperComponent["Scripts"];
			if (scripts)
			{
				for (auto script : scripts)
				{
					DeserializeScriptComponent(script, deserializedEntity);
				}
			}
		}
	}

	template<>
	void SerializeEntityComponent<NativeScriptComponent>(YAML::Emitter& out, Entity entity)
	{
		// TODO: serialize NativeScriptComponent?
	}

	template<>
	void DeserializeEntryComponent<NativeScriptComponent>(YAML::detail::iterator_value&, Entity&)
	{
		// TODO: deserialize NativeScriptComponent?
	}

	template<typename...T>
	void SerializeEntityComponents(YAML::Emitter& out, Entity entity)
	{
		(SerializeEntityComponent<T>(out, entity), ...);
	}

	template<typename...T>
	void DeserializeEntryComponents(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		(DeserializeEntryComponent<T>(entity, deserializedEntity), ...);
	}

	template<typename...T>
	void DeserializeEntryComponents(YAML::Node& entity, Entity& deserializedEntity)
	{
		(DeserializeEntryComponent<T>((YAML::detail::iterator_value)entity, deserializedEntity), ...);
	}

	template<typename... T, typename...V>
	void SerializeEntityComponents(ComponentGroup<V...>, YAML::Emitter& out, Entity entity)
	{
		SerializeEntityComponents<T..., V...>(out, entity);
	}

	template<typename... T, typename...V>
	void DeserializeEntryComponents(ComponentGroup<V...>, YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		DeserializeEntryComponents<T..., V...>(entity, deserializedEntity);
	}

	template<typename... T, typename...V>
	void DeserializeEntryComponents(ComponentGroup<V...>, YAML::Node& entity, Entity& deserializedEntity)
	{
		DeserializeEntryComponents<T..., V...>((YAML::detail::iterator_value)entity, deserializedEntity);
	}

	void SerializeAllEntityComponents(YAML::Emitter& out, Entity entity)
	{
		SerializeEntityComponents<TagComponent>(AllComponents{}, out, entity);
	}

	void DeserializeAllEntryComponents(YAML::detail::iterator_value& entity, Entity& deserializedEntity)
	{
		DeserializeEntryComponents(AllComponents{}, entity, deserializedEntity);
	}

	void DeserializeAllEntryComponents(YAML::Node& entity, Entity& deserializedEntity)
	{
		DeserializeEntryComponents(AllComponents{}, (YAML::detail::iterator_value&)entity, deserializedEntity);
	}

}
