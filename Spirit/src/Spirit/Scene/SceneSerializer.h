#pragma once

#include "Scene.h"

namespace Spirit {

	class SceneSerializer
	{
	public:
		SceneSerializer(const Ref<Scene>& scene);

		void SerializeScene(const std::string& filepath);
		void SerializePrefab(const std::string& filepath, Entity entity);
		void SerializeRuntime(const std::string& filepath);

		bool DeserializeScene(const std::string& filepath);
		Entity DeserializePrefab(const std::string& filepath);
		bool DeserializeRuntime(const std::string& filepath);
	private:
		Ref<Scene> m_Scene;
	};

}
