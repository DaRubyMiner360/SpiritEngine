#pragma once

#include "Spirit/Core/Timestep.h"
#include "Spirit/Core/UUID.h"
#include "Spirit/Renderer/EditorCamera.h"

#include "entt.hpp"

#include <vector>

class b2World;

namespace Spirit {

	class Entity;

	class Scene
	{
	public:
		Scene(const std::string& name = "Untitled");
		~Scene();

		Entity CreateEntity(const std::string& name = std::string(), const std::string& tag = std::string());
		Entity CreateEntityWithUUID(UUID uuid, const std::string& name = std::string(), const std::string& tag = std::string());
		void DestroyEntity(Entity entity);

		void OnRuntimeStart();
		void OnRuntimeStop();

		void OnSimulationStart();
		void OnSimulationStop();

		void OnUpdateRuntime(Timestep ts);
		void OnUpdateSimulation(Timestep ts, EditorCamera& camera);
		void OnUpdateEditor(Timestep ts, EditorCamera& camera);
		void OnViewportResize(uint32_t width, uint32_t height);

		Entity GetPrimaryCameraEntity();
		Entity GetPrimaryAudioListenerEntity();

		template<typename... Components>
		auto GetAllEntitiesWith()
		{
			return m_Registry.view<Components...>();
		}

		std::vector<Entity> GetEntities();
		Entity GetEntityByID(UUID id);
		std::vector<Entity> GetEntitiesByAssetID(UUID id);

		Entity DuplicateEntity(Entity entity);

		Entity GetEntityByUUID(UUID uuid);

		void UpdateRelationships();

		static Ref<Scene> Copy(Ref<Scene> scene);
		static Ref<Scene> ReversedCopy(Ref<Scene> scene);
	public:
		template<typename T>
		void OnComponentAdded(Entity entity, T& component);
		template<typename T>
		void OnComponentModified(Entity entity, T& component);
		template<typename T>
		void OnComponentRemoved(Entity entity, T& component);

		void OnPhysics2DStart();
		void OnPhysics2DStop();

		void RenderScene(EditorCamera& camera);
	public:
		std::string Name;
	private:
		bool m_IsRuntimeStarted = false;

		entt::registry m_Registry;
		uint32_t m_ViewportWidth = 0, m_ViewportHeight = 0;

		b2World* m_PhysicsWorld = nullptr;

		std::unordered_map<UUID, entt::entity> m_EntityMap;

		friend class Entity;
		friend class SceneSerializer;
		friend class SceneHierarchyPanel;
	};

}
