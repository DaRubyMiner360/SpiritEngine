#include "spiritpch.h"
#include "Scene.h"

#include "Entity.h"
#include "ScriptableEntity.h"
#include "Components.h"
#include "Spirit/Scripting/ScriptEngine.h"

#include "Spirit/Renderer/Renderer2D.h"
#include "Spirit/Renderer/Renderer3D.h"
#include "Spirit/Renderer/Model.h"

#include "Spirit/Utils/Audio.h"

#include <glm/glm.hpp>
#include <glad/glad.h>

// Box2D
#include "box2d/b2_world.h"
#include "box2d/b2_body.h"
#include "box2d/b2_fixture.h"
#include "box2d/b2_polygon_shape.h"
#include "box2d/b2_circle_shape.h"

namespace Spirit {

	static b2BodyType Rigidbody2DTypeToBox2DBody(Rigidbody2DComponent::BodyType bodyType)
	{
		switch (bodyType)
		{
		case Rigidbody2DComponent::BodyType::Static:    return b2_staticBody;
		case Rigidbody2DComponent::BodyType::Dynamic:   return b2_dynamicBody;
		case Rigidbody2DComponent::BodyType::Kinematic: return b2_kinematicBody;
		}

		SPIRIT_CORE_ASSERT(false, "Unknown body type");
		return b2_staticBody;
	}

	Scene::Scene(const std::string& name)
		: Name(name)
	{
	}

	Scene::~Scene()
	{
		delete m_PhysicsWorld;
	}

	template<typename... Component>
	static void CopyComponent(entt::registry& dst, entt::registry& src, const std::unordered_map<UUID, entt::entity>& enttMap)
	{
		([&]()
			{
				auto view = src.view<Component>();
				for (auto srcEntity : view)
				{
					entt::entity dstEntity = enttMap.at(src.get<IDComponent>(srcEntity).ID);

					auto& srcComponent = src.get<Component>(srcEntity);
					dst.emplace_or_replace<Component>(dstEntity, srcComponent);
				}
			}(), ...);
	}

	template<typename... Component>
	static void CopyComponent(ComponentGroup<Component...>, entt::registry& dst, entt::registry& src, const std::unordered_map<UUID, entt::entity>& enttMap)
	{
		CopyComponent<Component...>(dst, src, enttMap);
	}

	template<typename... Component>
	static void CopyComponentIfExists(Entity dst, Entity src)
	{
		([&]()
			{
				if (src.HasComponent<Component>())
					dst.AddOrReplaceComponent<Component>(src.GetComponent<Component>());
			}(), ...);
	}

	template<typename... Component>
	static void CopyComponentIfExists(ComponentGroup<Component...>, Entity dst, Entity src)
	{
		CopyComponentIfExists<Component...>(dst, src);
	}

	static void CopyAllComponents(entt::registry& dstSceneRegistry, entt::registry& srcSceneRegistry, const std::unordered_map<UUID, entt::entity>& enttMap)
	{
		CopyComponent(AllComponents{}, dstSceneRegistry, srcSceneRegistry, enttMap);
	}

	static void CopyAllExistingComponents(Entity dst, Entity src)
	{
		CopyComponentIfExists(AllComponents{}, dst, src);
	}

	Ref<Scene> Scene::Copy(Ref<Scene> other)
	{
		return ReversedCopy(ReversedCopy(other));
	}

	Ref<Scene> Scene::ReversedCopy(Ref<Scene> other)
	{
		Ref<Scene> newScene = CreateRef<Scene>();

		newScene->m_ViewportWidth = other->m_ViewportWidth;
		newScene->m_ViewportHeight = other->m_ViewportHeight;

		auto& srcSceneRegistry = other->m_Registry;
		auto& dstSceneRegistry = newScene->m_Registry;
		std::unordered_map<UUID, entt::entity> enttMap;

		// Create entities in new scene
		auto idView = srcSceneRegistry.view<IDComponent>();
		for (auto e : idView)
		{
			UUID uuid = srcSceneRegistry.get<IDComponent>(e).ID;
			const auto& name = srcSceneRegistry.get<TagComponent>(e).Name;
			const auto& tag = srcSceneRegistry.get<TagComponent>(e).Tag;
			Entity newEntity = newScene->CreateEntityWithUUID(uuid, name, tag);
			enttMap[uuid] = (entt::entity)newEntity;
		}

		// Copy components (except IDComponent and TagComponent)
		CopyAllComponents(dstSceneRegistry, srcSceneRegistry, enttMap);

		return newScene;
	}

	Entity Scene::CreateEntity(const std::string& name, const std::string& tag)
	{
		return CreateEntityWithUUID(UUID(), name, tag);
	}

	Entity Scene::CreateEntityWithUUID(UUID uuid, const std::string& name, const std::string& tag)
	{
		Entity entity = { m_Registry.create(), this };
		entity.AddComponent<IDComponent>(uuid);
		entity.AddComponent<TransformComponent>();
		auto& tagComp = entity.AddComponent<TagComponent>();
		tagComp.Name = name.empty() ? "New Entity" : name;
		tagComp.Tag = tag.empty() ? "Entity" : tag;

		m_EntityMap[uuid] = (entt::entity)entity;

		return entity;
	}

	void Scene::DestroyEntity(Entity entity)
	{
		if (m_IsRuntimeStarted && entity.HasComponent<ScriptWrapperComponent>() && !entity.GetComponent<ScriptWrapperComponent>().Scripts.empty())
			ScriptEngine::OnDestroy(entity);

		for (UUID uuid : entity.Children)
		{
			Entity child = GetEntityByID(uuid);
			if (child)
			{
				if (m_IsRuntimeStarted && entity.HasComponent<ScriptWrapperComponent>() && !entity.GetComponent<ScriptWrapperComponent>().Scripts.empty())
					ScriptEngine::OnDestroy(child);

				m_EntityMap.erase(child.GetUUID());
				m_Registry.destroy(child);
			}
		}
		m_EntityMap.erase(entity.GetUUID());
		m_Registry.destroy(entity);
	}

	void Scene::OnRuntimeStart()
	{
		m_IsRuntimeStarted = true;

		OnPhysics2DStart();

		// Scripting
		{
			ScriptEngine::OnRuntimeStart(this);

			// Instantiate all script entities
			auto view = m_Registry.view<ScriptWrapperComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				// Iterating twice to initialize everything.
				for (auto script : entity.GetComponent<ScriptWrapperComponent>().Scripts)
					ScriptEngine::OnInit(entity, script);
				for (auto script : entity.GetComponent<ScriptWrapperComponent>().Scripts)
					ScriptEngine::OnCreate(entity, script);
			}
		}

		// Audio
		{
			Audio::Init();

			auto view = m_Registry.view<AudioSourceComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				if (entity.GetComponent<AudioSourceComponent>().PlayImmediately)
					entity.GetComponent<AudioSourceComponent>().Play();
			}
		}
	}

	void Scene::OnRuntimeStop()
	{
		m_IsRuntimeStarted = false;

		OnPhysics2DStop();

		ScriptEngine::OnRuntimeStop();

		// Audio
		{
			//auto view = m_Registry.view<AudioSourceComponent>();
			//for (auto e : view)
			//{
			//	Entity entity = { e, this };
			//	entity.GetComponent<AudioSourceComponent>().Stop();
			//}

			Audio::Deinit();
		}
	}

	void Scene::OnSimulationStart()
	{
		OnPhysics2DStart();
	}

	void Scene::OnSimulationStop()
	{
		OnPhysics2DStop();
	}

	void Scene::OnUpdateRuntime(Timestep ts)
	{
		// Update scripts
		{
			// C# Entity OnUpdate
			auto view = m_Registry.view<ScriptWrapperComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				if (!entity.GetComponent<ScriptWrapperComponent>().Scripts.empty())
					ScriptEngine::OnUpdate(entity, ts);
			}

			m_Registry.view<NativeScriptComponent>().each([=](auto entity, auto& nsc)
				{
					// TODO: Move to Scene::OnScenePlay
					if (!nsc.Instance)
					{
						nsc.Instance = nsc.InstantiateScript();
						nsc.Instance->m_Entity = Entity{ entity, this };
						nsc.Instance->OnCreate();
					}

					nsc.Instance->OnUpdate(ts);
				});
		}

		// Physics
		{
			const int32_t velocityIterations = 6;
			const int32_t positionIterations = 2;
			m_PhysicsWorld->Step(ts, velocityIterations, positionIterations);

			// Retrieve transform from Box2D
			auto view = m_Registry.view<Rigidbody2DComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				auto& transform = entity.GetComponent<TransformComponent>();
				auto& rb2d = entity.GetComponent<Rigidbody2DComponent>();

				b2Body* body = (b2Body*)rb2d.RuntimeBody;
				const auto& position = body->GetPosition();
				transform.Translation.x = position.x;
				transform.Translation.y = position.y;
				transform.Rotation.z = body->GetAngle();
			}
		}

		// Audio
		{
			// Listeners
			{
				// TODO: Maybe implement multiple non-primary listeners.
				Audio::SetListenerCount(1);

				auto view = m_Registry.view<TransformComponent, AudioListenerComponent>();
				for (auto entity : view)
				{
					auto [transform, listener] = view.get<TransformComponent, AudioListenerComponent>(entity);

					if (listener.Primary)
					{
						Audio::SetListenerPosition(transform.Translation.x, transform.Translation.y, transform.Translation.z);
						//Audio::SetListenerRotation(transform.Rotation.x, transform.Rotation.y, transform.Rotation.z);

						break;
					}
				}
			}

			// Sources
			{
				auto view = m_Registry.view<TransformComponent, AudioSourceComponent>();
				for (auto entity : view)
				{
					auto [transform, source] = view.get<TransformComponent, AudioSourceComponent>(entity);

					source.m_Source.SetPosition(transform.Translation.x, transform.Translation.y, transform.Translation.z);
					source.m_Source.SetGain(source.Gain);
					source.m_Source.SetPitch(source.Pitch);
					source.m_Source.SetSpatial(source.Spatial);
					source.m_Source.SetLoop(source.Loop);
				}
			}
		}

		// Rendering
		{
			Camera* mainCamera = nullptr;
			glm::mat4 cameraTransform;
			{
				auto view = m_Registry.view<TransformComponent, CameraComponent>();
				for (auto entity : view)
				{
					auto [transform, camera] = view.get<TransformComponent, CameraComponent>(entity);

					if (camera.Primary)
					{
						mainCamera = &camera.Camera;
						cameraTransform = transform.GetTransform();

						break;
					}
				}
			}

			if (mainCamera)
			{
				Renderer2D::BeginScene(*mainCamera, cameraTransform);
				Renderer3D::BeginScene(*mainCamera, cameraTransform);

				// Draw sprites
				{
					auto group = m_Registry.group<TransformComponent>(entt::get<SpriteRendererComponent>);
					for (auto entity : group)
					{
						auto [transform, spriteRenderer] = group.get<TransformComponent, SpriteRendererComponent>(entity);


						Renderer2D::DrawSprite(transform.GetTransform(), spriteRenderer, (int)entity);
					}
				}

				// Draw circles
				{
					auto view = m_Registry.view<TransformComponent, CircleRendererComponent>();
					for (auto entity : view)
					{
						auto [transform, circleRenderer] = view.get<TransformComponent, CircleRendererComponent>(entity);

						Renderer2D::DrawCircle(transform.GetTransform(), circleRenderer.Color, circleRenderer.Thickness, circleRenderer.Fade, (int)entity);
					}
				}

				// TODO: Implement lights here
				// Draw meshes
				{
					auto view = m_Registry.view<TransformComponent, MeshRendererComponent>();
					for (auto entity : view)
					{
						auto [transform, meshRenderer] = view.get<TransformComponent, MeshRendererComponent>(entity);

						Renderer3D::DrawMesh(transform.GetTransform(), meshRenderer, (int)entity);
					}
				}

				Renderer2D::EndScene();
				Renderer3D::EndScene();
			}
		}
	}

	void Scene::OnUpdateSimulation(Timestep ts, EditorCamera& camera)
	{
		// Physics
		{
			const int32_t velocityIterations = 6;
			const int32_t positionIterations = 2;
			m_PhysicsWorld->Step(ts, velocityIterations, positionIterations);

			// Retrieve transform from Box2D
			auto view = m_Registry.view<Rigidbody2DComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				auto& transform = entity.GetComponent<TransformComponent>();
				auto& rb2d = entity.GetComponent<Rigidbody2DComponent>();

				b2Body* body = (b2Body*)rb2d.RuntimeBody;
				const auto& position = body->GetPosition();
				transform.Translation.x = position.x;
				transform.Translation.y = position.y;
				transform.Rotation.z = body->GetAngle();
			}
		}

		// Render
		RenderScene(camera);
	}

	void Scene::OnUpdateEditor(Timestep ts, EditorCamera& camera)
	{
		// Render
		RenderScene(camera);
	}

	void Scene::OnPhysics2DStart()
	{
		m_PhysicsWorld = new b2World({ 0.0f, -9.8f });

		// Physics
		{
			auto view = m_Registry.view<Rigidbody2DComponent>();
			for (auto e : view)
			{
				Entity entity = { e, this };
				auto& transform = entity.GetComponent<TransformComponent>();
				auto& rb2d = entity.GetComponent<Rigidbody2DComponent>();

				b2BodyDef bodyDef;
				bodyDef.type = Rigidbody2DTypeToBox2DBody(rb2d.Type);
				bodyDef.position.Set(transform.Translation.x, transform.Translation.y);
				bodyDef.angle = transform.Rotation.z;

				b2Body* body = m_PhysicsWorld->CreateBody(&bodyDef);
				body->SetFixedRotation(rb2d.FixedRotation);
				rb2d.RuntimeBody = body;

				if (entity.HasComponent<BoxCollider2DComponent>())
				{
					auto& bc2d = entity.GetComponent<BoxCollider2DComponent>();

					b2PolygonShape boxShape;
					boxShape.SetAsBox(bc2d.Size.x * transform.Scale.x, bc2d.Size.y * transform.Scale.y, b2Vec2(bc2d.Offset.x, bc2d.Offset.y), bc2d.Rotation);

					b2FixtureDef fixtureDef;
					fixtureDef.shape = &boxShape;
					fixtureDef.density = bc2d.Density;
					fixtureDef.friction = bc2d.Friction;
					fixtureDef.restitution = bc2d.Restitution;
					fixtureDef.restitutionThreshold = bc2d.RestitutionThreshold;
					body->CreateFixture(&fixtureDef);
				}

				if (entity.HasComponent<CircleCollider2DComponent>())
				{
					auto& cc2d = entity.GetComponent<CircleCollider2DComponent>();

					b2CircleShape circleShape;
					circleShape.m_p.Set(cc2d.Offset.x, cc2d.Offset.y);
					circleShape.m_radius = transform.Scale.x * cc2d.Radius;

					b2FixtureDef fixtureDef;
					fixtureDef.shape = &circleShape;
					fixtureDef.density = cc2d.Density;
					fixtureDef.friction = cc2d.Friction;
					fixtureDef.restitution = cc2d.Restitution;
					fixtureDef.restitutionThreshold = cc2d.RestitutionThreshold;
					body->CreateFixture(&fixtureDef);
				}
			}
		}
	}

	void Scene::OnPhysics2DStop()
	{
		delete m_PhysicsWorld;
		m_PhysicsWorld = nullptr;
	}

	void Scene::RenderScene(EditorCamera& camera)
	{
		Renderer2D::BeginScene(camera);
		Renderer3D::BeginScene(camera);

		// Draw sprites
		{
			auto group = m_Registry.group<TransformComponent>(entt::get<SpriteRendererComponent>);
			for (auto entity : group)
			{
				auto [transform, spriteRenderer] = group.get<TransformComponent, SpriteRendererComponent>(entity);

				Renderer2D::DrawSprite(transform.GetTransform(), spriteRenderer, (int)entity);
			}
		}

		// Draw circles
		{
			auto view = m_Registry.view<TransformComponent, CircleRendererComponent>();
			for (auto entity : view)
			{
				auto [transform, circleRenderer] = view.get<TransformComponent, CircleRendererComponent>(entity);

				Renderer2D::DrawCircle(transform.GetTransform(), circleRenderer.Color, circleRenderer.Thickness, circleRenderer.Fade, (int)entity);
			}
		}

		// TODO: Implement lights here
		// Draw meshes
		{
			auto view = m_Registry.view<TransformComponent, MeshRendererComponent>();
			for (auto entity : view)
			{
				auto [transform, meshRenderer] = view.get<TransformComponent, MeshRendererComponent>(entity);

				Renderer3D::DrawMesh(transform.GetTransform(), meshRenderer, (int)entity);
			}
		}

		Renderer2D::EndScene();
		Renderer3D::EndScene();
	}

	void Scene::OnViewportResize(uint32_t width, uint32_t height)
	{
		m_ViewportWidth = width;
		m_ViewportHeight = height;

		// Resize our non-FixedAspectRatio cameras
		auto view = m_Registry.view<CameraComponent>();
		for (auto entity : view)
		{
			auto& cameraComponent = view.get<CameraComponent>(entity);
			if (!cameraComponent.FixedAspectRatio)
				cameraComponent.Camera.SetViewportSize(width, height);
		}

	}

	Entity Scene::GetPrimaryCameraEntity()
	{
		auto view = m_Registry.view<CameraComponent>();
		for (auto entity : view)
		{
			const auto& camera = view.get<CameraComponent>(entity);
			if (camera.Primary)
				return Entity{ entity, this };
		}
		return {};
	}

	Entity Scene::GetPrimaryAudioListenerEntity()
	{
		auto view = m_Registry.view<AudioListenerComponent>();
		for (auto entity : view)
		{
			const auto& listener = view.get<AudioListenerComponent>(entity);
			if (listener.Primary)
				return Entity{ entity, this };
		}
		return {};
	}

	Entity Scene::DuplicateEntity(Entity entity)
	{
		Entity newEntity = CreateEntity(entity.GetName(), entity.GetTag());
		CopyAllExistingComponents(newEntity, entity);
		return newEntity;
	}

	Entity Scene::GetEntityByUUID(UUID uuid)
	{
		// TODO: Maybe should be assert
		if (m_EntityMap.find(uuid) != m_EntityMap.end())
			return { m_EntityMap.at(uuid), this };

		return {};
	}

	std::vector<Entity> Scene::GetEntities()
	{
		std::vector<Entity> entities;
		auto view = m_Registry.view<IDComponent>();
		for (auto entity : view)
		{
			entities.push_back(Entity{ entity, this });
		}
		return entities;
	}

	void Scene::UpdateRelationships()
	{
		for (auto entity : GetEntities())
		{
			entity.UpdateRelationships();
		}
	}

	Entity Scene::GetEntityByID(UUID id)
	{
		std::vector<Entity> entity;
		auto view = m_Registry.view<IDComponent>();
		for (auto e : view)
		{
			if (view.get<IDComponent>(e).AssetID == id)
				return Entity{ e, this };
		}
		return {};
	}

	std::vector<Entity> Scene::GetEntitiesByAssetID(UUID id)
	{
		std::vector<Entity> entities;
		auto view = m_Registry.view<IDComponent>();
		for (auto entity : view)
		{
			if (view.get<IDComponent>(entity).AssetID == id)
				entities.push_back(Entity{ entity, this });
		}
		return entities;
	}

	template<typename T>
	void Scene::OnComponentAdded(Entity entity, T& component)
	{
#ifdef _MSC_VER
		static_assert(sizeof(T) == 0);
#else
		SPIRIT_CORE_ASSERT(sizeof(T) == 0);
#endif
	}

	template<typename T>
	void Scene::OnComponentModified(Entity entity, T& component)
	{
#ifdef _MSC_VER
		static_assert(sizeof(T) == 0);
#else
		SPIRIT_CORE_ASSERT(sizeof(T) == 0);
#endif
	}

	template<typename T>
	void Scene::OnComponentRemoved(Entity entity, T& component)
	{
#ifdef _MSC_VER
		static_assert(sizeof(T) == 0);
#else
		SPIRIT_CORE_ASSERT(sizeof(T) == 0);
#endif
	}

	template<>
	void Scene::OnComponentAdded<IDComponent>(Entity entity, IDComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<IDComponent>(Entity entity, IDComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<IDComponent>(Entity entity, IDComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<TransformComponent>(Entity entity, TransformComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<TransformComponent>(Entity entity, TransformComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<TransformComponent>(Entity entity, TransformComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<CameraComponent>(Entity entity, CameraComponent& component)
	{
		if (m_ViewportWidth > 0 && m_ViewportHeight > 0)
			component.Camera.SetViewportSize(m_ViewportWidth, m_ViewportHeight);
	}

	template<>
	void Scene::OnComponentModified<CameraComponent>(Entity entity, CameraComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<CameraComponent>(Entity entity, CameraComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<ScriptWrapperComponent>(Entity entity, ScriptWrapperComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<ScriptWrapperComponent>(Entity entity, ScriptWrapperComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<ScriptWrapperComponent>(Entity entity, ScriptWrapperComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<ScriptComponent>(Entity entity, ScriptComponent& component)
	{
		if (m_IsRuntimeStarted)
		{
			ScriptEngine::OnInit(entity, component);
			ScriptEngine::OnCreate(entity, component);
		}
	}

	template<>
	void Scene::OnComponentModified<ScriptComponent>(Entity entity, ScriptComponent& component)
	{
		if (m_IsRuntimeStarted)
			ScriptEngine::OnModify(entity, component);
	}

	template<>
	void Scene::OnComponentRemoved<ScriptComponent>(Entity entity, ScriptComponent& component)
	{
		if (m_IsRuntimeStarted)
			ScriptEngine::OnRemove(entity, component);
	}

	template<>
	void Scene::OnComponentAdded<SpriteRendererComponent>(Entity entity, SpriteRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<SpriteRendererComponent>(Entity entity, SpriteRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<SpriteRendererComponent>(Entity entity, SpriteRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<CircleRendererComponent>(Entity entity, CircleRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<CircleRendererComponent>(Entity entity, CircleRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<CircleRendererComponent>(Entity entity, CircleRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<MeshRendererComponent>(Entity entity, MeshRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<MeshRendererComponent>(Entity entity, MeshRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<MeshRendererComponent>(Entity entity, MeshRendererComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<DirectionalLightComponent>(Entity entity, DirectionalLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<DirectionalLightComponent>(Entity entity, DirectionalLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<DirectionalLightComponent>(Entity entity, DirectionalLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<PointLightComponent>(Entity entity, PointLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<PointLightComponent>(Entity entity, PointLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<PointLightComponent>(Entity entity, PointLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<SpotLightComponent>(Entity entity, SpotLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<SpotLightComponent>(Entity entity, SpotLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<SpotLightComponent>(Entity entity, SpotLightComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<TagComponent>(Entity entity, TagComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<TagComponent>(Entity entity, TagComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<TagComponent>(Entity entity, TagComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<Rigidbody2DComponent>(Entity entity, Rigidbody2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<Rigidbody2DComponent>(Entity entity, Rigidbody2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<Rigidbody2DComponent>(Entity entity, Rigidbody2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<BoxCollider2DComponent>(Entity entity, BoxCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<BoxCollider2DComponent>(Entity entity, BoxCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<BoxCollider2DComponent>(Entity entity, BoxCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<CircleCollider2DComponent>(Entity entity, CircleCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<CircleCollider2DComponent>(Entity entity, CircleCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<CircleCollider2DComponent>(Entity entity, CircleCollider2DComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<AudioListenerComponent>(Entity entity, AudioListenerComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<AudioListenerComponent>(Entity entity, AudioListenerComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<AudioListenerComponent>(Entity entity, AudioListenerComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<AudioSourceComponent>(Entity entity, AudioSourceComponent& component)
	{
		if (component.PlayImmediately)
			component.Play();
	}

	template<>
	void Scene::OnComponentModified<AudioSourceComponent>(Entity entity, AudioSourceComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<AudioSourceComponent>(Entity entity, AudioSourceComponent& component)
	{
	}

	template<>
	void Scene::OnComponentAdded<NativeScriptComponent>(Entity entity, NativeScriptComponent& component)
	{
	}

	template<>
	void Scene::OnComponentModified<NativeScriptComponent>(Entity entity, NativeScriptComponent& component)
	{
	}

	template<>
	void Scene::OnComponentRemoved<NativeScriptComponent>(Entity entity, NativeScriptComponent& component)
	{
	}

}
