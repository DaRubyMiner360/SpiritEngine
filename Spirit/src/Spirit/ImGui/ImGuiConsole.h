#pragma once

#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"

#include <string>
#include <vector>

namespace Spirit {

	class Message {
	public:
		enum class Level : int8_t {
			Info = 0,
			Warn = 1,
			Error = 2,
			Critical = 3
		};
	public:
		Message() = default;
		Message(const Message&) = default;
		Message(const std::string& message, Level level = Level::Info)
			: m_MessageData(message), m_MessageLevel(level) {}

		~Message() = default;

	public:
		std::string m_MessageData;
		Level m_MessageLevel;
	};

	class ImGuiConsole
	{
	private:
		bool m_AutoScroll = true;
		bool m_ScrollToBottom = false;

		std::vector<Message> m_MessageBuffer;
	public:
		ImGuiConsole() = default;

		void OnImGuiRender();

		template <typename... Args>
		void Info(const std::string& data, Args&&... args);
		template <typename... Args>
		void Warning(const std::string& data, Args&&... args);
		template <typename... Args>
		void Error(const std::string& data, Args&&... args);
		template <typename... Args>
		void Critical(const std::string& data, Args&&... args);

	private:
		void Clear();

		template <typename... Args>
		static std::string Format(const std::string& fmt, Args&&... args);
	};

	template<typename ...Args>
	inline std::string ImGuiConsole::Format(const std::string& fmt, Args && ...args)
	{
		size_t size = snprintf(nullptr, 0, fmt.c_str(), args...);
		std::string buffer;
		buffer.reserve(size + 1);
		buffer.resize(size);
		snprintf(&buffer[0], size + 1, fmt.c_str(), args...);
		return buffer;
	}

	// Logging Implementations
	template<typename ...Args>
	inline void ImGuiConsole::Info(const std::string& data, Args && ...args)
	{
		m_MessageBuffer.push_back({ ImGuiConsole::Format(data, args...) });
	}

	template<typename ...Args>
	inline void ImGuiConsole::Warning(const std::string& data, Args && ...args)
	{
		m_MessageBuffer.push_back({ ImGuiConsole::Format(data, args...), Message::Level::Warn });
	}

	template<typename ...Args>
	inline void ImGuiConsole::Error(const std::string& data, Args && ...args)
	{
		m_MessageBuffer.push_back({ ImGuiConsole::Format(data, args...), Message::Level::Error });
	}
	
	template<typename ...Args>
	inline void ImGuiConsole::Critical(const std::string& data, Args && ...args)
	{
		m_MessageBuffer.push_back({ ImGuiConsole::Format(data, args...), Message::Level::Critical });
	}

}
