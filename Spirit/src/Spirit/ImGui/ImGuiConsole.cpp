#include "spiritpch.h"
#include "ImGuiConsole.h"

#include <imgui.h>
#include <imgui_internal.h>

#include "ImGuiLogSink.h"
#include "Spirit/Core/Log.h"

namespace Spirit {

	bool s_ShowInfoMessages = true;
	bool s_ShowWarnMessages = true;
	bool s_ShowErrorMessages = true;
	bool s_ShowCriticalMessages = true;

	ImVec4 s_InfoColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	ImVec4 s_WarningColor = { 1.0f, 1.0f, 0.0f, 1.0f };
	ImVec4 s_ErrorColor = { 1.0f, 0.0f, 0.0f, 1.0f };
	ImVec4 s_CriticalColor = { 0.545f, 0.0f, 0.0f, 1.0f };

	void ImGuiConsole::OnImGuiRender()
	{
		if (((ImGuiLogSink_mt*)Log::GetClientLogger()->sinks()[2].get())->console != this)
			((ImGuiLogSink_mt*)Log::GetClientLogger()->sinks()[2].get())->console = this;

		ImGui::Begin("Console");

		if (ImGui::Button("Clear", { 50.0f, 30.0f }))
			Clear();

		ImGui::SameLine();
		//ImGui::PushStyleColor(ImGuiCol_Button, s_InfoColor);
		if (ImGui::Button("Info", { 50.0f, 30.0f }))
			s_ShowInfoMessages = !s_ShowInfoMessages;
		//ImGui::PopStyleColor(1);

		ImGui::SameLine();
		//ImGui::PushStyleColor(ImGuiCol_Button, s_WarningColor);
		if (ImGui::Button("Warn", { 50.0f, 30.0f }))
			s_ShowWarnMessages = !s_ShowWarnMessages;
		//ImGui::PopStyleColor(1);

		ImGui::SameLine();
		//ImGui::PushStyleColor(ImGuiCol_Button, s_ErrorColor);
		if (ImGui::Button("Error", { 50.0f, 30.0f }))
			s_ShowErrorMessages = !s_ShowErrorMessages;
		//ImGui::PopStyleColor(1);

		ImGui::SameLine();
		//ImGui::PushStyleColor(ImGuiCol_Button, s_CriticalColor);
		if (ImGui::Button("Critical", { 50.0f, 30.0f }))
			s_ShowCriticalMessages = !s_ShowCriticalMessages;
		//ImGui::PopStyleColor(1);

		if (ImGui::BeginPopup("Options"))
		{
			ImGui::Checkbox("Auto-scroll", &m_AutoScroll);
			ImGui::EndPopup();
		}

		ImGui::SameLine();
		if (ImGui::Button("Options"))
			ImGui::OpenPopup("Options");

		ImGui::Separator();

		const float reservedFooterHeight = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing();
		ImGui::BeginChild("Scrolling Region", ImVec2(0, -reservedFooterHeight), false, ImGuiWindowFlags_HorizontalScrollbar);

		for (uint32_t i = 0; i < m_MessageBuffer.size(); i++)
		{
			if (m_MessageBuffer.at(i).m_MessageLevel == Message::Level::Info && s_ShowInfoMessages)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, s_InfoColor);
				ImGui::Selectable(m_MessageBuffer.at(i).m_MessageData.c_str());
				ImGui::PopStyleColor();

				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::MenuItem("Copy"))
					{
						ImGui::LogToClipboard();
						ImGui::LogText((std::string("[INFO] ") + m_MessageBuffer.at(i).m_MessageData).c_str());
						ImGui::LogFinish();
					}

					ImGui::EndPopup();
				}

				ImGui::Separator();

				continue;
			}
			if (m_MessageBuffer.at(i).m_MessageLevel == Message::Level::Warn && s_ShowWarnMessages)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, s_WarningColor);
				ImGui::Selectable(m_MessageBuffer.at(i).m_MessageData.c_str());
				ImGui::PopStyleColor();

				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::MenuItem("Copy"))
					{
						ImGui::LogToClipboard();
						ImGui::LogText((std::string("[WARN] ") + m_MessageBuffer.at(i).m_MessageData).c_str());
						ImGui::LogFinish();
					}

					ImGui::EndPopup();
				}

				ImGui::Separator();

				continue;
			}
			if (m_MessageBuffer.at(i).m_MessageLevel == Message::Level::Error && s_ShowErrorMessages)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, s_ErrorColor);
				ImGui::Selectable(m_MessageBuffer.at(i).m_MessageData.c_str());
				ImGui::PopStyleColor();

				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::MenuItem("Copy"))
					{
						ImGui::LogToClipboard();
						ImGui::LogText((std::string("[ERROR] ") + m_MessageBuffer.at(i).m_MessageData).c_str());
						ImGui::LogFinish();
					}

					ImGui::EndPopup();
				}

				ImGui::Separator();

				continue;
			}
			if (m_MessageBuffer.at(i).m_MessageLevel == Message::Level::Critical && s_ShowCriticalMessages)
			{
				ImGui::PushStyleColor(ImGuiCol_Text, s_CriticalColor);
				ImGui::Selectable(m_MessageBuffer.at(i).m_MessageData.c_str());
				ImGui::PopStyleColor();

				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::MenuItem("Copy"))
					{
						ImGui::LogToClipboard();
						ImGui::LogText((std::string("[CRITICAL] ") + m_MessageBuffer.at(i).m_MessageData).c_str());
						ImGui::LogFinish();
					}

					ImGui::EndPopup();
				}

				ImGui::Separator();

				continue;
			}
		}

		if (m_ScrollToBottom || (m_AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
			ImGui::SetScrollHereY(1.0f);
		m_ScrollToBottom = false;

		ImGui::EndChild();

		ImGui::End();
	}

	void ImGuiConsole::Clear()
	{
		m_MessageBuffer.clear();
	}
}
