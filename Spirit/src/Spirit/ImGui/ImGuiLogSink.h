#pragma once

#include "Spirit/ImGui/ImGuiConsole.h"

#include <shared_mutex>
#include <mutex>
#include <string>
#include <vector>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/sink.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/formatter.h>
#include <spdlog/details/null_mutex.h>

#include <imgui.h>

namespace Spirit {
	template<typename Mutex>
	class ImGuiLogSink : public spdlog::sinks::base_sink<Mutex>
	{
	public:
		ImGuiConsole* console = nullptr;

		explicit ImGuiLogSink() {}
	protected:
		void sink_it_(const spdlog::details::log_msg& msg) override
		{
			if (console)
			{
				spdlog::memory_buf_t formatted;
				spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);
				
				if (msg.level == spdlog::level::level_enum::info)
					console->Info(fmt::to_string(formatted));
				else if (msg.level == spdlog::level::level_enum::warn)
					console->Warning(fmt::to_string(formatted));
				else if (msg.level == spdlog::level::level_enum::err)
					console->Error(fmt::to_string(formatted));
				else if (msg.level == spdlog::level::level_enum::critical)
					console->Critical(fmt::to_string(formatted));
			}
		}

		void flush_() override {}
	};

	using ImGuiLogSink_mt = ImGuiLogSink<std::mutex>;
	using ImGuiLogSink_st = ImGuiLogSink<spdlog::details::null_mutex>;
}
