#pragma once

#include "StringUtils.h"

#include <filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>

#include "nfd.hpp"

namespace Spirit {

	class FileTypes
	{
	public:
		// Spirit types
		static std::vector<std::string> GetSceneTypes() { return { "spirit", "sscene" }; }
		static std::vector<std::string> GetPrefabTypes() { return { "prefab", "sprefab" }; }
		static std::vector<std::string> GetProjectTypes() { return { "sproj" }; }

		static bool IsSceneType(const std::string extension)
		{
			for (auto& type : GetSceneTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsPrefabType(const std::string extension)
		{
			for (auto& type : GetPrefabTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsProjectType(const std::string extension)
		{
			for (auto& type : GetProjectTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}

		static nfdfilteritem_t GetSceneFilter() { return { "Spirit Scene", "spirit,sscene" }; }
		static nfdfilteritem_t GetPrefabFilter() { return { "Prefab", "prefab,sprefab" }; }
		static nfdfilteritem_t GetProjectFilter() { return { "Spirit Project", "sproj" }; }


		// Normal types
		static std::vector<std::string> GetCodeTypes() { return { "h", "c", "hpp", "cpp", "cs", "lua" }; }
		static std::vector<std::string> GetShaderTypes() { return { "glsl", "vert", "frag", "geom" }; }
		static std::vector<std::string> GetFontTypes() { return { "tff" }; }
		static std::vector<std::string> GetDataTypes() { return { "json", "xml", "yml", "yaml" }; }
		static std::vector<std::string> GetImageTypes() { return { "png" }; }
		static std::vector<std::string> GetModelTypes() { return { "3d", "3ds", "3mf", "ac", "ac3d", "acc", "amj", "ase", "ask", "b3d", "blend", "bvh", "cms", "cob", "dae", "dxf", "enff", "fbx", "gltf", "glf", "hmb", "ifc", "step", "irr", "irrmesh", "lwo", "lws", "lxo", "m3d", "md2", "md3", "md5", "mdc", "mdl", "mesh", "mot", "ms3d", "ndo", "nff", "obj", "off", "ogex", "ply", "pmx", "prj", "q3o", "q3s", "raw", "scn", "sib", "smd", "stl", "ter", "uc", "vta", "x", "x3d", "xgl", "zgl" }; }
		static std::vector<std::string> GetAudioTypes() { return { "ogg", "mp3" }; }
		// Specifics
		static std::vector<std::string> GetAllCTypes() { return { "h", "c", "hpp", "cpp" }; }
		static std::vector<std::string> GetCTypes() { return { "h", "c" }; }
		static std::vector<std::string> GetCPPTypes() { return { "h", "hpp", "cpp" }; }
		static std::vector<std::string> GetCSTypes() { return { "cs" }; }
		static std::vector<std::string> GetLuaTypes() { return { "lua" }; }

		static bool IsCodeType(const std::string extension)
		{
			for (auto& type : GetCodeTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsShaderType(const std::string extension)
		{
			for (auto& type : GetShaderTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsFontType(const std::string extension)
		{
			for (auto& type : GetFontTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsDataType(const std::string extension)
		{
			for (auto& type : GetDataTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsImageType(const std::string extension)
		{
			for (auto& type : GetImageTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsModelType(const std::string extension)
		{
			for (auto& type : GetModelTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsAudioType(const std::string extension)
		{
			for (auto& type : GetAudioTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		// Specifics
		static bool IsAnyCType(const std::string extension)
		{
			for (auto& type : GetAllCTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsCType(const std::string extension)
		{
			for (auto& type : GetCTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsCPPType(const std::string extension)
		{
			for (auto& type : GetCPPTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsCSType(const std::string extension)
		{
			for (auto& type : GetCSTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
		static bool IsLuaType(const std::string extension)
		{
			for (auto& type : GetLuaTypes())
			{
				if (IsType(extension, type))
					return true;
			}
			return false;
		}
	private:
		static bool IsType(const std::string extension, const std::string type)
		{
			if (extension.at(0) == '.')
			{
				if (extension == "." + type)
					return true;
			}
			else if (type.at(0) == '.')
			{
				if ("." + extension == type)
					return true;
			}
			else
			{
				if (extension == type)
					return true;
			}
			return false;
		}
	};

	class FileUtils
	{
	public:
		static std::string ReadFile(std::string filePath)
		{
			std::ifstream fileStream(filePath.c_str());

			if (fileStream.is_open())
			{
				std::string result((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());
				fileStream.close();
				return result;
			}

			SPIRIT_ERROR("Cannot open file: {0}", filePath);
			return "";
		}

		static void WriteFile(std::string path, std::string fileName, std::string fileContent)
		{
			std::string filePathName(path + PathSeparator() + fileName);
			std::ofstream newFile(filePathName, std::ios_base::trunc);
			newFile << fileContent << "\n";
			newFile.close();
		}

		static bool CreateFolder(std::string path, std::string folderName)
		{
			std::string newDir(path + PathSeparator() + folderName);

			if (!std::filesystem::exists(newDir) || !std::filesystem::is_directory(newDir))
			{
				std::filesystem::create_directory(newDir);
				return true;
			}

			return false;
		}

		static void CopyFile(std::string& origin, std::string& target)
		{
			std::filesystem::copy(origin, target, std::filesystem::copy_options::overwrite_existing);
		}

		static std::string GetFileNameFromPath(std::string& filePath)
		{
			return std::filesystem::path(filePath).filename().string();
		}

		static bool IsFile(std::string& filePath)
		{
			return std::filesystem::is_regular_file(filePath);
		}

		static std::string PathSeparator()
		{
#ifdef _WIN32
			return "\\";
#else
			return "/";
#endif
		}

	private:
		bool FileExists(std::string& path)
		{
			return std::filesystem::exists(path);
		}
	};

}
