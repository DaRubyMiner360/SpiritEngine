#pragma once

#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"

namespace Spirit {

	class ScriptWrapperUtils
	{
	public:
		static ScriptComponent& AddComponent(Entity entity, ScriptComponent& script)
		{
			entity.GetComponent<ScriptWrapperComponent>().Scripts.push_back(script);
			entity.GetScene().OnComponentAdded<ScriptComponent>(entity, script);
			return script;
		}

		static void RemoveComponent(Entity entity, ScriptComponent& script)
		{
			if (HasComponent(entity, script))
			{
				entity.GetScene().OnComponentRemoved<ScriptComponent>(entity, script);

				auto& scripts = entity.GetComponent<ScriptWrapperComponent>().Scripts;
				scripts.erase(std::remove(scripts.begin(), scripts.end(), script), scripts.end());
			}
		}

		static bool HasComponent(Entity entity, ScriptComponent& script)
		{
			auto& scripts = entity.GetComponent<ScriptWrapperComponent>().Scripts;
			return std::find(scripts.begin(), scripts.end(), script) != scripts.end();
		}

		static int GetComponentIndex(Entity entity, ScriptComponent& script)
		{
			if (HasComponent(entity, script))
			{
				auto& scripts = entity.GetComponent<ScriptWrapperComponent>().Scripts;
				return std::distance(scripts.begin(), std::find(scripts.begin(), scripts.end(), script));
			}
			return -1;
		}
	};
}
