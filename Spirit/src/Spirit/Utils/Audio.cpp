#include "spiritpch.h"
#include "Audio.h"

#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

#include <stdio.h>
#include <stdlib.h>

#include <thread>
#include <filesystem>

namespace Spirit {

	static ma_engine s_Engine;

	//static uint8_t* s_AudioScratchBuffer;
	//static uint32_t s_AudioScratchBufferSize = 10 * 1024 * 1024; // 10mb initially

	static bool s_DebugLog = true;

	// Currently supported file formats
	/*enum class AudioFileFormat
	{
		None = 0,
		Ogg,
		MP3
	};

	static AudioFileFormat GetFileFormat(const std::string& filename)
	{
		std::filesystem::path path = filename;
		std::string extension = path.extension().string();

		if (extension == ".ogg")  return AudioFileFormat::Ogg;
		if (extension == ".mp3")  return AudioFileFormat::MP3;

		return AudioFileFormat::None;
	}

	static ALenum GetOpenALFormat(uint32_t channels)
	{
		// Note: sample size is always 2 bytes (16-bits) with
		// both the .mp3 and .ogg decoders that we're using
		switch (channels)
		{
		case 1:  return AL_FORMAT_MONO16;
		case 2:  return AL_FORMAT_STEREO16;
		}
		// assert
		return 0;
	}

	AudioSource Audio::LoadAudioSourceOgg(const std::string& filename)
	{
		FILE* f = fopen(filename.c_str(), "rb");

		OggVorbis_File vf;
		if (ov_open_callbacks(f, &vf, NULL, 0, OV_CALLBACKS_NOCLOSE) < 0)
			SPIRIT_WARN("Could not open ogg stream");

		// Useful info
		vorbis_info* vi = ov_info(&vf, -1);
		auto sampleRate = vi->rate;
		auto channels = vi->channels;
		auto alFormat = GetOpenALFormat(channels);

		uint64_t samples = ov_pcm_total(&vf, -1);
		float trackLength = (float)samples / (float)sampleRate; // in seconds
		uint32_t bufferSize = 2 * channels * samples; // 2 bytes per sample (I'm guessing...)

		if (s_DebugLog)
		{
			SPIRIT_INFO("File Info - " + filename + ":");
			SPIRIT_INFO("  Channels: " + channels);
			SPIRIT_INFO("  Sample Rate: " + sampleRate);
			SPIRIT_INFO("  Expected size: " + bufferSize);
		}

		// TODO: Replace with Spirit::Buffer
		if (s_AudioScratchBufferSize < bufferSize)
		{
			s_AudioScratchBufferSize = bufferSize;
			delete[] s_AudioScratchBuffer;
			s_AudioScratchBuffer = new uint8_t[s_AudioScratchBufferSize];
		}

		uint8_t* oggBuffer = s_AudioScratchBuffer;
		uint8_t* bufferPtr = oggBuffer;
		int eof = 0;
		while (!eof)
		{
			int current_section;
			long length = ov_read(&vf, (char*)bufferPtr, 4096, 0, 2, 1, &current_section);
			bufferPtr += length;
			if (length == 0)
			{
				eof = 1;
			}
			else if (length < 0)
			{
				if (length == OV_EBADLINK)
				{
					SPIRIT_WARN("Corrupt bitstream section!");
					return { 0, false, 0 };
				}
			}
		}

		uint32_t size = bufferPtr - oggBuffer;
		// assert bufferSize == size

		if (s_DebugLog)
			SPIRIT_INFO("  Read " + std::to_string(size) + " bytes");

		// Release file
		ov_clear(&vf);
		fclose(f);

		ALuint buffer;
		alGenBuffers(1, &buffer);
		alBufferData(buffer, alFormat, oggBuffer, size, sampleRate);

		AudioSource result = { buffer, true, trackLength };
		alGenSources(1, &result.m_SourceHandle);
		alSourcei(result.m_SourceHandle, AL_BUFFER, buffer);

		if (alGetError() != AL_NO_ERROR)
			SPIRIT_INFO("Failed to setup sound source");

		return result;
	}

	AudioSource Audio::LoadAudioSourceMP3(const std::string& filename)
	{
		mp3dec_file_info_t info;
		int loadResult = mp3dec_load(&s_Mp3d, filename.c_str(), &info, NULL, NULL);
		uint32_t size = info.samples * sizeof(mp3d_sample_t);

		auto sampleRate = info.hz;
		auto channels = info.channels;
		auto alFormat = GetOpenALFormat(channels);
		float lengthSeconds = size / (info.avg_bitrate_kbps * 1024.0f);

		ALuint buffer;
		alGenBuffers(1, &buffer);
		alBufferData(buffer, alFormat, info.buffer, size, sampleRate);

		AudioSource result = { buffer, true, lengthSeconds };
		alGenSources(1, &result.m_SourceHandle);
		alSourcei(result.m_SourceHandle, AL_BUFFER, buffer);

		if (s_DebugLog)
		{
			SPIRIT_INFO("File Info - " + filename + ":");
			SPIRIT_INFO("  Channels: " + channels);
			SPIRIT_INFO("  Sample Rate: " + sampleRate);
			SPIRIT_INFO("  Size: " + std::to_string(size) + " bytes");

			auto [mins, secs] = result.GetLengthMinutesAndSeconds();
			SPIRIT_INFO("  Length: " + std::to_string(mins) + "m" + std::to_string(secs) + "s");
		}

		if (alGetError() != AL_NO_ERROR)
			SPIRIT_WARN("Failed to setup sound source");

		return result;
	}*/

	static void PrintAudioDeviceInfo()
	{
		if (s_DebugLog)
		{
			// SPIRIT_INFO("Audio Device Info:");
			// SPIRIT_INFO("  Name: " + s_AudioDevice->DeviceName);
			// SPIRIT_INFO("  Sample Rate: " + s_AudioDevice->Frequency);
			// SPIRIT_INFO("  Max Sources: " + s_AudioDevice->SourcesMax);
			// SPIRIT_INFO("    Mono: " + s_AudioDevice->NumMonoSources);
			// SPIRIT_INFO("    Stereo: " + s_AudioDevice->NumStereoSources);
		}
	}

	void Audio::Init()
	{
		ma_result result = ma_engine_init(NULL, &s_Engine);
		if (result != MA_SUCCESS) {
			SPIRIT_WARN("Failed to initialize audio engine.");
			return;
		}

		//if (InitAL(nullptr, 0) != 0)
		//	SPIRIT_WARN("Audio device error!");

		//s_AudioDevice = alcGetContextsDevice(alcGetCurrentContext());

		// PrintAudioDeviceInfo();

		//mp3dec_init(&s_Mp3d);

		//s_AudioScratchBuffer = new uint8_t[s_AudioScratchBufferSize];

		// Init listener
		//ALfloat listenerPos[] = { 0.0, 0.0, 0.0 };
		//ALfloat listenerVel[] = { 0.0, 0.0, 0.0 };
		//ALfloat listenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
		//alListenerfv(AL_POSITION, listenerPos);
		//alListenerfv(AL_VELOCITY, listenerVel);
		//alListenerfv(AL_ORIENTATION, listenerOri);

		// m_Initialized = true;
	}

	void Audio::Deinit()
	{
		ma_engine_uninit(&s_Engine);

		//CloseAL();
		// m_Initialized = false;
	}

	//AudioSource Audio::LoadAudioSource(const std::string& filename)
	//{
	//	// If this doesn't change, loading failed or unsupported file type
	//	AudioSource src = { 0, false, 0 };
	//
	//		//auto format = GetFileFormat(filename);
	//		//switch (format)
	//		//{
	//		//case AudioFileFormat::Ogg: src = LoadAudioSourceOgg(filename);
	//		//case AudioFileFormat::MP3: src = LoadAudioSourceMP3(filename);
	//		//}
	//
	//		src.m_Path = filename;
	//
	//	return src;
	//}

	void Audio::Play(AudioSource& audioSource)
	{
		// Play the sound until it finishes
		ma_sound_start(audioSource.m_Sound);
		//alSourcePlay(audioSource.m_SourceHandle);

		// TODO: current playback time and playback finished callback
		// eg.
		// ALfloat offset;
		// alGetSourcei(audioSource.m_SourceHandle, AL_SOURCE_STATE, &s_PlayState);
		// ALenum s_PlayState;
		// alGetSourcef(audioSource.m_SourceHandle, AL_SEC_OFFSET, &offset);
	}

	void Audio::SetListenerCount(int count)
	{
		s_Engine.listenerCount = count;
	}

	void Audio::SetListenerPosition(float x, float y, float z)
	{
		ma_engine_listener_set_position(&s_Engine, 0, x, y, z);
		//alListener3f(AL_POSITION, x, y, z);
	}

	void Audio::SetListenerPosition(int index, float x, float y, float z)
	{
		ma_engine_listener_set_position(&s_Engine, index, x, y, z);
		//alListener3f(AL_POSITION, x, y, z);
	}

	void Audio::SetListenerRotation(float x, float y, float z)
	{
		ma_engine_listener_set_direction(&s_Engine, 0, x, y, z);
		//alListener3f(AL_DIRECTION, x, y, z);
	}

	void Audio::SetListenerRotation(int index, float x, float y, float z)
	{
		ma_engine_listener_set_direction(&s_Engine, index, x, y, z);
		//alListener3f(AL_DIRECTION, x, y, z);
	}

	void Audio::SetListenerVelocity(float x, float y, float z)
	{
		ma_engine_listener_set_velocity(&s_Engine, 0, x, y, z);
		//alListener3f(AL_VELOCITY, x, y, z);
	}

	void Audio::SetListenerVelocity(int index, float x, float y, float z)
	{
		ma_engine_listener_set_velocity(&s_Engine, index, x, y, z);
		//alListener3f(AL_VELOCITY, x, y, z);
	}

	void Audio::SetDebugLogging(bool log)
	{
		s_DebugLog = log;
	}

	AudioSource::AudioSource(ma_sound* sound, float length)
		: m_Sound(sound), m_TotalDuration(length)
	{
	}

	//AudioSource::AudioSource(uint32_t handle, bool loaded, float length)
	//	: m_BufferHandle(handle), m_Loaded(loaded), m_TotalDuration(length)
	//{
	//}

	AudioSource::~AudioSource()
	{
		// TODO: free openal audio source?
	}

	void AudioSource::SetPosition(float x, float y, float z)
	{
		//alSource3f(source, AL_VELOCITY, 0, 0, 0);

		m_Position[0] = x;
		m_Position[1] = y;
		m_Position[2] = z;

		//alSourcefv(m_SourceHandle, AL_POSITION, m_Position);
	}

	void AudioSource::SetGain(float gain)
	{
		m_Gain = gain;

		//alSourcef(m_SourceHandle, AL_GAIN, gain);
	}

	void AudioSource::SetPitch(float pitch)
	{
		m_Pitch = pitch;

		//alSourcef(m_SourceHandle, AL_PITCH, pitch);
	}

	void AudioSource::SetSpatial(bool spatial)
	{
		m_Spatial = spatial;

		ma_sound_set_spatialization_enabled(m_Sound, spatial);
		//alSourcei(m_SourceHandle, AL_SOURCE_SPATIALIZE_SOFT, spatial ? AL_TRUE : AL_FALSE);
		//alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
	}

	void AudioSource::SetLoop(bool loop)
	{
		m_Loop = loop;

		ma_sound_set_looping(m_Sound, loop);
		//alSourcei(m_SourceHandle, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
	}

	std::pair<uint32_t, uint32_t> AudioSource::GetLengthMinutesAndSeconds() const
	{
		return { (uint32_t)(m_TotalDuration / 60.0f), (uint32_t)m_TotalDuration % 60 };
	}

	AudioSource AudioSource::LoadFromFile(const std::string& file, bool spatial)
	{
		/////ma_engine_play_sound(&s_Engine, "my_sound.wav", pGroup);
		//AudioSource src = Audio::LoadAudioSource(file);

		ma_sound sound;

		ma_result result = ma_sound_init_from_file(&s_Engine, file.c_str(), 0, NULL, NULL, &sound);
		if (result != MA_SUCCESS) {
			return {};  // Failed to load sound.
		}

		float length;
		ma_sound_get_length_in_seconds(&sound, &length);
		AudioSource src = { &sound, length };
		src.m_Path = file;

		src.SetSpatial(spatial);
		return src;
	}

}
