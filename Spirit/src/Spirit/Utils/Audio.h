#pragma once

struct ma_sound;

namespace Spirit {

	class AudioSource
	{
	public:
		AudioSource() = default;
		~AudioSource();

		bool operator==(const AudioSource& other) const { return m_Gain == other.GetGain() && m_Pitch == other.GetPitch() && m_Spatial == other.GetSpatial() && m_Loop == other.GetLoop() && m_Position == other.GetPosition(); }
		//bool operator==(const AudioSource& other) const { return m_Loaded == other.IsLoaded() && m_Gain == other.GetGain() && m_Pitch == other.GetPitch() && m_Spatial == other.GetSpatial() && m_Loop == other.GetLoop() && m_Position == other.GetPosition(); }
		bool operator!=(const AudioSource& other) const { return !(this == &other); }

		//bool IsLoaded() const { return m_Loaded; }
		float GetGain() const { return m_Gain; }
		float GetPitch() const { return m_Pitch; }
		bool GetSpatial() const { return m_Spatial; }
		bool GetLoop() const { return m_Loop; }
		const float* GetPosition() const { return m_Position; }

		std::string GetPath() const { return m_Path; }

		void SetPosition(float x, float y, float z);
		void SetGain(float gain);
		void SetPitch(float pitch);
		void SetSpatial(bool spatial);
		void SetLoop(bool loop);

		std::pair<uint32_t, uint32_t> GetLengthMinutesAndSeconds() const;

		static AudioSource LoadFromFile(const std::string& file, bool spatial = false);
	private:
		AudioSource(ma_sound* sound, float length);
		//AudioSource(uint32_t handle, bool loaded, float length);

		ma_sound* m_Sound;

		//uint32_t m_BufferHandle = 0;
		uint32_t m_SourceHandle = 0;
		//bool m_Loaded = false;
		bool m_Spatial = false;

		float m_TotalDuration = 0; // in seconds

		// Attributes
		float m_Position[3] = { 0.0f, 0.0f, 0.0f };
		float m_Gain = 1.0f;
		float m_Pitch = 1.0f;
		bool m_Loop = false;

		std::string m_Path;

		friend class Audio;
	};

	class Audio
	{
	public:
		static void Init();
		static void Deinit();
		//static bool IsInitialized() { return false; };

		//static AudioSource LoadAudioSource(const std::string& filename);
		static void Play(AudioSource& source);
		static void SetListenerCount(int count);
		static void SetListenerPosition(float x, float y, float z);
		static void SetListenerPosition(int index, float x, float y, float z);
		static void SetListenerRotation(float x, float y, float z);
		static void SetListenerRotation(int index, float x, float y, float z);
		static void SetListenerVelocity(float x, float y, float z);
		static void SetListenerVelocity(int index, float x, float y, float z);

		// TODO: temporary whilst Spirit Audio is in early dev
		static void SetDebugLogging(bool log);
	private:
		//static bool m_Initialized;

		//static AudioSource LoadAudioSourceOgg(const std::string& filename);
		//static AudioSource LoadAudioSourceMP3(const std::string& filename);
	};

}
