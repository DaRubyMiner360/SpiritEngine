#pragma once

#include <string>
#include <optional>
#include <iostream>

namespace Spirit {

	class FileDialogs
	{
	public:
		// These return empty strings if cancelled
		// TODO: Deprecate and remove
		static std::string OpenFile(const char* filter);
		// TODO: Deprecate and remove
		static std::string SaveFile(const char* filter);
	};

	class Time
	{
	public:
		static float GetTime();
	};

}
