#pragma once

#include <string>
#include <algorithm>

namespace Spirit {

	class StringUtils
	{
	public:
		static inline std::string ToLowerCase(std::string str)
		{
			std::transform(str.begin(), str.end(), str.begin(), ::tolower);
			return str;
		}

		static inline std::string ToUpperCase(std::string str)
		{
			std::transform(str.begin(), str.end(), str.begin(), ::toupper);
			return str;
		}

		static inline bool ToBoolean(std::string str)
		{
			// Convert to lower case to make string comparisons case-insensitive
			std::transform(str.begin(), str.end(), str.begin(), ::tolower);
			if (str == "true" || str == "yes" || str == "on" || str == "1")
				return true;
			return false;
		}

		static inline bool StartsWith(std::string const& fullString, std::string const& start)
		{
			return fullString.rfind(start, 0) == 0;
		}

		static inline bool EndsWith(std::string const& fullString, std::string const& ending)
		{
			if (fullString.length() >= ending.length())
				return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
			else
				return false;
		}
	};

}
