#include "spiritpch.h"
#include "Spirit/Core/Window.h"

#if defined(SPIRIT_PLATFORM_WINDOWS) || defined(SPIRIT_PLATFORM_LINUX)
	#include "Platform/Windows/WindowsWindow.h"
#endif

namespace Spirit
{
	Scope<Window> Window::Create(const WindowProps& props)
	{
	#if defined(SPIRIT_PLATFORM_WINDOWS) || defined(SPIRIT_PLATFORM_LINUX)
		return CreateScope<WindowsWindow>(props);
	#else
		SPIRIT_CORE_ASSERT(false, "Unknown platform!");
		return nullptr;
	#endif
	}

}
