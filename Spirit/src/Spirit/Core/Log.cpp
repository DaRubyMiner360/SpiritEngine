#include "spiritpch.h"
#include "Spirit/Core/Log.h"

#include "Spirit/ImGui/ImGuiLogSink.h"

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

namespace Spirit {

	Ref<spdlog::logger> Log::s_CoreLogger;
	Ref<spdlog::logger> Log::s_ClientLogger;

	void Log::Init()
	{
		std::vector<spdlog::sink_ptr> coreSinks;
		std::vector<spdlog::sink_ptr> appSinks;

		auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
		auto consoleSinkPattern = "%^[%T] %n: %v%$";
		consoleSink->set_pattern(consoleSinkPattern);
		coreSinks.emplace_back(consoleSink);
		appSinks.emplace_back(consoleSink);

		auto fileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("Spirit.log", true);
		auto fileSinkPattern = "[%T] [%l] %n: %v";
		fileSink->set_pattern(fileSinkPattern);
		coreSinks.emplace_back(fileSink);
		appSinks.emplace_back(fileSink);

		auto editorConsoleSink = std::make_shared<ImGuiLogSink_mt>();
		auto editorConsoleSinkPattern = "[%T]: %v";
		editorConsoleSink->set_pattern(editorConsoleSinkPattern);
		appSinks.emplace_back(editorConsoleSink);

		s_CoreLogger = std::make_shared<spdlog::logger>("SPIRIT", begin(coreSinks), end(coreSinks));
		spdlog::register_logger(s_CoreLogger);
		s_CoreLogger->set_level(spdlog::level::trace);
		s_CoreLogger->flush_on(spdlog::level::trace);

		s_ClientLogger = std::make_shared<spdlog::logger>("APP", begin(appSinks), end(appSinks));
		spdlog::register_logger(s_ClientLogger);
		s_ClientLogger->set_level(spdlog::level::trace);
		s_ClientLogger->flush_on(spdlog::level::trace);
	}

}
