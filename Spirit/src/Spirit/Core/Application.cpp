#include "spiritpch.h"
#include "Spirit/Core/Application.h"

#include "Spirit/Core/Log.h"

#include "Spirit/Renderer/Renderer.h"
#include "Spirit/Scripting/ScriptEngine.h"

#include "Spirit/Core/Input.h"
#include "Spirit/Utils/PlatformUtils.h"

#include <filesystem>

namespace Spirit {

	Application* Application::s_Instance = nullptr;

	Application::Application(const ApplicationSpecification& specification, bool fullyInitialize, bool deinitialize)
		: m_Specification(specification), m_ShouldDeinitialize(deinitialize)
	{
		SPIRIT_PROFILE_FUNCTION();

		SPIRIT_CORE_ASSERT(!s_Instance, "Application already exists!");
		s_Instance = this;

		// Set working directory here
		if (!m_Specification.WorkingDirectory.empty())
			std::filesystem::current_path(m_Specification.WorkingDirectory);

		m_Window = Window::Create(WindowProps(m_Specification.Name));
		m_Window->SetEventCallback(SPIRIT_BIND_EVENT_FN(OnEvent));

		if (fullyInitialize)
		{
			Renderer::Init();
			ScriptEngine::Init();
		}

		m_ImGuiLayer = new ImGuiLayer();
		PushOverlay(m_ImGuiLayer);
	}

	Application::~Application()
	{
		SPIRIT_PROFILE_FUNCTION();

		if (m_ShouldDeinitialize)
		{
			ScriptEngine::Shutdown();
			Renderer::Shutdown();
		}
	}

	void Application::PushLayer(Layer* layer)
	{
		SPIRIT_PROFILE_FUNCTION();

		m_LayerStack.PushLayer(layer);
		layer->OnAttach();
	}

	void Application::PopLayer(Layer* layer)
	{
		SPIRIT_PROFILE_FUNCTION();

		m_LayerStack.PopLayer(layer);
		layer->OnAttach();
	}

	void Application::PushOverlay(Layer* layer)
	{
		SPIRIT_PROFILE_FUNCTION();

		m_LayerStack.PushOverlay(layer);
		layer->OnAttach();
	}

	void Application::PopOverlay(Layer* layer)
	{
		SPIRIT_PROFILE_FUNCTION();

		m_LayerStack.PopOverlay(layer);
		layer->OnAttach();
	}

	void Application::Close()
	{
		m_Running = false;
	}

	void Application::OnEvent(Event& e)
	{
		SPIRIT_PROFILE_FUNCTION();

		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<WindowCloseEvent>(SPIRIT_BIND_EVENT_FN(OnWindowClose));
		dispatcher.Dispatch<WindowResizeEvent>(SPIRIT_BIND_EVENT_FN(OnWindowResize));

		for (auto it = m_LayerStack.rbegin(); it != m_LayerStack.rend(); ++it)
		{
			if (e.Handled)
				break;
			(*it)->OnEvent(e);
		}
	}

	void Application::OnCustomEvent(CustomEvent& e)
	{
		SPIRIT_PROFILE_FUNCTION();

		for (auto it = m_LayerStack.rbegin(); it != m_LayerStack.rend(); ++it)
		{
			if (e.Handled)
				break;
			(*it)->OnCustomEvent(e);
		}
	}

	void Application::Run()
	{
		SPIRIT_PROFILE_FUNCTION();

		while (m_Running)
		{
			SPIRIT_PROFILE_SCOPE("RunLoop");

			float time = Time::GetTime();
			Timestep timestep = time - m_LastFrameTime;
			m_LastFrameTime = time;

			if (!m_Minimized)
			{
				{
					SPIRIT_PROFILE_SCOPE("LayerStack OnUpdate");

					for (Layer* layer : m_LayerStack)
						layer->OnUpdate(timestep);
				}

				m_ImGuiLayer->Begin();
				{
					SPIRIT_PROFILE_SCOPE("LayerStack OnImGuiRender");

					for (Layer* layer : m_LayerStack)
						layer->OnImGuiRender();
				}
				m_ImGuiLayer->End();
			}

			Input::OnUpdate();

			m_Window->OnUpdate();
		}
	}

	bool Application::OnWindowClose(WindowCloseEvent& e)
	{
		m_Running = false;
		return true;
	}

	bool Application::OnWindowResize(WindowResizeEvent& e)
	{
		SPIRIT_PROFILE_FUNCTION();

		if (e.GetWidth() == 0 || e.GetHeight() == 0)
		{
			m_Minimized = true;
			return false;
		}

		m_Minimized = false;
		Renderer::OnWindowResize(e.GetWidth(), e.GetHeight());

		return false;
	}

}
