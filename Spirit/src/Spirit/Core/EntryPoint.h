#pragma once
#include "Spirit/Core/Base.h"
#include "Spirit/Core/Application.h"

#ifdef SPIRIT_PROFILE
#include <filesystem>
#endif

#if defined(SPIRIT_PLATFORM_WINDOWS) || defined(SPIRIT_PLATFORM_LINUX)

extern Spirit::Application* Spirit::CreateApplication(ApplicationCommandLineArgs args);

int main(int argc, char** argv)
{
	Spirit::Log::Init();

	std::filesystem::path binDirectory = std::filesystem::path(argv[0]).remove_filename();
	SPIRIT_PROFILE_BEGIN_SESSION("Startup", (binDirectory / "SpiritProfile-Startup.json").string());
	auto app = Spirit::CreateApplication({ argc, argv });
	SPIRIT_PROFILE_END_SESSION();

	SPIRIT_PROFILE_BEGIN_SESSION("Runtime", (binDirectory / "SpiritProfile-Runtime.json").string());
	app->Run();
	SPIRIT_PROFILE_END_SESSION();

	SPIRIT_PROFILE_BEGIN_SESSION("Startup", (binDirectory / "SpiritProfile-Shutdown.json").string());
	delete app;
	SPIRIT_PROFILE_END_SESSION();
}
#endif
