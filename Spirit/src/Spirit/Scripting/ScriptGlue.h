#pragma once

namespace Spirit {

	class ScriptGlue
	{
	public:
		static void RegisterComponents();
		static void RegisterFunctions();
	};
}
