#pragma once

#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"

#include <filesystem>
#include <string>

extern "C" {
	typedef struct _MonoClass MonoClass;
	typedef struct _MonoObject MonoObject;
	typedef struct _MonoMethod MonoMethod;
	typedef struct _MonoAssembly MonoAssembly;
	typedef struct _MonoImage MonoImage;
}

namespace Spirit {

	class ScriptClass
	{
	public:
		ScriptClass() = default;
		ScriptClass(const std::string& classNamespace, const std::string& className);

		MonoObject* Instantiate();
		MonoMethod* GetMethod(const std::string& name, int parameterCount);
		MonoObject* InvokeMethod(MonoObject* instance, MonoMethod* method, void** params = nullptr);
		MonoObject* InvokeVirtualMethod(MonoObject* instance, MonoMethod* method, void** params = nullptr);
	private:
		std::string m_ClassNamespace;
		std::string m_ClassName;

		MonoClass* m_MonoClass = nullptr;
	};

	class ScriptInstance
	{
	public:
		ScriptInstance(Ref<ScriptClass> scriptClass, Entity entity);

		void InvokeOnInit();
		void InvokeOnCreate();
		void InvokeOnUpdate(float ts);
		void InvokeOnPhysicsUpdate(float ts);
		void InvokeOnModify();
		void InvokeOnRemove();
		void InvokeOnDestroy();

		MonoObject* GetInstance() { return m_Instance; }
	private:
		Ref<ScriptClass> m_ScriptClass;

		MonoObject* m_Instance = nullptr;
		MonoMethod* m_Constructor = nullptr;
		MonoMethod* m_OnInitMethod = nullptr;
		MonoMethod* m_OnCreateMethod = nullptr;
		MonoMethod* m_OnUpdateMethod = nullptr;
		MonoMethod* m_OnPhysicsUpdateMethod = nullptr;
		MonoMethod* m_OnModifyMethod = nullptr;
		MonoMethod* m_OnRemoveMethod = nullptr;
		MonoMethod* m_OnDestroyMethod = nullptr;
	};

	class ScriptEngine
	{
	public:
		static void Init();
		static void Shutdown();

		static void LoadAssembly(const std::filesystem::path& filepath);

		static void OnRuntimeStart(Scene* scene);
		static void OnRuntimeStop();

		static bool ComponentClassExists(const std::string& fullClassName);
		static void OnInit(Entity entity, ScriptComponent component);
		static void OnCreate(Entity entity, ScriptComponent component);
		static void OnUpdate(Entity entity, Timestep ts);
		static void OnPhysicsUpdate(Entity entity, Timestep ts);
		static void OnModify(Entity entity, ScriptComponent component);
		static void OnRemove(Entity entity, ScriptComponent component);
		static void OnDestroy(Entity entity);

		static Scene* GetSceneContext();
		static std::unordered_map<std::string, Ref<ScriptClass>> GetComponentClasses();
		static std::unordered_map<UUID, std::vector<Ref<ScriptInstance>>> GetEntityComponentInstances();
		static std::unordered_map<UUID, std::string> GetDestroyedEntities();

		static MonoImage* GetCoreAssemblyImage();
	private:
		static void InitMono();
		static void ShutdownMono();

		static MonoObject* InstantiateClass(MonoClass* monoClass);
		static void ScriptEngine::LoadAssemblyClasses(MonoAssembly* assembly);

		friend class ScriptClass;
		friend class ScriptGlue;
	};

}
