#include "spiritpch.h"
#include "ScriptGlue.h"

#include "Spirit/Core/UUID.h"

#include "ScriptEngine.h"
#include "Spirit/Scene/Scene.h"
#include "Spirit/Utils/ScriptWrapperUtils.h"

#include "mono/metadata/object.h"
#include "mono/metadata/reflection.h"

#include "box2d/b2_body.h"

namespace Spirit {

	static std::unordered_map<MonoType*, std::function<void(Entity)>> s_EntityAddComponentFuncs;
	static std::unordered_map<MonoType*, std::function<bool(Entity)>> s_EntityHasComponentFuncs;
	static std::unordered_map<MonoType*, std::function<void(Entity)>> s_EntityRemoveComponentFuncs;

#define SPIRIT_ADD_INTERNAL_CALL(Name) mono_add_internal_call("Spirit.InternalCalls::" #Name, Name)

	static void NativeLog(MonoString* string, int parameter)
	{
		char* cStr = mono_string_to_utf8(string);
		std::string str(cStr);
		mono_free(cStr);
		std::cout << str << ", " << parameter << std::endl;
	}

	static void NativeLog_Vector3(glm::vec3* parameter, glm::vec3* outResult)
	{
		SPIRIT_CORE_WARN("Value: {0}", *parameter);
		*outResult = glm::normalize(*parameter);
	}

	static float NativeLog_Vector3Dot(glm::vec3* parameter)
	{
		SPIRIT_CORE_WARN("Value: {0}", *parameter);
		return glm::dot(*parameter, *parameter);
	}

	static void NativeLog_Vector2(glm::vec2* parameter, glm::vec2* outResult)
	{
		SPIRIT_CORE_WARN("Value: {0}", *parameter);
		*outResult = glm::normalize(*parameter);
	}

	static float NativeLog_Vector2Dot(glm::vec2* parameter)
	{
		SPIRIT_CORE_WARN("Value: {0}", *parameter);
		return glm::dot(*parameter, *parameter);
	}


	static void Entity_Destroy(UUID entityID)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.Destroy();
	}

	static void Entity_GetName(UUID entityID, MonoString** outName)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			*outName = mono_string_new_wrapper("DESTROYED ENTITY");
		}

		*outName = mono_string_new_wrapper(entity.GetComponent<TagComponent>().Name.c_str());
	}

	static void Entity_SetName(UUID entityID, MonoString** name)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.GetComponent<TagComponent>().Name = mono_string_to_utf8(*name);
	}

	static void Entity_GetTag(UUID entityID, MonoString** outTag)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			*outTag = mono_string_new_wrapper("DESTROYED ENTITY");
			return;
		}

		*outTag = mono_string_new_wrapper(entity.GetComponent<TagComponent>().Tag.c_str());
	}

	static void Entity_SetTag(UUID entityID, MonoString** tag)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.GetComponent<TagComponent>().Tag = mono_string_to_utf8(*tag);
	}

	static MonoObject* Entity_AddComponent(UUID entityID, MonoReflectionType* componentType)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		MonoType* managedType = mono_reflection_type_get_type(componentType);
		if (!managedType)
		{
			SPIRIT_CORE_ERROR("Could not find component type {}", mono_type_get_name(managedType));
			return nullptr;
		}

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' and component of type '{}' was already destroyed!", entityID, mono_type_get_name(managedType));
			return nullptr;
		}

		if (s_EntityAddComponentFuncs.find(managedType) != s_EntityAddComponentFuncs.end())
		{
			s_EntityAddComponentFuncs.at(managedType)(entity);
			// Returning nullptr since we're using GetComponent for internal components in C# anyway.
			return nullptr;
		}

		MonoClass* monoComponentClass = mono_class_from_name(ScriptEngine::GetCoreAssemblyImage(), "Spirit", "MonoComponent");
		MonoClass* monoClass = mono_type_get_class(managedType);
		bool isComponent = mono_class_is_subclass_of(monoClass, monoComponentClass, false);
		if (!isComponent)
			return nullptr;

		if (!entity.HasComponent<ScriptWrapperComponent>())
			entity.AddComponent<ScriptWrapperComponent>();

		auto& script = ScriptComponent();
		script.ClassName = std::string(mono_class_get_namespace(monoClass)) + "." + mono_class_get_name(monoClass);
		ScriptWrapperUtils::AddComponent(entity, script);

		std::string typeName = mono_type_get_name(managedType);
		auto& instances = ScriptEngine::GetEntityComponentInstances();
		if (instances.find(entityID) == instances.end())
			return nullptr;
		for (auto& scriptInstance : instances[entityID])
		{
			MonoObject* instance = scriptInstance->GetInstance();
			MonoClass* clazz = mono_object_get_class(instance);
			if (std::string(mono_class_get_namespace(clazz)) + "." + mono_class_get_name(clazz) == typeName)
				return instance;
		}
		return nullptr;
	}

	static bool Entity_HasComponent(UUID entityID, MonoReflectionType* componentType)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		MonoType* managedType = mono_reflection_type_get_type(componentType);
		if (!managedType)
		{
			SPIRIT_CORE_ERROR("Could not find component type {}", mono_type_get_name(managedType));
			return false;
		}

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' and component of type '{}' was already destroyed!", entityID, mono_type_get_name(managedType));
			return false;
		}

		if (s_EntityHasComponentFuncs.find(managedType) != s_EntityHasComponentFuncs.end())
			return s_EntityHasComponentFuncs.at(managedType)(entity);
		else if (!entity.HasComponent<ScriptWrapperComponent>())
			return false;
		else
		{
			MonoClass* monoComponentClass = mono_class_from_name(ScriptEngine::GetCoreAssemblyImage(), "Spirit", "MonoComponent");
			MonoClass* monoClass = mono_type_get_class(managedType);
			bool isComponent = mono_class_is_subclass_of(monoClass, monoComponentClass, false);
			if (!isComponent)
				return false;
			for (auto& script : entity.GetComponent<ScriptWrapperComponent>().Scripts)
			{
				if (script.ClassName == std::string(mono_class_get_namespace(monoClass)) + "." + mono_class_get_name(monoClass))
					return true;
			}
		}
	}

	static MonoObject* Entity_GetMonoComponent(UUID entityID, MonoReflectionType* componentType)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		MonoType* managedType = mono_reflection_type_get_type(componentType);
		if (!managedType)
		{
			SPIRIT_CORE_ERROR("Could not find component type {}", mono_type_get_name(managedType));
			return nullptr;
		}

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' and component of type '{}' was already destroyed!", entityID, mono_type_get_name(managedType));
			return nullptr;
		}

		std::string typeName = mono_type_get_name(managedType);
		auto& instances = ScriptEngine::GetEntityComponentInstances();
		if (instances.find(entityID) == instances.end())
			return nullptr;
		for (auto& scriptInstance : instances[entityID])
		{
			MonoObject* instance = scriptInstance->GetInstance();
			MonoClass* clazz = mono_object_get_class(instance);
			if (std::string(mono_class_get_namespace(clazz)) + "." + mono_class_get_name(clazz) == typeName)
				return instance;
		}
		return nullptr;
	}

	static void Entity_RemoveComponent(UUID entityID, MonoReflectionType* componentType)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		MonoType* managedType = mono_reflection_type_get_type(componentType);
		if (!managedType)
		{
			SPIRIT_CORE_ERROR("Could not find component type {}", mono_type_get_name(managedType));
			return;
		}

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' and component of type '{}' was already destroyed!", entityID, mono_type_get_name(managedType));
			return;
		}

		if (s_EntityRemoveComponentFuncs.find(managedType) != s_EntityRemoveComponentFuncs.end())
		{
			s_EntityRemoveComponentFuncs.at(managedType)(entity);
			return;
		}

		if (!entity.HasComponent<ScriptWrapperComponent>())
			return;

		MonoClass* monoComponentClass = mono_class_from_name(ScriptEngine::GetCoreAssemblyImage(), "Spirit", "MonoComponent");
		MonoClass* monoClass = mono_type_get_class(managedType);
		bool isComponent = mono_class_is_subclass_of(monoClass, monoComponentClass, false);
		if (!isComponent)
			return;

		auto& script = ScriptComponent();
		script.ClassName = std::string(mono_class_get_namespace(monoClass)) + "." + mono_class_get_name(monoClass);
		ScriptWrapperUtils::RemoveComponent(entity, script);
	}


	static void TransformComponent_GetTranslation(UUID entityID, glm::vec3* outTranslation)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			*outTranslation = glm::vec3();
			return;
		}

		*outTranslation = entity.GetComponent<TransformComponent>().Translation;
	}

	static void TransformComponent_SetTranslation(UUID entityID, glm::vec3* translation)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.GetComponent<TransformComponent>().Translation = *translation;
	}

	static void TransformComponent_GetRotation(UUID entityID, glm::vec3* outRotation)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			*outRotation = glm::vec3();
			return;
		}

		*outRotation = entity.GetComponent<TransformComponent>().Rotation;
	}

	static void TransformComponent_SetRotation(UUID entityID, glm::vec3* rotation)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.GetComponent<TransformComponent>().Rotation = *rotation;
	}

	static void TransformComponent_GetScale(UUID entityID, glm::vec3* outScale)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			*outScale = glm::vec3();
			return;
		}

		*outScale = entity.GetComponent<TransformComponent>().Scale;
	}

	static void TransformComponent_SetScale(UUID entityID, glm::vec3* scale)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		entity.GetComponent<TransformComponent>().Scale = *scale;
	}


	static void Rigidbody2DComponent_ApplyLinearImpulse(UUID entityID, glm::vec2* impulse, glm::vec2* point, bool wake)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		auto& rb2d = entity.GetComponent<Rigidbody2DComponent>();
		b2Body* body = (b2Body*)rb2d.RuntimeBody;
		body->ApplyLinearImpulse(b2Vec2(impulse->x, impulse->y), b2Vec2(point->x, point->y), wake);
	}

	static void Rigidbody2DComponent_ApplyLinearImpulseToCenter(UUID entityID, glm::vec2* impulse, bool wake)
	{
		Scene* scene = ScriptEngine::GetSceneContext();
		SPIRIT_CORE_ASSERT(scene);
		Entity entity = scene->GetEntityByUUID(entityID);
		bool entityDestroyed = ScriptEngine::GetDestroyedEntities().find(entityID) != ScriptEngine::GetDestroyedEntities().end();
		if (!entityDestroyed)
			SPIRIT_CORE_ASSERT(entity);

		if (entityDestroyed)
		{
			SPIRIT_ERROR("Entity with ID '{}' was already destroyed!", entityID);
			return;
		}

		auto& rb2d = entity.GetComponent<Rigidbody2DComponent>();
		b2Body* body = (b2Body*)rb2d.RuntimeBody;
		body->ApplyLinearImpulseToCenter(b2Vec2(impulse->x, impulse->y), wake);
	}


	static bool Input_IsKeyPressed(KeyCode keyCode)
	{
		return Input::IsKeyPressed(keyCode);
	}

	static bool Input_IsKeyDown(KeyCode keyCode)
	{
		return Input::IsKeyDown(keyCode);
	}

	static bool Input_IsKeyUp(KeyCode keyCode)
	{
		return Input::IsKeyUp(keyCode);
	}

	static bool Input_IsMouseButtonPressed(MouseCode mouseCode)
	{
		return Input::IsMouseButtonPressed(mouseCode);
	}

	static bool Input_IsMouseButtonDown(MouseCode mouseCode)
	{
		return Input::IsMouseButtonDown(mouseCode);
	}

	static bool Input_IsMouseButtonUp(MouseCode mouseCode)
	{
		return Input::IsMouseButtonUp(mouseCode);
	}

	static float Input_GetMouseX()
	{
		return Input::GetMouseX();
	}

	static float Input_GetMouseY()
	{
		return Input::GetMouseY();
	}

	template<typename... Component>
	static void RegisterComponent()
	{
		([]()
			{
				std::string_view typeName = typeid(Component).name();
				size_t pos = typeName.find_last_of(':');
				std::string_view structName = typeName.substr(pos + 1);
				std::string managedTypename = fmt::format("Spirit.{}", structName);

				MonoType* managedType = mono_reflection_type_from_name(managedTypename.data(), ScriptEngine::GetCoreAssemblyImage());
				if (!managedType)
				{
					SPIRIT_CORE_ERROR("Could not find component type {}", managedTypename);
					return;
				}
				s_EntityAddComponentFuncs[managedType] = [](Entity entity) { entity.AddComponent<Component>(); };
				s_EntityHasComponentFuncs[managedType] = [](Entity entity) { return entity.HasComponent<Component>(); };
				s_EntityRemoveComponentFuncs[managedType] = [](Entity entity) { entity.RemoveComponent<Component>(); };
			}(), ...);
	}

	template<typename... Component>
	static void RegisterComponent(ComponentGroup<Component...>)
	{
		RegisterComponent<Component...>();
	}

	void ScriptGlue::RegisterComponents()
	{
		RegisterComponent(AllComponents{});
	}

	void ScriptGlue::RegisterFunctions()
	{
		SPIRIT_ADD_INTERNAL_CALL(NativeLog);
		SPIRIT_ADD_INTERNAL_CALL(NativeLog_Vector3);
		SPIRIT_ADD_INTERNAL_CALL(NativeLog_Vector3Dot);
		SPIRIT_ADD_INTERNAL_CALL(NativeLog_Vector2);
		SPIRIT_ADD_INTERNAL_CALL(NativeLog_Vector2Dot);

		SPIRIT_ADD_INTERNAL_CALL(Entity_Destroy);
		SPIRIT_ADD_INTERNAL_CALL(Entity_GetName);
		SPIRIT_ADD_INTERNAL_CALL(Entity_SetName);
		SPIRIT_ADD_INTERNAL_CALL(Entity_GetTag);
		SPIRIT_ADD_INTERNAL_CALL(Entity_SetTag);
		SPIRIT_ADD_INTERNAL_CALL(Entity_AddComponent);
		SPIRIT_ADD_INTERNAL_CALL(Entity_HasComponent);
		SPIRIT_ADD_INTERNAL_CALL(Entity_GetMonoComponent);
		//SPIRIT_ADD_INTERNAL_CALL(Entity_GetMonoComponents);
		SPIRIT_ADD_INTERNAL_CALL(Entity_RemoveComponent);

		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_GetTranslation);
		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_SetTranslation);
		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_GetRotation);
		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_SetRotation);
		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_GetScale);
		SPIRIT_ADD_INTERNAL_CALL(TransformComponent_SetScale);

		SPIRIT_ADD_INTERNAL_CALL(Rigidbody2DComponent_ApplyLinearImpulse);
		SPIRIT_ADD_INTERNAL_CALL(Rigidbody2DComponent_ApplyLinearImpulseToCenter);

		SPIRIT_ADD_INTERNAL_CALL(Input_IsKeyPressed);
		SPIRIT_ADD_INTERNAL_CALL(Input_IsKeyDown);
		SPIRIT_ADD_INTERNAL_CALL(Input_IsKeyUp);
		SPIRIT_ADD_INTERNAL_CALL(Input_IsMouseButtonPressed);
		SPIRIT_ADD_INTERNAL_CALL(Input_IsMouseButtonDown);
		SPIRIT_ADD_INTERNAL_CALL(Input_IsMouseButtonUp);
		//SPIRIT_ADD_INTERNAL_CALL(Input_GetMousePosition);
		SPIRIT_ADD_INTERNAL_CALL(Input_GetMouseX);
		SPIRIT_ADD_INTERNAL_CALL(Input_GetMouseY);
	}

}
