#include "spiritpch.h"
#include "ScriptEngine.h"

#include "ScriptGlue.h"
#include "Spirit/Utils/ScriptWrapperUtils.h"

#include "mono/jit/jit.h"
#include "mono/metadata/assembly.h"
#include "mono/metadata/object.h"

namespace Spirit {

	namespace Utils {

		// TODO: move to FileSystem class
		static char* ReadBytes(const std::filesystem::path& filepath, uint32_t* outSize)
		{
			std::ifstream stream(filepath, std::ios::binary | std::ios::ate);

			if (!stream)
			{
				// Failed to open the file
				return nullptr;
			}

			std::streampos end = stream.tellg();
			stream.seekg(0, std::ios::beg);
			uint64_t size = end - stream.tellg();

			if (size == 0)
			{
				// File is empty
				return nullptr;
			}

			char* buffer = new char[size];
			stream.read((char*)buffer, size);
			stream.close();

			*outSize = (uint32_t)size;
			return buffer;
		}

		static MonoAssembly* LoadMonoAssembly(const std::filesystem::path& assemblyPath)
		{
			uint32_t fileSize = 0;
			char* fileData = ReadBytes(assemblyPath, &fileSize);

			// NOTE: We can't use this image for anything other than loading the assembly because this image doesn't have a reference to the assembly
			MonoImageOpenStatus status;
			MonoImage* image = mono_image_open_from_data_full(fileData, fileSize, 1, &status, 0);

			if (status != MONO_IMAGE_OK)
			{
				const char* errorMessage = mono_image_strerror(status);
				// Log some error message using the errorMessage data
				return nullptr;
			}

			std::string pathString = assemblyPath.string();
			MonoAssembly* assembly = mono_assembly_load_from_full(image, pathString.c_str(), &status, 0);
			mono_image_close(image);

			// Don't forget to free the file data
			delete[] fileData;

			return assembly;
		}

		void PrintAssemblyTypes(MonoAssembly* assembly)
		{
			MonoImage* image = mono_assembly_get_image(assembly);
			const MonoTableInfo* typeDefinitionsTable = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);
			int32_t numTypes = mono_table_info_get_rows(typeDefinitionsTable);

			for (int32_t i = 0; i < numTypes; i++)
			{
				uint32_t cols[MONO_TYPEDEF_SIZE];
				mono_metadata_decode_row(typeDefinitionsTable, i, cols, MONO_TYPEDEF_SIZE);

				const char* nameSpace = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAMESPACE]);
				const char* name = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAME]);

				SPIRIT_CORE_TRACE("{}.{}", nameSpace, name);
			}
		}

	}

	struct ScriptEngineData
	{
		MonoDomain* RootDomain = nullptr;
		MonoDomain* AppDomain = nullptr;

		MonoAssembly* CoreAssembly = nullptr;
		MonoImage* CoreAssemblyImage = nullptr;

		ScriptClass MonoComponentClass;

		std::unordered_map<std::string, Ref<ScriptClass>> ComponentClasses;
		std::unordered_map<UUID, std::vector<Ref<ScriptInstance>>> EntityComponentInstances;

		// Runtime
		Scene* SceneContext = nullptr;
		std::unordered_map<UUID, std::string> DestroyedEntities;
	};

	static ScriptEngineData* s_Data = nullptr;

	void ScriptEngine::Init()
	{
		s_Data = new ScriptEngineData();

		InitMono();
		LoadAssembly("Resources/Scripts/Spirit-ScriptCore.dll");
		LoadAssemblyClasses(s_Data->CoreAssembly);

		ScriptGlue::RegisterComponents();
		ScriptGlue::RegisterFunctions();

		// Retrieve and instantiate class
		s_Data->MonoComponentClass = ScriptClass("Spirit", "MonoComponent");
	}

	void ScriptEngine::Shutdown()
	{
		ShutdownMono();
		delete s_Data;
	}

	void ScriptEngine::InitMono()
	{
		mono_set_assemblies_path("mono/lib");

		MonoDomain* rootDomain = mono_jit_init("SpiritJITRuntime");
		SPIRIT_CORE_ASSERT(rootDomain);

		// Store the root domain pointer
		s_Data->RootDomain = rootDomain;
	}

	void ScriptEngine::ShutdownMono()
	{
		// NOTE: mono is a little confusing to shutdown, so maybe come back to this

		// mono_domain_unload(s_Data->AppDomain);
		s_Data->AppDomain = nullptr;

		// mono_jit_cleanup(s_Data->RootDomain);
		s_Data->RootDomain = nullptr;
	}

	void ScriptEngine::LoadAssembly(const std::filesystem::path& filepath)
	{
		// Create an App Domain
		s_Data->AppDomain = mono_domain_create_appdomain("SpiritScriptRuntime", nullptr);
		mono_domain_set(s_Data->AppDomain, true);

		// Move this maybe
		s_Data->CoreAssembly = Utils::LoadMonoAssembly(filepath);
		s_Data->CoreAssemblyImage = mono_assembly_get_image(s_Data->CoreAssembly);
		// Utils::PrintAssemblyTypes(s_Data->CoreAssembly);
	}

	void ScriptEngine::OnRuntimeStart(Scene* scene)
	{
		s_Data->SceneContext = scene;
	}

	void ScriptEngine::OnRuntimeStop()
	{
		s_Data->SceneContext = nullptr;

		s_Data->EntityComponentInstances.clear();
	}

	bool ScriptEngine::ComponentClassExists(const std::string& fullClassName)
	{
		return s_Data->ComponentClasses.find(fullClassName) != s_Data->ComponentClasses.end();
	}

	void ScriptEngine::OnInit(Entity entity, ScriptComponent component)
	{
		UUID entityUUID = entity.GetUUID();

		if (ScriptEngine::ComponentClassExists(component.ClassName))
		{
			if (s_Data->EntityComponentInstances.find(entity.GetUUID()) == s_Data->EntityComponentInstances.end())
				s_Data->EntityComponentInstances[entity.GetUUID()] = {};

			Ref<ScriptInstance> instance = CreateRef<ScriptInstance>(s_Data->ComponentClasses[component.ClassName], entity);
			s_Data->EntityComponentInstances[entity.GetUUID()].push_back(instance);
			instance->InvokeOnInit();
		}

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnCreate(Entity entity, ScriptComponent component)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		int index = ScriptWrapperUtils::GetComponentIndex(entity, component);

		Ref<ScriptInstance> instance = s_Data->EntityComponentInstances[entityUUID].at(index);
		instance->InvokeOnCreate();

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnUpdate(Entity entity, Timestep ts)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		std::vector<Ref<ScriptInstance>> instances = s_Data->EntityComponentInstances[entityUUID];
		for (auto component : entity.GetComponent<ScriptWrapperComponent>().Scripts)
		{
			Ref<ScriptInstance> instance = instances[ScriptWrapperUtils::GetComponentIndex(entity, component)];
			instance->InvokeOnUpdate(ts);
		}

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnPhysicsUpdate(Entity entity, Timestep ts)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		std::vector<Ref<ScriptInstance>> instances = s_Data->EntityComponentInstances[entityUUID];
		for (auto& component : entity.GetComponent<ScriptWrapperComponent>().Scripts)
		{
			Ref<ScriptInstance> instance = instances[ScriptWrapperUtils::GetComponentIndex(entity, component)];
			instance->InvokeOnPhysicsUpdate(ts);
		}

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnModify(Entity entity, ScriptComponent component)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		int index = ScriptWrapperUtils::GetComponentIndex(entity, component);

		Ref<ScriptInstance> instance = s_Data->EntityComponentInstances[entityUUID].at(index);
		instance->InvokeOnModify();

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnRemove(Entity entity, ScriptComponent component)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		int index = ScriptWrapperUtils::GetComponentIndex(entity, component);

		auto& instances = s_Data->EntityComponentInstances[entityUUID];
		Ref<ScriptInstance> instance = instances.at(index);
		instance->InvokeOnRemove();
		instances.erase(instances.begin() + index);

		if (s_Data->DestroyedEntities.find(entityUUID) != s_Data->DestroyedEntities.end())
			s_Data->DestroyedEntities.erase(entityUUID);
	}

	void ScriptEngine::OnDestroy(Entity entity)
	{
		UUID entityUUID = entity.GetUUID();
		SPIRIT_CORE_ASSERT(s_Data->EntityComponentInstances.find(entityUUID) != s_Data->EntityComponentInstances.end());

		std::vector<Ref<ScriptInstance>> instances = s_Data->EntityComponentInstances[entityUUID];
		for (auto& component : entity.GetComponent<ScriptWrapperComponent>().Scripts)
		{
			Ref<ScriptInstance> instance = instances[ScriptWrapperUtils::GetComponentIndex(entity, component)];
			instance->InvokeOnDestroy();
		}
		s_Data->EntityComponentInstances.erase(entity.GetUUID());
	}

	Scene* ScriptEngine::GetSceneContext()
	{
		return s_Data->SceneContext;
	}

	std::unordered_map<std::string, Ref<ScriptClass>> ScriptEngine::GetComponentClasses()
	{
		return s_Data->ComponentClasses;
	}

	std::unordered_map<UUID, std::vector<Ref<ScriptInstance>>> ScriptEngine::GetEntityComponentInstances()
	{
		return s_Data->EntityComponentInstances;
	}

	std::unordered_map<UUID, std::string> ScriptEngine::GetDestroyedEntities()
	{
		return s_Data->DestroyedEntities;
	}

	MonoImage* ScriptEngine::GetCoreAssemblyImage()
	{
		return s_Data->CoreAssemblyImage;
	}

	void ScriptEngine::LoadAssemblyClasses(MonoAssembly* assembly)
	{
		s_Data->ComponentClasses.clear();

		MonoImage* image = mono_assembly_get_image(assembly);
		const MonoTableInfo* typeDefinitionsTable = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);
		int32_t numTypes = mono_table_info_get_rows(typeDefinitionsTable);
		MonoClass* monoComponentClass = mono_class_from_name(image, "Spirit", "MonoComponent");

		for (int32_t i = 0; i < numTypes; i++)
		{
			uint32_t cols[MONO_TYPEDEF_SIZE];
			mono_metadata_decode_row(typeDefinitionsTable, i, cols, MONO_TYPEDEF_SIZE);

			const char* nameSpace = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAMESPACE]);
			const char* name = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAME]);
			std::string fullName;
			if (strlen(nameSpace) != 0)
				fullName = fmt::format("{}.{}", nameSpace, name);
			else
				fullName = name;

			MonoClass* monoClass = mono_class_from_name(image, nameSpace, name);

			if (monoClass == monoComponentClass)
				continue;

			bool isComponent = mono_class_is_subclass_of(monoClass, monoComponentClass, false);
			if (isComponent)
				s_Data->ComponentClasses[fullName] = CreateRef<ScriptClass>(nameSpace, name);
		}
	}

	MonoObject* ScriptEngine::InstantiateClass(MonoClass* monoClass)
	{
		MonoObject* instance = mono_object_new(s_Data->AppDomain, monoClass);
		mono_runtime_object_init(instance);
		return instance;
	}

	ScriptClass::ScriptClass(const std::string& classNamespace, const std::string& className)
		: m_ClassNamespace(classNamespace), m_ClassName(className)
	{
		m_MonoClass = mono_class_from_name(s_Data->CoreAssemblyImage, classNamespace.c_str(), className.c_str());
	}

	MonoObject* ScriptClass::Instantiate()
	{
		return ScriptEngine::InstantiateClass(m_MonoClass);
	}

	MonoMethod* ScriptClass::GetMethod(const std::string& name, int parameterCount)
	{
		return mono_class_get_method_from_name(m_MonoClass, name.c_str(), parameterCount);
	}

	MonoObject* ScriptClass::InvokeMethod(MonoObject* instance, MonoMethod* method, void** params)
	{
		return mono_runtime_invoke(method, instance, params, nullptr);
	}

	MonoObject* ScriptClass::InvokeVirtualMethod(MonoObject* instance, MonoMethod* method, void** params)
	{
		MonoMethod* virtualMethod = mono_object_get_virtual_method(instance, method);
		return InvokeMethod(instance, virtualMethod, params);
	}

	ScriptInstance::ScriptInstance(Ref<ScriptClass> scriptClass, Entity entity)
		: m_ScriptClass(scriptClass)
	{
		m_Instance = scriptClass->Instantiate();

		m_Constructor = s_Data->MonoComponentClass.GetMethod(".ctor", 2);
		m_OnInitMethod = s_Data->MonoComponentClass.GetMethod("OnInit", 0);
		m_OnCreateMethod = s_Data->MonoComponentClass.GetMethod("OnCreate", 0);
		m_OnUpdateMethod = s_Data->MonoComponentClass.GetMethod("OnUpdate", 1);
		m_OnPhysicsUpdateMethod = s_Data->MonoComponentClass.GetMethod("OnPhysicsUpdate", 1);
		m_OnModifyMethod = s_Data->MonoComponentClass.GetMethod("OnModify", 0);
		m_OnRemoveMethod = s_Data->MonoComponentClass.GetMethod("OnRemove", 0);
		m_OnDestroyMethod = s_Data->MonoComponentClass.GetMethod("OnDestroy", 0);

		// Call MonoComponent constructor
		{
			void* param[2] = { &entity.GetUUID(), &entity.GetAssetID() };
			m_ScriptClass->InvokeMethod(m_Instance, m_Constructor, param);
		}
	}

	void ScriptInstance::InvokeOnInit()
	{
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnInitMethod);
	}

	void ScriptInstance::InvokeOnCreate()
	{
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnCreateMethod);
	}

	void ScriptInstance::InvokeOnUpdate(float ts)
	{
		void* param = &ts;
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnUpdateMethod, &param);
	}

	void ScriptInstance::InvokeOnPhysicsUpdate(float ts)
	{
		void* param = &ts;
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnPhysicsUpdateMethod, &param);
	}

	void ScriptInstance::InvokeOnModify()
	{
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnModifyMethod);
	}

	void ScriptInstance::InvokeOnRemove()
	{
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnRemoveMethod);
	}

	void ScriptInstance::InvokeOnDestroy()
	{
		m_ScriptClass->InvokeVirtualMethod(m_Instance, m_OnDestroyMethod);
	}

}
