#pragma once

#include "Spirit/Renderer/Camera.h"
#include "Spirit/Renderer/EditorCamera.h"
#include "Spirit/Renderer/OrthographicCamera.h"

#include "Spirit/Renderer/Model.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/Vertex.h"
#include "Spirit/Scene/Components.h"

#include <string>

namespace Spirit {

	class Renderer3D
	{
	public:
		static void Init();
		static void Shutdown();

		static void BeginScene(const Camera& camera, const glm::mat4& transform);
		static void BeginScene(const EditorCamera& camera);
		static void BeginScene(const OrthographicCamera& camera); // TODO: Remove
		static void EndScene();
		static void Flush();

		// Primitives
		static void DrawMesh(const glm::mat4& transform, MeshRendererComponent& src, int entityID);

		// Stats
		struct Renderer3DStatistics
		{
			uint32_t DrawCalls = 0;
			uint32_t MeshCount = 0;
			uint32_t TextureCount = 0;

			uint32_t GetTotalVertexCount() const { return MeshCount * 4; }
			uint32_t GetTotalIndexCount() const { return MeshCount * 6; }
		};
		static void ResetStats();
		static Renderer3DStatistics GetStats();
		static void IncrementMeshCount();

	private:
		static void StartBatch();
		static void NextBatch();
	};

}
