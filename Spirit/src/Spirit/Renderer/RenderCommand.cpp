#include "spiritpch.h"
#include "Spirit/Renderer/RenderCommand.h"

namespace Spirit {

	Scope<RendererAPI> RenderCommand::s_RendererAPI = RendererAPI::Create();

}
