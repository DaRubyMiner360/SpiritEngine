#include "spiritpch.h"
#include "Spirit/Renderer/Model.h"

#include "Spirit/Renderer/Renderer.h"
#include "Platform/OpenGL/OpenGLModel.h"
#include "Spirit/Core/Application.h"

namespace Spirit {

	Ref<Model> Model::Create(std::string const& path, bool gamma)
	{
		switch (Renderer::GetAPI())
		{
		case RendererAPI::API::None:    SPIRIT_CORE_ASSERT(false, "RendererAPI::None is currently not supported!"); return nullptr;
		case RendererAPI::API::OpenGL:  return CreateRef<OpenGLModel>(path, gamma);
		}

		SPIRIT_CORE_ASSERT(false, "Unknown RendererAPI!");
		return nullptr;
	}

}
