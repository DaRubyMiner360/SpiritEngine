#include "spiritpch.h"
#include "Spirit/Renderer/Mesh.h"

#include "Spirit/Renderer/Renderer.h"
#include "Platform/OpenGL/OpenGLMesh.h"
#include "Spirit/Core/Application.h"

namespace Spirit {

	Ref<Mesh> Mesh::Create(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<ModelTexture> textures)
	{
		switch (Renderer::GetAPI())
		{
		case RendererAPI::API::None:    SPIRIT_CORE_ASSERT(false, "RendererAPI::None is currently not supported!"); return nullptr;
		case RendererAPI::API::OpenGL:  return CreateRef<OpenGLMesh>(vertices, indices, textures);
		}

		SPIRIT_CORE_ASSERT(false, "Unknown RendererAPI!");
		return nullptr;
	}

}
