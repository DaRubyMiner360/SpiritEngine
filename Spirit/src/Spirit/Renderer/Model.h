#pragma once

#include "Spirit/Core/Base.h"

#include "Spirit/Renderer/Mesh.h"
#include "Spirit/Renderer/Shader.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

namespace Spirit {

	class Model
	{
	public:
		static Ref<Model> Create(std::string const& path, bool gamma = false);

		std::string GetPath() { return m_Path; }

		virtual void Draw(Ref<Shader>& shader, const glm::vec4& tintColor = { 1, 1, 1, 1 }, const glm::vec2& textureUVOffset = { 0, 0 }, const glm::mat4& transform = {}, const glm::vec3 cameraPosition = {}, const glm::vec3 cameraRotation = {}, const int entityID = -1) = 0;
	protected:
		std::string m_Path;
	};
}
