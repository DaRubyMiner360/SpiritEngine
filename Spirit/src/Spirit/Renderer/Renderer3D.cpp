#include "spiritpch.h"
#include "Spirit/Renderer/Renderer3D.h"

#include "Spirit/Renderer/VertexArray.h"
#include "Spirit/Renderer/Shader.h"
#include "Spirit/Renderer/UniformBuffer.h"
#include "Spirit/Renderer/RenderCommand.h"
#include "Spirit/Math/Math.h"

#include <glm/gtc/matrix_transform.hpp>

namespace Spirit {

	struct Renderer3DData
	{
		static const uint32_t MaxQuads = 20000;
		static const uint32_t MaxVertices = MaxQuads * 4;
		static const uint32_t MaxIndices = MaxQuads * 36;
		static const uint32_t MaxTextureSlots = 32; // TODO: RenderCaps

		Ref<Texture2D> WhiteTexture;

		Ref<VertexArray> MeshVertexArray;
		Ref<VertexBuffer> MeshVertexBuffer;
		Ref<Shader> MeshShader;

		uint32_t MeshIndexCount = 0;
		MeshVertex* MeshVertexBufferBase = nullptr;
		MeshVertex* MeshVertexBufferPtr = nullptr;

		std::array<Ref<Texture2D>, MaxTextureSlots> TextureSlots;
		uint32_t TextureSlotIndex = 1; // 0 = white texture

		Renderer3D::Renderer3DStatistics Stats;

		struct CameraData
		{
			glm::mat4 ViewProjection;
		};
		CameraData CameraBuffer;
		Ref<UniformBuffer> CameraUniformBuffer;
		glm::vec3 CameraPosition;
		glm::vec3 CameraRotation;
	};

	static Renderer3DData s_Data;

	void Renderer3D::Init()
	{
		SPIRIT_PROFILE_FUNCTION();

		s_Data.MeshVertexArray = VertexArray::Create();

		s_Data.MeshVertexBuffer = VertexBuffer::Create(s_Data.MaxVertices * sizeof(MeshVertex));
		s_Data.MeshVertexBuffer->SetLayout({
			{ ShaderDataType::Float3, "a_Position"     },
			{ ShaderDataType::Float3, "a_Normal"	   },
			{ ShaderDataType::Float3, "a_Tangent"      },
			{ ShaderDataType::Float3, "a_Bitangent"    },
			{ ShaderDataType::Float4, "a_Color"        },
			{ ShaderDataType::Float2, "a_TexCoord"     },
			{ ShaderDataType::Int,    "a_EntityID"     }
			});
		s_Data.MeshVertexArray->AddVertexBuffer(s_Data.MeshVertexBuffer);

		// TODO: Make this load a white cubemap texture with shading
		s_Data.WhiteTexture = Texture2D::Create(1, 1);
		uint32_t whiteTextureData = 0xffffffff;
		s_Data.WhiteTexture->SetData(&whiteTextureData, sizeof(uint32_t));

		int32_t samplers[s_Data.MaxTextureSlots];
		for (uint32_t i = 0; i < s_Data.MaxTextureSlots; i++)
			samplers[i] = i;

		s_Data.MeshShader = Shader::Create("assets/shaders/Renderer3D_Mesh.glsl");

		// Set first texture slot to 0
		s_Data.TextureSlots[0] = s_Data.WhiteTexture;

		s_Data.CameraUniformBuffer = UniformBuffer::Create(sizeof(Renderer3DData::CameraData), 0);
	}

	void Renderer3D::Shutdown()
	{
		SPIRIT_PROFILE_FUNCTION();

		delete[] s_Data.MeshVertexBufferBase;
	}

	void Renderer3D::BeginScene(const OrthographicCamera& camera)
	{
		SPIRIT_PROFILE_FUNCTION();

		glm::vec3 position, rotation, scale;
		Math::DecomposeTransform(camera.GetViewMatrix(), position, rotation, scale);
		s_Data.CameraPosition = position;
		s_Data.CameraRotation = rotation;

		s_Data.CameraBuffer.ViewProjection = camera.GetViewProjectionMatrix();
		s_Data.CameraUniformBuffer->SetData(&s_Data.CameraBuffer, sizeof(Renderer3DData::CameraData));

		StartBatch();
	}

	void Renderer3D::BeginScene(const Camera& camera, const glm::mat4& transform)
	{
		SPIRIT_PROFILE_FUNCTION();

		glm::vec3 position, rotation, scale;
		Math::DecomposeTransform(glm::inverse(transform), position, rotation, scale);
		s_Data.CameraPosition = position;
		s_Data.CameraRotation = rotation;

		s_Data.CameraBuffer.ViewProjection = camera.GetProjection() * glm::inverse(transform);
		s_Data.CameraUniformBuffer->SetData(&s_Data.CameraBuffer, sizeof(Renderer3DData::CameraData));

		StartBatch();
	}

	void Renderer3D::BeginScene(const EditorCamera& camera)
	{
		SPIRIT_PROFILE_FUNCTION();

		glm::vec3 position, rotation, scale;
		Math::DecomposeTransform(camera.GetViewMatrix(), position, rotation, scale);
		s_Data.CameraPosition = position;
		s_Data.CameraRotation = rotation;

		s_Data.CameraBuffer.ViewProjection = camera.GetViewProjection();
		s_Data.CameraUniformBuffer->SetData(&s_Data.CameraBuffer, sizeof(Renderer3DData::CameraData));

		StartBatch();
	}

	void Renderer3D::EndScene()
	{
		SPIRIT_PROFILE_FUNCTION();

		Flush();
	}

	void Renderer3D::StartBatch()
	{
		s_Data.MeshIndexCount = 0;
		s_Data.MeshVertexBufferPtr = s_Data.MeshVertexBufferBase;

		s_Data.TextureSlotIndex = 1;
	}

	void Renderer3D::Flush()
	{
		if (s_Data.MeshIndexCount)
		{
			uint32_t dataSize = (uint32_t)((uint8_t*)s_Data.MeshVertexBufferPtr - (uint8_t*)s_Data.MeshVertexBufferBase);
			s_Data.MeshVertexBuffer->SetData(s_Data.MeshVertexBufferBase, dataSize);

			// Bind textures
			for (uint32_t i = 0; i < s_Data.TextureSlotIndex; i++)
				s_Data.TextureSlots[i]->Bind(i);

			s_Data.MeshShader->Bind();
			//RenderCommand::DrawIndexed(s_Data.MeshVertexArray, s_Data.MeshIndexCount);
			s_Data.Stats.DrawCalls++;
		}
	}

	void Renderer3D::NextBatch()
	{
		Flush();
		StartBatch();
	}

	void Renderer3D::DrawMesh(const glm::mat4& transform, MeshRendererComponent& src, int entityID)
	{
		Ref<Model> model = src.Mesh;
		if (model)
			model->Draw(s_Data.MeshShader, src.Color, src.TextureUVOffset, transform, s_Data.CameraPosition, s_Data.CameraRotation, entityID);
	}

	void Renderer3D::ResetStats()
	{
		memset(&s_Data.Stats, 0, sizeof(Renderer3DStatistics));
	}

	Renderer3D::Renderer3DStatistics Renderer3D::GetStats()
	{
		return s_Data.Stats;
	}

	void Renderer3D::IncrementMeshCount()
	{
		s_Data.Stats.MeshCount++;
	}
}
