#pragma once

#include "Spirit/Core/Base.h"

#include "Spirit/Renderer/Vertex.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/Shader.h"

#include <vector>

namespace Spirit {

	class Mesh
	{
	public:
		static Ref<Mesh> Create(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<ModelTexture> textures);

		virtual void Draw(Ref<Shader>& shader, const glm::vec4& tintColor = { 1, 1, 1, 1 }, const glm::vec2& textureUVOffset = { 0, 0 }, const glm::mat4& transform = {}, const glm::vec3 cameraPosition = {}, const glm::vec3 cameraRotation = {}, const int entityID = -1) = 0;
	private:
		virtual void SetupMesh() = 0;
	};
}
