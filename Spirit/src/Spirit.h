#pragma once

// For use by Spirit applications

#include "Spirit/Core/Base.h"

#include "Spirit/Core/Application.h"
#include "Spirit/Core/Layer.h"
#include "Spirit/Core/Log.h"

#include "Spirit/Core/Timestep.h"

#include "Spirit/Core/Input.h"
#include "Spirit/Core/KeyCodes.h"
#include "Spirit/Core/MouseCodes.h"
#include "Spirit/Renderer/OrthographicCameraController.h"

#include "Spirit/ImGui/ImGuiLayer.h"

#include "Spirit/Utils/Random.h"

#include "Spirit/Scene/Scene.h"
#include "Spirit/Scene/Entity.h"
#include "Spirit/Scene/Components.h"

// ---Renderer------------------------
#include "Spirit/Renderer/Renderer.h"
#include "Spirit/Renderer/Renderer2D.h"
#include "Spirit/Renderer/Renderer3D.h"
#include "Spirit/Renderer/RenderCommand.h"

#include "Spirit/Renderer/Buffer.h"
#include "Spirit/Renderer/Shader.h"
#include "Spirit/Renderer/Framebuffer.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/VertexArray.h"

#include "Spirit/Renderer/OrthographicCamera.h"
// -----------------------------------

#ifdef SPIRIT_BUILD_DLL
// BUILD LIB
#define SPIRIT_EXPORT __declspec(dllexport)
#else
// USE LIB
#define SPIRIT_EXPORT __declspec(dllimport)
#endif
