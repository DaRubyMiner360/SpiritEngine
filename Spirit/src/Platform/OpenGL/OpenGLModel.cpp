#include "spiritpch.h"
#include "Platform/OpenGL/OpenGLModel.h"

//#include "Spirit/Renderer/Mesh.h"
//#include "Spirit/Renderer/Model.h"

//#include "Spirit/Renderer/Vertex.h"
//#include "Spirit/Renderer/Texture.h"
//#include "Spirit/Renderer/Shader.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stb_image.h>
//#include <scene.h>
//#include <stb_image.h>
#include <glad/glad.h>

//#include <string>
//#include <fstream>
//#include <sstream>
//#include <iostream>
//#include <map>
//#include <vector>

namespace Spirit {

	OpenGLModel::OpenGLModel(std::string const& path, bool gamma)
		: m_GammaCorrection(gamma)
	{
		m_Path = path;

		LoadModel(path);
	}

	void OpenGLModel::Draw(Ref<Shader>& shader, const glm::vec4& tintColor, const glm::vec2& textureUVOffset, const glm::mat4& transform, const glm::vec3 cameraPosition, const glm::vec3 cameraRotation, const int entityID)
	{
		shader->Bind();

		for (unsigned int i = 0; i < m_Meshes.size(); i++)
			m_Meshes[i]->Draw(shader, tintColor, textureUVOffset, transform, cameraPosition, cameraRotation, entityID);
	}

	void OpenGLModel::LoadModel(std::string const& path)
	{
		// Read file via ASSIMP
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		{
			SPIRIT_ERROR(importer.GetErrorString());
			return;
		}

		// Retrieve the directory path of the filepath
		if (path.find('/') != std::string::npos)
			m_Directory = path.substr(0, path.find_last_of('/'));
		else if (path.find('\\') != std::string::npos)
			m_Directory = path.substr(0, path.find_last_of('\\'));

		// Process ASSIMP's root node recursively
		ProcessNode(scene->mRootNode, scene);
	}

	void OpenGLModel::ProcessNode(aiNode* node, const aiScene* scene)
	{
		// Process each mesh located at the current node
		for (unsigned int i = 0; i < node->mNumMeshes; i++)
		{
			// The node object only contains indices to index the actual objects in the scene. 
			// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			m_Meshes.push_back(ProcessMesh(mesh, scene));
		}
		// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			ProcessNode(node->mChildren[i], scene);
		}

	}

	Ref<Mesh> OpenGLModel::ProcessMesh(aiMesh* mesh, const aiScene* scene)
	{
		std::vector<MeshVertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<ModelTexture> textures;

		// Walk through each of the mesh's vertices
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			MeshVertex vertex;
			glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
			// Positions
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.Position = vector;
			// Normals
			if (mesh->HasNormals())
			{
				vector.x = mesh->mNormals[i].x;
				vector.y = mesh->mNormals[i].y;
				vector.z = mesh->mNormals[i].z;
				vertex.Normal = vector;
			}
			// Texture coordinates
			if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.TexCoord = vec;
				// Tangent
				vector.x = mesh->mTangents[i].x;
				vector.y = mesh->mTangents[i].y;
				vector.z = mesh->mTangents[i].z;
				vertex.Tangent = vector;
				// Bitangent
				vector.x = mesh->mBitangents[i].x;
				vector.y = mesh->mBitangents[i].y;
				vector.z = mesh->mBitangents[i].z;
				vertex.Bitangent = vector;
			}
			else
				vertex.TexCoord = glm::vec2(0.0f, 0.0f);
			// Colors
			glm::vec4 color;
			if (mesh->mColors[0])
			{
				color.r = mesh->mColors[0][i].r;
				color.g = mesh->mColors[0][i].g;
				color.b = mesh->mColors[0][i].b;
				color.a = mesh->mColors[0][i].a;
				vertex.Color = color;
			}

			vertices.push_back(vertex);
		}
		// Now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			// Retrieve all indices of the face and store them in the indices vector
			for (unsigned int j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}
		// Process materials
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'u_TextureDiffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: u_TextureDiffuseN
		// Specular: u_TextureSpecularN
		// Normal: u_TextureNormalN

		// 1. Diffuse maps
		std::vector<ModelTexture> diffuseMaps = LoadMaterialTextures(material, aiTextureType_DIFFUSE, "u_TextureDiffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<ModelTexture> specularMaps = LoadMaterialTextures(material, aiTextureType_SPECULAR, "u_TextureSpecular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		std::vector<ModelTexture> normalMaps = LoadMaterialTextures(material, aiTextureType_HEIGHT, "u_TextureNormal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		std::vector<ModelTexture> heightMaps = LoadMaterialTextures(material, aiTextureType_AMBIENT, "u_TextureHeight");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());

		// Return a mesh object created from the extracted mesh data
		return Mesh::Create(vertices, indices, textures);
	}

	std::vector<ModelTexture> OpenGLModel::LoadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
	{
		std::vector<ModelTexture> textures;
		for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
		{
			aiString str;
			mat->GetTexture(type, i, &str);
			// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
			bool skip = false;
			for (unsigned int j = 0; j < m_LoadedTextures.size(); j++)
			{
				if (std::strcmp(m_LoadedTextures[j].path.data(), str.C_Str()) == 0)
				{
					textures.push_back(m_LoadedTextures[j]);
					skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
					break;
				}
			}
			if (!skip)
			{   // If texture hasn't been loaded already, load it
				ModelTexture texture;
				texture.id = TextureFromFile(str.C_Str(), this->m_Directory);
				texture.type = typeName;
				texture.path = str.C_Str();
				textures.push_back(texture);
				m_LoadedTextures.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
			}
		}
		return textures;
	}


	unsigned int TextureFromFile(const std::string path, const std::string& directory, bool gamma)
	{
		std::string filename = directory + '/' + path;

		unsigned int textureID;
		glGenTextures(1, &textureID);

		int width, height, nrComponents;
		unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
		if (data)
		{
			GLenum format;
			if (nrComponents == 1)
				format = GL_RED;
			else if (nrComponents == 3)
				format = GL_RGB;
			else if (nrComponents == 4)
				format = GL_RGBA;

			glBindTexture(GL_TEXTURE_2D, textureID);
			glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			stbi_image_free(data);
		}
		else
		{
			SPIRIT_ERROR("Texture failed to load at path: {0}", path);
			stbi_image_free(data);
		}

		return textureID;
	}
}
