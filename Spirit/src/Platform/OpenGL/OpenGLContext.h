#pragma once

#include "Spirit/Renderer/GraphicsContext.h"

struct GLFWwindow;

namespace Spirit {

	class OpenGLContext : public GraphicsContext
	{
	public:
		OpenGLContext(GLFWwindow* windowHandle);

		virtual void Init() override;
		virtual void SwapBuffers() override;
	private:
		GLFWwindow* m_WindowHandle;
	};

}
