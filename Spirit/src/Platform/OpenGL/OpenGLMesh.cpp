#include "spiritpch.h"
#include "Platform/OpenGL/OpenGLMesh.h"

#include "Spirit/Renderer/Renderer3D.h"
#include "Spirit/Renderer/UniformBuffer.h"

#include <glad/glad.h>

#include <vector>

namespace Spirit {

	OpenGLMesh::OpenGLMesh(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<ModelTexture> textures)
		: m_Vertices(vertices), m_Indices(indices), m_Textures(textures) {}

	void OpenGLMesh::Draw(Ref<Shader>& shader, const glm::vec4& tintColor, const glm::vec2& textureUVOffset, const glm::mat4& transform, const glm::vec3 cameraPosition, const glm::vec3 cameraRotation, const int entityID)
	{
		auto savedVertices = m_Vertices;

		for (size_t i = 0; i < m_Vertices.size(); i++)
		{
			m_Vertices[i].Position = transform * glm::vec4(m_Vertices[i].Position, 1.0f);
			m_Vertices[i].TexCoord = m_Vertices[i].TexCoord - textureUVOffset;
			m_Vertices[i].Color = tintColor * m_Vertices[i].Color;
			m_Vertices[i].EntityID = entityID;
		}

		SetupMesh();

		// Bind appropriate textures
		struct DirectionalLight
		{
			glm::vec3 Direction;

			glm::vec3 Ambient;
			glm::vec3 Diffuse;
			glm::vec3 Specular;
		};
		struct PointLight
		{
			glm::vec3 Position;

			float Constant;
			float Linear;
			float Quadratic;

			glm::vec3 Ambient;
			glm::vec3 Diffuse;
			glm::vec3 Specular;
		};
		struct SpotLight
		{
			glm::vec3 Position;
			glm::vec3 Direction;
			float CutOff;
			float OuterCutOff;

			float Constant;
			float Linear;
			float Quadratic;

			glm::vec3 Ambient;
			glm::vec3 Diffuse;
			glm::vec3 Specular;
		};
		struct Material
		{
			int Diffuse;
			int Specular;
			float Shininess;
		};
		struct Data
		{
			int HasTextures;
			int DirectionalLightCount;
			int PointLightCount;
			int SpotLightCount;

			glm::vec3 ViewPosition;
			std::vector<DirectionalLight> DirectionalLights;
			std::vector<PointLight> PointLights;
			std::vector<SpotLight> SpotLights;

			float MaterialShininess;
		};
		Data DataBuffer;
		Ref<UniformBuffer> DataUniformBuffer;
		DataUniformBuffer = UniformBuffer::Create(sizeof(Data), 1);
		DataBuffer.HasTextures = m_Textures.size() > 0;

		DataBuffer.ViewPosition = cameraPosition;

		

		// positions of the point lights
		glm::vec3 pointLightPositions[] = {
			glm::vec3(0.7f,  0.2f,  2.0f),
			glm::vec3(2.3f, -3.3f, -4.0f),
			glm::vec3(-4.0f,  2.0f, -12.0f),
			glm::vec3(0.0f,  0.0f, -3.0f)
		};

		/*
		   Here we set all the uniforms for the 5/6 types of lights we have. We have to set them manually and index
		   the proper PointLight struct in the array to set each uniform variable. This can be done more code-friendly
		   by defining light types as classes and set their values in there, or by using a more efficient uniform approach
		   by using 'Uniform buffer objects', but that is something we'll discuss in the 'Advanced GLSL' tutorial.
		*/
		// Directional light
		DataBuffer.DirectionalLights.push_back({});
		DataBuffer.DirectionalLights[0].Direction = { -0.2f, -1.0f, -0.3f };
		DataBuffer.DirectionalLights[0].Ambient = { 0.05f, 0.05f, 0.05f };
		DataBuffer.DirectionalLights[0].Diffuse = { 0.4f, 0.4f, 0.4f };
		DataBuffer.DirectionalLights[0].Direction = { 0.5f, 0.5f, 0.5f };
		// Point light 1
		DataBuffer.PointLights.push_back({});
		DataBuffer.PointLights[0].Position = pointLightPositions[0];
		DataBuffer.PointLights[0].Ambient = { 0.05f, 0.05f, 0.05f };
		DataBuffer.PointLights[0].Diffuse = { 0.8f, 0.8f, 0.8f };
		DataBuffer.PointLights[0].Specular = { 1.0f, 1.0f, 1.0f };
		DataBuffer.PointLights[0].Constant = 1.0f;
		DataBuffer.PointLights[0].Linear = 0.09;
		DataBuffer.PointLights[0].Quadratic = 0.032;
		// Point light 2
		DataBuffer.PointLights.push_back({});
		DataBuffer.PointLights[1].Position = pointLightPositions[1];
		DataBuffer.PointLights[1].Ambient = { 0.05f, 0.05f, 0.05f };
		DataBuffer.PointLights[1].Diffuse = { 0.8f, 0.8f, 0.8f };
		DataBuffer.PointLights[1].Specular = { 1.0f, 1.0f, 1.0f };
		DataBuffer.PointLights[1].Constant = 1.0f;
		DataBuffer.PointLights[1].Linear = 0.09;
		DataBuffer.PointLights[1].Quadratic = 0.032;
		// Point light 3
		DataBuffer.PointLights.push_back({});
		DataBuffer.PointLights[2].Position = pointLightPositions[2];
		DataBuffer.PointLights[2].Ambient = { 0.05f, 0.05f, 0.05f };
		DataBuffer.PointLights[2].Diffuse = { 0.8f, 0.8f, 0.8f };
		DataBuffer.PointLights[2].Specular = { 1.0f, 1.0f, 1.0f };
		DataBuffer.PointLights[2].Constant = 1.0f;
		DataBuffer.PointLights[2].Linear = 0.09;
		DataBuffer.PointLights[2].Quadratic = 0.032;
		// Point light 4
		DataBuffer.PointLights.push_back({});
		DataBuffer.PointLights[3].Position = pointLightPositions[3];
		DataBuffer.PointLights[3].Ambient = { 0.05f, 0.05f, 0.05f };
		DataBuffer.PointLights[3].Diffuse = { 0.8f, 0.8f, 0.8f };
		DataBuffer.PointLights[3].Specular = { 1.0f, 1.0f, 1.0f };
		DataBuffer.PointLights[3].Constant = 1.0f;
		DataBuffer.PointLights[3].Linear = 0.09;
		DataBuffer.PointLights[3].Quadratic = 0.032;
		// Spot light
		DataBuffer.SpotLights.push_back({});

		DataBuffer.SpotLights[0].Position = cameraPosition;
		DataBuffer.SpotLights[0].Direction = cameraRotation;
		DataBuffer.SpotLights[0].Ambient = { 0.0f, 0.0f, 0.0f };
		DataBuffer.SpotLights[0].Diffuse = { 1.0f, 1.0f, 1.0f };
		DataBuffer.SpotLights[0].Specular = { 1.0f, 1.0f, 1.0f };
		DataBuffer.SpotLights[0].Constant = 1.0f;
		DataBuffer.SpotLights[0].Linear = 0.09;
		DataBuffer.SpotLights[0].Quadratic = 0.032;
		DataBuffer.SpotLights[0].CutOff = glm::cos(glm::radians(12.5f));
		DataBuffer.SpotLights[0].OuterCutOff = glm::cos(glm::radians(15.0f));



		DataBuffer.DirectionalLightCount = DataBuffer.DirectionalLights.size();
		DataBuffer.PointLightCount = DataBuffer.PointLights.size();
		DataBuffer.SpotLightCount = DataBuffer.SpotLights.size();


		DataBuffer.MaterialShininess = 32.0f;

		

		DataUniformBuffer->SetData(&DataBuffer, sizeof(Data));


		shader->SetInt("u_MaterialDiffuse", 0);
		shader->SetInt("u_MaterialSpecular", 1);


		unsigned int diffuseNr = 1;
		unsigned int specularNr = 1;
		unsigned int normalNr = 1;
		unsigned int heightNr = 1;
		for (unsigned int i = 0; i < m_Textures.size(); i++)
		{
			glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
			// Retrieve texture number (the N in diffuse_textureN)
			std::string number;
			std::string name = m_Textures[i].type;
			if (name == "u_TextureDiffuse")
				number = std::to_string(diffuseNr++);
			else if (name == "u_TextureSpecular")
				number = std::to_string(specularNr++); // Transfer unsigned int to string
			else if (name == "u_TextureNormal")
				number = std::to_string(normalNr++); // Transfer unsigned int to string
			else if (name == "u_TextureHeight")
				number = std::to_string(heightNr++); // Transfer unsigned int to string

			//  Now set the sampler to the correct texture unit
			shader->SetInt(name + number, i);
			// And finally bind the texture
			glBindTexture(GL_TEXTURE_2D, m_Textures[i].id);
		}

		// Draw mesh
		glBindVertexArray(m_VAO);
		glDrawElements(GL_TRIANGLES, m_Indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		// Always good practice to set everything back to defaults once configured.
		glActiveTexture(GL_TEXTURE0);

		m_Vertices = savedVertices;

		Renderer3D::IncrementMeshCount();
	}

	void OpenGLMesh::SetupMesh()
	{
		// Create buffers/arrays
		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);
		glGenBuffers(1, &m_EBO);

		glBindVertexArray(m_VAO);
		// Load data into vertex buffers
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		// A great thing about structs is that their memory layout is sequential for all its items.
		// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
		// Again translates to 3/2 floats which translates to a byte array.
		glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(MeshVertex), &m_Vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(unsigned int), &m_Indices[0], GL_STATIC_DRAW);


		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, Normal));

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, TexCoord));

		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, Color));

		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, Tangent));

		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(MeshVertex), (void*)offsetof(MeshVertex, Bitangent));

		glEnableVertexAttribArray(6);
		glVertexAttribIPointer(6, 1, GL_INT, sizeof(MeshVertex), (void*)offsetof(MeshVertex, EntityID));

		glBindVertexArray(0);
	}
}
