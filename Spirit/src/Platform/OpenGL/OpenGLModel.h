#pragma once

#include "Spirit/Renderer/Model.h"

#include <Importer.hpp>
#include <postprocess.h>
#include <scene.h>

#include "Spirit/Renderer/Mesh.h"

#include "Spirit/Renderer/Vertex.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/Shader.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

namespace Spirit {

	class OpenGLModel : public Model
	{
	public:
		std::vector<ModelTexture> m_LoadedTextures; // Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
		std::vector<Ref<Mesh>> m_Meshes;
		std::string m_Directory;
		bool m_GammaCorrection;

		OpenGLModel(std::string const& path, bool gamma = false);

		virtual void Draw(Ref<Shader>& shader, const glm::vec4& tintColor = { 1, 1, 1, 1 }, const glm::vec2& textureUVOffset = { 0, 0 }, const glm::mat4& transform = {}, const glm::vec3 cameraPosition = {}, const glm::vec3 cameraRotation = {}, const int entityID = -1) override;
	private:
		void LoadModel(std::string const& path);
		void ProcessNode(aiNode* node, const aiScene* scene);
		Ref<Mesh> ProcessMesh(aiMesh* mesh, const aiScene* scene);
		std::vector<ModelTexture> LoadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
	};

	unsigned int TextureFromFile(const std::string path, const std::string& directory, bool gamma = false);
}
