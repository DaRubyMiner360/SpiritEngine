#pragma once

#include "Spirit/Renderer/Mesh.h"

#include "Spirit/Renderer/Vertex.h"
#include "Spirit/Renderer/Texture.h"
#include "Spirit/Renderer/Shader.h"

#include <vector>

namespace Spirit {

	class OpenGLMesh : public Mesh
	{
	public:
		std::vector<MeshVertex> m_Vertices;
		std::vector<unsigned int> m_Indices;
		std::vector<ModelTexture> m_Textures;

		unsigned int m_VAO;

		OpenGLMesh(std::vector<MeshVertex> vertices, std::vector<unsigned int> indices, std::vector<ModelTexture> textures);
		virtual void Draw(Ref<Shader>& shader, const glm::vec4& tintColor = { 1, 1, 1, 1 }, const glm::vec2& textureUVOffset = { 0, 0 }, const glm::mat4& transform = {}, const glm::vec3 cameraPosition = {}, const glm::vec3 cameraRotation = {}, const int entityID = -1) override;
	private:
		unsigned int m_VBO, m_EBO;

		virtual void SetupMesh() override;
	};


}
