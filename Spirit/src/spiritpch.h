#pragma once

#include "Spirit/Core/PlatformDetection.h"

#ifdef SPIRIT_PLATFORM_WINDOWS
#ifndef NOMINMAX
// See github.com/skypjack/entt/wiki/Frequently-Asked-Questions#warning-c4003-the-min-the-max-and-the-macro
#define NOMINMAX
#endif
#endif

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <variant>
#include <unordered_map>
#include <unordered_set>

#include "Spirit/Core/Base.h"

#include "Spirit/Core/Log.h"

#include "Spirit/Debug/Instrumentor.h"

#ifdef SPIRIT_PLATFORM_WINDOWS
#include <Windows.h>
#endif
