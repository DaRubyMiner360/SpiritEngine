project "Spirit"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "off"

	targetdir ("%{wks.location}/bin/" .. outputdir)
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	pchheader "spiritpch.h"
	pchsource "src/spiritpch.cpp"

	files
	{
		"src/**.h",
		"src/**.cpp",
		"vendor/stb_image/**.h",
		"vendor/stb_image/**.cpp",
		"vendor/glm/glm/**.hpp",
		"vendor/glm/glm/**.inl",
		"vendor/minimp3/**.h",

		"vendor/miniaudio/miniaudio.h",

		"vendor/ImGuizmo/ImGuizmo.h",
		"vendor/ImGuizmo/ImGuizmo.cpp",
		"vendor/ImGuiColorTextEdit/TextEditor.h",
		"vendor/ImGuiColorTextEdit/TextEditor.cpp",

		"Discord-GameSDK/cpp/*.h",
		"Discord-GameSDK/cpp/*.cpp",

		"vendor/Box2D/src/collision/*.cpp",
		"vendor/Box2D/src/common/*.cpp",
		"vendor/Box2D/src/dynamics/*.cpp",
		"vendor/Box2D/src/rope/*.cpp"
	}

	defines
	{
		"SPIRIT_BUILD_DLL",
		"_CRT_SECURE_NO_WARNINGS",
		"GLFW_INCLUDE_NONE",
		"AL_LIBTYPE_STATIC"
	}

	includedirs
	{
		"src",
		"src/Utils",
		"vendor/spdlog/include",
		"%{IncludeDir.assimp}",
		"%{IncludeDir.assimp}/assimp",
		"%{IncludeDir.Box2D}",
		"vendor/Box2D/src",
		"vendor/Box2D/src/dynamics",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.ImGui}",
		"%{IncludeDir.glm}",
		"%{IncludeDir.stb_image}",
		"%{IncludeDir.entt}",
		"%{IncludeDir.mono}",
		"%{IncludeDir.yaml_cpp}",
		"%{IncludeDir.ImGuizmo}",
		"%{IncludeDir.ImGuiColorTextEdit}",
		"%{IncludeDir.NativeFileDialog}",
		"%{IncludeDir.VulkanSDK}",
		"%{IncludeDir.shaderc}",
		"%{IncludeDir.SPIRV_Cross}",
		--"%{IncludeDir.OpenALInclude}",
		--"%{IncludeDir.OpenALSrc}",
		--"%{IncludeDir.OpenALSrcCommon}",
		"%{IncludeDir.ogg}",
		"%{IncludeDir.Vorbis}",
		"%{IncludeDir.minimp3}",
		"%{IncludeDir.miniaudio}",
		"%{IncludeDir.DiscordGameSDK}",
	}

	links
	{
		"Spirit-ScriptCore",
		"%{Library.assimp}",
		"Box2D",
		"GLFW",
		"Glad",
		"ImGui",
		"yaml-cpp",
		--"OpenAL-Soft",
		"Vorbis",
		"libogg",
		"%{Library.DiscordGameSDK}",
		"%{Library.mono}"
	}

	filter "files:vendor/Box2D/**.cpp"
		flags { "NoPCH" }

	filter "files:vendor/ImGuizmo/**.cpp"
		flags { "NoPCH" }

	filter "files:vendor/NativeFileDialog/**.cpp"
		flags { "NoPCH" }
		
	filter "files:vendor/Discord-GameSDK/cpp/**.cpp"
		flags { "NoPCH" }

	filter { "system:windows", "action:vs*" }
		systemversion "latest"

		removefiles { "**/Linux/**", "**/MacOS/**" }

		links
		{ 
			"opengl32.lib",
			"%{Library.WinSock}",
			"%{Library.WinMM}",
			"%{Library.WinVersion}",
			"%{Library.BCrypt}",
		}

		defines
		{
			"SPIRIT_PLATFORM_WINDOWS"
		}

		files
		{
			"vendor/NativeFileDialog/src/nfd_win.cpp"
		}

	filter "system:linux"
		removefiles { "src/Platform/Windows/WindowsPlatformUtils.cpp", "**/MacOS/**" }

		buildoptions {"`pkg-config --cflags gtk+-3.0`"}

		defines
		{
			"SPIRIT_PLATFORM_LINUX",
		}

		files
		{
			--"/usr/include/gtk-3.0/gtk/**",
			--"/usr/include/gtk-3.0/gdk/**",
			"vendor/NativeFileDialog/src/nfd_gtk.cpp"
		}

	filter "system:macosx"
		removefiles { "src/Platform/Windows/WindowsPlatformUtils.cpp", "**/Linux/**" }
	
		defines
		{
			"SPIRIT_PLATFORM_MACOS",
		}

		files
		{
			"vendor/NativeFileDialog/src/nfd_cocoa.m"
		}
	
	--filter {"system:windows", "configurations:Debug"}
	--	links
	--	{
	--		"%{Library.ShaderC_Debug}",
	--		"%{Library.SPIRV_Cross_Debug}",
	--		"%{Library.SPIRV_Cross_GLSL_Debug}"
	--	}
	VULKAN_SDK = os.getenv("VULKAN_SDK")
	if VULKAN_SDK ~= nil then
		filter { "configurations:Debug", "action:vs*" }
			links
			{
				"%{Library.ShaderC_Debug}",
				"%{Library.SPIRV_Cross_Debug}",
				"%{Library.SPIRV_Cross_GLSL_Debug}"
			}
	end

	--filter {"system:windows", "configurations:Release or Dist"}
	--	links
	--	{
	--		"%{Library.ShaderC_Release}",
	--		"%{Library.SPIRV_Cross_Release}",
	--		"%{Library.SPIRV_Cross_GLSL_Release}"
	--	}
	if VULKAN_SDK ~= nil then
		filter { "configurations:Release", "action:vs*" }
			links
			{
				"%{Library.ShaderC_Release}",
				"%{Library.SPIRV_Cross_Release}",
				"%{Library.SPIRV_Cross_GLSL_Release}"
			}
	end

	if VULKAN_SDK ~= nil then
		filter { "configurations:Dist", "action:vs*" }
			links
			{
				"%{Library.ShaderC_Release}",
				"%{Library.SPIRV_Cross_Release}",
				"%{Library.SPIRV_Cross_GLSL_Release}"
			}
	end

	if VULKAN_SDK == nil then
		filter { "action:vs*" }
			links
			{
				"%{Library.ShaderC}",
				"%{Library.ShaderC_Util}",

				"%{Library.SPIRV}",
				"%{Library.SPIRV_Cross}",
				"%{Library.SPIRV_Cross_MachineIndependent}",
				"%{Library.SPIRV_Cross_OSDependent}",
				"%{Library.SPIRV_Cross_GenericCodeGen}",
				"%{Library.SPIRV_Cross_OGLCompiler}",

				"%{Library.SPIRV_Tools_SPIRV_Tools}",
				"%{Library.SPIRV_Tools_Opt}",
			}
	end

	filter "configurations:Debug"
		defines "SPIRIT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "SPIRIT_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "SPIRIT_DIST"
		runtime "Release"
		optimize "on"
