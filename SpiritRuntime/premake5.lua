project "SpiritRuntime"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "off"

	targetdir ("%{wks.location}/bin/" .. outputdir)
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"src/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"%{wks.location}/Spirit/vendor/spdlog/include",
		"%{wks.location}/Spirit/src",
		"%{wks.location}/Spirit/vendor",
		"%{IncludeDir.glm}",
		"%{IncludeDir.entt}",
		"%{IncludeDir.NativeFileDialog}"
	}

	links
	{
		"Spirit"
	}

	postbuildcommands
	{
		"{COPYDIR} \"%{wks.location}/%{prj.name}/mono\" \"%{cfg.targetdir}/mono\"",
		"{COPY} \"%{Library.mono}\" \"%{cfg.targetdir}\"",
		--"{COPYDIR} \"%{LibraryDir.OpenAL}\" \"%{cfg.targetdir}\"",
		"{COPYDIR} \"%{LibraryDir.assimp_DLL}\" \"%{cfg.targetdir}\"",

		"{COPYDIR} \"%{wks.location}/%{prj.name}/assets\" \"%{cfg.targetdir}/assets\"",
		"{COPYDIR} \"%{wks.location}/%{prj.name}/Resources\" \"%{cfg.targetdir}/Resources\""
	}

	filter "system:windows"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_WINDOWS"
		}

		files
		{
			"resource.h",
			"app.aps",
			"app.rc"
		}

	filter { "system:windows", "action:gmake*" }
		links
		{
			"GLFW",
			"Glad",
			"ImGui",
			"yaml-cpp",
			"OpenGL32",
			"gdi32",
			"comdlg32",
			"spirv-cross",
			"shaderc",
			"shaderc_util",
			"SPIRV-Tools-opt",
			"SPIRV-Tools",
			"MachineIndependent",
			"OSDependent",
			"GenericCodeGen",
			"OGLCompiler",
			"SPIRV"
		}

	filter "system:linux"
		links
		{
			"dl",
			"pthread",
			"GLFW",
			"GL",
			"Glad",
			"yaml-cpp",
			"X11",
			"spirv-cross",
			"shaderc",
			"shaderc_util",
			"SPIRV-Tools-opt",
			"SPIRV-Tools",
			"MachineIndependent",
			"OSDependent",
			"GenericCodeGen",
			"OGLCompiler",
			"SPIRV"


			--"Xrandr",
			--"Xi",
			--"GLU",
			--"stdc++fs",	--GCC versions 5.3 through 8.x need stdc++fs for std::filesystem

			--"vulkan",
			--"shaderc_shared",
			--"spirv-cross-c",
			--"spirv-cross-core",
			--"spirv-cross-cpp",
			--"spirv-cross-glsl",
			--"spirv-cross-reflect",
			--"spirv-cross-util",
			--"spirv-cross-c-shared",
			--"spirv-cross-hlsl",
			--"spirv-cross-msl"
		}

		defines
		{
			"SPIRIT_PLATFORM_LINUX"
		}

	filter "system:macosx"
		systemversion "latest"
	
		defines
		{
			"SPIRIT_PLATFORM_MACOS"
		}

	filter { "configurations:Debug", "action:vs*" }
		debugdir "%{cfg.targetdir}"
		defines "SPIRIT_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "SPIRIT_RELEASE"
		runtime "Release"
		optimize "on"
		kind "WindowedApp"
		entrypoint "mainCRTStartup"

	filter "configurations:Dist"
		defines "SPIRIT_DIST"
		runtime "Release"
		optimize "on"
