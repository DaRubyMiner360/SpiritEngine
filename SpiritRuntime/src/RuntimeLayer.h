#pragma once

#include <Spirit.h>
#include "entt.hpp"

namespace Spirit {

	class RuntimeLayer : public Layer
	{
	public:
		RuntimeLayer();
		virtual ~RuntimeLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;

		void OnUpdate(Timestep ts) override;
		virtual void OnImGuiRender() override;

		void OnOverlayRender();

		Ref<Scene> GetActiveScene() { return m_ActiveScene; }

		void OpenPrefab(const std::filesystem::path& path);
		void NewScene();
		void OpenScene(const std::filesystem::path& path);
	private:
		// Temp
		Ref<VertexArray> m_SquareVA;
		Ref<Shader> m_FlatColorShader;
		Ref<Framebuffer> m_Framebuffer;
		Ref<Framebuffer> m_IDFramebuffer;

		Ref<Scene> m_ActiveScene;

		bool m_ViewportFocused = false, m_ViewportHovered = false;
		glm::vec2 m_ViewportSize = { 0.0f, 0.0f };
	};

}
