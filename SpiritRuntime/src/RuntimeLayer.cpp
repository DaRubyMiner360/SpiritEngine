#include "spiritpch.h"
#include "RuntimeLayer.h"
#include "Spirit/Scene/SceneSerializer.h"
#include "Spirit/Utils/PlatformUtils.h"
#include "Spirit/Utils/StringUtils.h"
#include "Spirit/Utils/FileUtils.h"
#include "Spirit/Utils/INIUtils.h"
#include "Spirit/Math/Math.h"

#include "Spirit/Scene/ScriptableEntity.h"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <iostream>
#include <fstream>
#include <string>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Glad/include/glad/glad.h>

namespace Spirit {

	extern std::filesystem::path g_AssetPath;
	extern std::filesystem::path g_StartupScenePath;
	extern std::string g_ApplicationName;

	RuntimeLayer::RuntimeLayer()
		: Layer("RuntimeLayer")
	{
	}

	void RuntimeLayer::OnAttach()
	{
		SPIRIT_PROFILE_FUNCTION();

		FramebufferSpecification fbSpec;
		fbSpec.Attachments = { FramebufferTextureFormat::RGBA8, FramebufferTextureFormat::RED_INTEGER, FramebufferTextureFormat::Depth };
		fbSpec.Width = 1600;
		fbSpec.Height = 900;
		m_Framebuffer = Framebuffer::Create(fbSpec);

		m_IDFramebuffer = Framebuffer::Create(fbSpec);

		NewScene();


		auto commandLineArgs = Application::Get().GetSpecification().CommandLineArgs;
		if (commandLineArgs.Count > 1)
		{
			auto sceneFilePath = commandLineArgs[1];
			SceneSerializer serializer(m_ActiveScene);
			serializer.DeserializeScene(sceneFilePath);
		}

		Renderer2D::SetLineWidth(4.0f);

		Application::Get().GetWindow().SetTitle(g_ApplicationName);


		m_ActiveScene->OnRuntimeStart();

		if (!m_ActiveScene->GetPrimaryCameraEntity())
			SPIRIT_WARN("No active camera rendering!");
	}

	void RuntimeLayer::OnDetach()
	{
		SPIRIT_PROFILE_FUNCTION();
	}

	void RuntimeLayer::OnUpdate(Timestep ts)
	{
		SPIRIT_PROFILE_FUNCTION();

		// Resize
		if (FramebufferSpecification spec = m_Framebuffer->GetSpecification();
			m_ViewportSize.x > 0.0f && m_ViewportSize.y > 0.0f && // zero sized framebuffer is invalid
			(spec.Width != m_ViewportSize.x || spec.Height != m_ViewportSize.y))
		{
			m_Framebuffer->Resize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
			m_IDFramebuffer->Resize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
			m_ActiveScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
		}

		// Render
		Renderer2D::ResetStats();
		Renderer3D::ResetStats();
		m_Framebuffer->Bind();
		RenderCommand::SetClearColor({ 0.1f, 0.1f, 0.1f, 1 });
		RenderCommand::Clear();
		m_Framebuffer->Bind();

		// Clear our entity ID attachment to -1
		m_Framebuffer->ClearAttachment(1, -1);

		m_ActiveScene->OnUpdateRuntime(ts);

		OnOverlayRender();

		m_Framebuffer->Unbind();
	}

	void RuntimeLayer::OnImGuiRender()
	{
		SPIRIT_PROFILE_FUNCTION();

		// Note: Switch this to true to enable dockspace
		static bool dockspaceOpen = true;
		static bool opt_fullscreen_persistant = true;
		bool opt_fullscreen = opt_fullscreen_persistant;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}

		// When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;

		// Important: note that we proceed even if Begin() returns false (aka window is collapsed).
		// This is because we want to keep our DockSpace() active. If a DockSpace() is inactive, 
		// all active windows docked into it will lose their parent and become undocked.
		// We cannot preserve the docking relationship between an active window and an inactive docking, otherwise 
		// any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("Viewport", &dockspaceOpen, window_flags);
		ImGui::PopStyleVar();

		if (opt_fullscreen)
			ImGui::PopStyleVar(2);

		auto viewportMinRegion = ImGui::GetWindowContentRegionMin();
		auto viewportMaxRegion = ImGui::GetWindowContentRegionMax();
		auto viewportOffset = ImGui::GetWindowPos();

		m_ViewportFocused = ImGui::IsWindowFocused();
		m_ViewportHovered = ImGui::IsWindowHovered();

		Application::Get().GetImGuiLayer()->BlockEvents(false);

		ImVec2 viewportPanelSize = ImGui::GetContentRegionAvail();
		m_ViewportSize = { viewportPanelSize.x, viewportPanelSize.y };

		if (m_ActiveScene->GetPrimaryCameraEntity())
		{
			uint64_t textureID = m_Framebuffer->GetColorAttachmentRendererID();
			ImGui::Image(reinterpret_cast<void*>(textureID), ImVec2{ m_ViewportSize.x, m_ViewportSize.y }, ImVec2{ 0, 1 }, ImVec2{ 1, 0 });
		}
		else
		{
			ImGui::SetWindowFontScale(5);
			std::string text = "No Active";
			ImVec2 size = ImGui::CalcTextSize(text.c_str());
			ImGui::SetCursorPosX((m_ViewportSize.x * 0.5f) - (size.x * 0.5f));
			ImGui::SetCursorPosY((m_ViewportSize.y * 0.5f) - ((size.y * 0.5f) * 2.5f));
			ImGui::Text(text.c_str());

			text = "Camera Rendering";
			size = ImGui::CalcTextSize(text.c_str());
			ImGui::SetCursorPosX((m_ViewportSize.x * 0.5f) - (size.x * 0.5f));
			ImGui::SetCursorPosY((m_ViewportSize.y * 0.5f) - ((size.y * 0.5f) / 2.5f));
			ImGui::Text(text.c_str());
			ImGui::SetWindowFontScale(1);
		}

		ImGui::End();
	}

	void RuntimeLayer::OnOverlayRender()
	{
		Entity camera = m_ActiveScene->GetPrimaryCameraEntity();
		if (!camera)
			return;

		Renderer2D::BeginScene(camera.GetComponent<CameraComponent>().Camera, camera.GetComponent<TransformComponent>().GetTransform());
		Renderer3D::BeginScene(camera.GetComponent<CameraComponent>().Camera, camera.GetComponent<TransformComponent>().GetTransform());
		Renderer2D::EndScene();
		Renderer3D::EndScene();
	}

	void RuntimeLayer::OpenPrefab(const std::filesystem::path& path)
	{
		if (!FileTypes::IsPrefabType(path.extension().string()))
		{
			SPIRIT_WARN("Could not load {0} - not a prefab file", path.filename().string());
			return;
		}

		SceneSerializer serializer(m_ActiveScene);
		serializer.DeserializePrefab(path.string());
	}

	void RuntimeLayer::NewScene()
	{
		m_ActiveScene = CreateRef<Scene>();
		m_ActiveScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
		Application::Get().GetWindow().SetTitle(g_ApplicationName);
	}

	void RuntimeLayer::OpenScene(const std::filesystem::path& path)
	{
		if (!FileTypes::IsSceneType(path.extension().string()))
		{
			SPIRIT_WARN("Could not load {0} - not a scene file", path.filename().string());
			return;
		}

		Ref<Scene> newScene = CreateRef<Scene>();
		SceneSerializer serializer(newScene);
		if (serializer.DeserializeScene(path.string()))
		{
			m_ActiveScene = newScene;
			m_ActiveScene->OnViewportResize((uint32_t)m_ViewportSize.x, (uint32_t)m_ViewportSize.y);
		}

		Application::Get().GetWindow().SetTitle(g_ApplicationName);
	}

}
