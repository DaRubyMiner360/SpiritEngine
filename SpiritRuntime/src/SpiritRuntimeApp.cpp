#include <Spirit.h>
#include <Spirit/Core/EntryPoint.h>

#include "RuntimeLayer.h"

#include <iostream>
#include <fstream>
#include <string>

namespace Spirit {

	// TODO: Store information like this that the user will specify, in a file (preferably binary).
	extern std::filesystem::path g_AssetPath = "assets";
	extern std::filesystem::path g_StartupScenePath;
	extern std::string g_ApplicationName = "My Spirit Game";

	class SpiritRuntime : public Application
	{
	public:
		SpiritRuntime(const ApplicationSpecification& spec)
			: Application(spec)
		{
			PushLayer(new RuntimeLayer());
		}
	};

	Application* CreateApplication(ApplicationCommandLineArgs args)
	{
		ApplicationSpecification spec;
		spec.Name = g_ApplicationName;
		spec.CommandLineArgs = args;

		return new SpiritRuntime(spec);
	}

}
